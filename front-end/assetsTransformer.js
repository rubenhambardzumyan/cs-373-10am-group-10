// Caitlin's repo: https://gitlab.com/caitlinlien/cs373-sustainability/-/blob/phase3/frontend/assetsTransformer.js

const path = require("path");

module.exports = {
  process(src, filename, config, options) {
    return "module.exports = " + JSON.stringify(path.basename(filename)) + ";";
  },
};
