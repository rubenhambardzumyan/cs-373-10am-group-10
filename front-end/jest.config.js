// caitlin's repo: https://gitlab.com/caitlinlien/cs373-sustainability/-/blob/phase3/frontend/jest.config.js

module.exports = {
  roots: ["<rootDir>/src"],
  testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$",
  transform: {
    "^.+.(ts|tsx)?$": "ts-jest",
    "\\.(jpg|jpeg|png|gif|eot|otf)$": "<rootDir>/assetsTransformer.js",
    "\\.(webp|svg|ttf|woff|woff2|mp4|webm)$": "<rootDir>/assetsTransformer.js",
    "\\.(wav|mp3|m4a|aac|oga)$": "<rootDir>/assetsTransformer.js",
  },
  moduleFileExtensions: ["js", "json", "jsx", "node", "ts", "tsx"],
  globals: {
    "ts-jest": {
      tsconfig: {
        allowJs: true,
      },
    },
  },
  snapshotSerializers: ["enzyme-to-json/serializer"],
  setupFilesAfterEnv: ["<rootDir>/src/setupEnzyme.ts"],
  preset: "ts-jest",
  moduleNameMapper: {
    "\\.(jpg|jpeg|png|gif|eot|otf)$": "<rootDir>/src/__mocks__/fileMock.js",
    "\\.(webp|svg|ttf|woff|woff2|mp4)$": "<rootDir>/src/__mocks__/fileMock.js",
    "\\.(webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/src/__mocks__/fileMock.js",
    "\\.(css|less)$": "identity-obj-proxy",
  },
};
