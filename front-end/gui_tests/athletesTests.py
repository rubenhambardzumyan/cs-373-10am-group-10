import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options

import sys

PATH = "./front-end/gui_tests/chromedriver"
URL = "https://www.goingforgold.me/athletes?p=1"


class TestAthlete(unittest.TestCase):

    # Get drivers and run website before all tests
    @classmethod
    def setUpClass(cls):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        cls.driver = webdriver.Chrome(PATH, options=chrome_options)
        cls.driver.get(URL)

    # Close browser and quit after all tests
    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def test_athlete_instance(self):
        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, "card"))
            )
        except Exception as ex:
            print(ex)
            return
        time.sleep(5)
        element2 = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div[2]/div[3]/div/div[4]/div/div/div/h3/a'
        )
        self.driver.execute_script("arguments[0].click();", element2)
        assert self.driver.current_url == "https://www.goingforgold.me/athletes/4"
        time.sleep(5)
        element = self.driver.find_element_by_tag_name("h1")
        assert element.text == "Christine Aaftink"
        element3 = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div[2]/div/div/div[1]/button'
        )
        self.driver.execute_script("arguments[0].click();", element3)
        assert self.driver.current_url == "https://www.goingforgold.me/athletes?p=1"

    def test_athlete_pagination_next(self):
        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, "card"))
            )
        except Exception as ex:
            print(ex)
            return
        time.sleep(5)

        # click on next page
        element1 = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div[2]/div[4]/div[2]/button[6]'
        )
        self.driver.execute_script("arguments[0].click();", element1)
        time.sleep(5)

        # click on athlete on second page
        element2 = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div[2]/div[3]/div/div[10]/div/div/div/h3/a'
        )
        self.driver.execute_script("arguments[0].click();", element2)
        assert self.driver.current_url == "https://www.goingforgold.me/athletes/21"
        time.sleep(5)
        element = self.driver.find_element_by_tag_name("h1")
        assert element.text == "Ragnhild Aamodt"
        element3 = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div[2]/div/div/div[1]/button'
        )
        self.driver.execute_script("arguments[0].click();", element3)
        assert self.driver.current_url == "https://www.goingforgold.me/athletes?p=2"

    def test_athlete_pagination_pg_number(self):
        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, "card"))
            )
        except Exception as ex:
            print(ex)
            return
        time.sleep(5)

        # click on page 3, current is page 2
        element1 = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div[2]/div[4]/div[2]/button[5]'
        )
        self.driver.execute_script("arguments[0].click();", element1)
        time.sleep(5)

        # click on athlete on 3rd page
        element2 = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div[2]/div[3]/div/div[10]/div/div/div/h3/a'
        )
        self.driver.execute_script("arguments[0].click();", element2)
        assert self.driver.current_url == "https://www.goingforgold.me/athletes/1510"
        time.sleep(5)
        element = self.driver.find_element_by_tag_name("h1")
        assert element.text == "Im Dong-hyun"
        element3 = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div[2]/div/div/div[1]/button'
        )
        self.driver.execute_script("arguments[0].click();", element3)
        assert self.driver.current_url == "https://www.goingforgold.me/athletes?p=3"

    def test_athlete_pagination_prev(self):
        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, "card"))
            )
        except Exception as ex:
            print(ex)
            return
        time.sleep(5)

        # click on prev page
        element2 = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div[2]/div[4]/div[2]/button[2]'
        )
        self.driver.execute_script("arguments[0].click();", element2)
        assert self.driver.current_url == "https://www.goingforgold.me/athletes?p=2"
        # click on first page
        element2 = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div[2]/div[4]/div[2]/button[1]'
        )
        self.driver.execute_script("arguments[0].click();", element2)
        assert self.driver.current_url == "https://www.goingforgold.me/athletes?p=1"

        time.sleep(5)
        # click on athlete 7 on first page
        element4 = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div[2]/div[3]/div/div[7]/div/div/div/h3/a'
        )
        self.driver.execute_script("arguments[0].click();", element4)
        assert self.driver.current_url == "https://www.goingforgold.me/athletes/7"
        time.sleep(5)
        element = self.driver.find_element_by_tag_name("h1")
        assert element.text == "Katrine Aalerud"
        element3 = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div[2]/div/div/div[1]/button'
        )
        self.driver.execute_script("arguments[0].click();", element3)
        assert self.driver.current_url == "https://www.goingforgold.me/athletes?p=1"

    def test_filter(self):
        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, "card"))
            )
        except Exception as ex:
            print(ex)
            return
        time.sleep(5)
        assert self.driver.current_url == "https://www.goingforgold.me/athletes?p=1"
        element2 = self.driver.find_element_by_xpath(
            "/html/body/div/div/div[2]/div[2]/div[2]/div[1]/div"
        )
        self.driver.execute_script("arguments[0].click();", element2)
        assert self.driver.current_url == "https://www.goingforgold.me/athletes?p=1"
        time.sleep(5)
        element2b = self.driver.find_element_by_xpath(
            "/html/body/div/div/div[2]/div[2]/div[2]/div[1]/div/div[2]/ul/li[3]"
        )
        self.driver.execute_script("arguments[0].click();", element2b)
        assert (
            self.driver.current_url
            == "https://www.goingforgold.me/athletes?p=1&athlete_name=F-K"
        )
        time.sleep(5)

        element3 = self.driver.find_element_by_xpath(
            "/html/body/div/div/div[2]/div[2]/div[2]/div[2]/div/div[1]"
        )
        self.driver.execute_script("arguments[0].click();", element3)
        assert (
            self.driver.current_url
            == "https://www.goingforgold.me/athletes?p=1&athlete_name=F-K"
        )
        time.sleep(5)
        element3b = self.driver.find_element_by_xpath(
            "/html/body/div/div/div[2]/div[2]/div[2]/div[2]/div/div[2]/ul/li[2]"
        )
        self.driver.execute_script("arguments[0].click();", element3b)
        assert (
            self.driver.current_url
            == "https://www.goingforgold.me/athletes?p=1&athlete_name=F-K&athlete_sport=Archery"
        )
        time.sleep(5)

        element4 = self.driver.find_element_by_xpath(
            "/html/body/div/div/div[2]/div[2]/div[2]/div[2]/div/div[1]"
        )
        self.driver.execute_script("arguments[0].click();", element4)
        assert (
            self.driver.current_url
            == "https://www.goingforgold.me/athletes?p=1&athlete_name=F-K&athlete_sport=Archery"
        )
        time.sleep(5)
        element4b = self.driver.find_element_by_xpath(
            "/html/body/div/div/div[2]/div[2]/div[2]/div[2]/div/div[1]/span/img"
        )
        self.driver.execute_script("arguments[0].click();", element4b)
        assert (
            self.driver.current_url
            == "https://www.goingforgold.me/athletes?p=1&athlete_name=F-K"
        )
        time.sleep(5)

        element5 = self.driver.find_element_by_xpath(
            "/html/body/div/div/div[2]/div[2]/div[2]/div[1]/div/div[1]"
        )
        self.driver.execute_script("arguments[0].click();", element5)
        assert (
            self.driver.current_url
            == "https://www.goingforgold.me/athletes?p=1&athlete_name=F-K"
        )
        time.sleep(5)
        element5b = self.driver.find_element_by_xpath(
            "/html/body/div/div/div[2]/div[2]/div[2]/div[1]/div/div[2]/ul/li[1]"
        )
        self.driver.execute_script("arguments[0].click();", element5b)
        assert self.driver.current_url == "https://www.goingforgold.me/athletes?p=1"

    def test_sort(self):
        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, "card"))
            )
        except Exception as ex:
            print(ex)
            return
        time.sleep(5)
        assert self.driver.current_url == "https://www.goingforgold.me/athletes?p=1"
        element2 = self.driver.find_element_by_xpath(
            "/html/body/div/div/div[2]/div[2]/div[4]/div[1]/div/button"
        )
        self.driver.execute_script("arguments[0].click();", element2)
        assert self.driver.current_url == "https://www.goingforgold.me/athletes?p=1"
        time.sleep(5)
        element2b = self.driver.find_element_by_xpath(
            "/html/body/div/div/div[2]/div[2]/div[4]/div[1]/div/div/a[1]"
        )
        self.driver.execute_script("arguments[0].click();", element2b)
        assert (
            self.driver.current_url
            == "https://www.goingforgold.me/athletes?p=1&sort=name&order=asc"
        )
        time.sleep(5)
        element2 = self.driver.find_element_by_xpath(
            "/html/body/div/div/div[2]/div[2]/div[4]/div[1]/div/button"
        )
        self.driver.execute_script("arguments[0].click();", element2)
        assert (
            self.driver.current_url
            == "https://www.goingforgold.me/athletes?p=1&sort=name&order=asc"
        )
        time.sleep(5)
        element2b = self.driver.find_element_by_xpath(
            "/html/body/div/div/div[2]/div[2]/div[4]/div[1]/div/div/a[2]"
        )
        self.driver.execute_script("arguments[0].click();", element2b)
        assert (
            self.driver.current_url
            == "https://www.goingforgold.me/athletes?p=1&sort=name&order=desc"
        )
        self.driver.back()
        self.driver.back()
        assert self.driver.current_url == "https://www.goingforgold.me/athletes?p=1"


if __name__ == "__main__":
    PATH = sys.argv[1]
    unittest.main(argv=["first-arg-is-ignored"])
