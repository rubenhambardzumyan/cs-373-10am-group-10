import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options

import sys

PATH = "./front-end/gui_tests/chromedriver"
URL = "https://goingforgold.me/countries/6"


class TestCountry(unittest.TestCase):

    # Get drivers and run website before all tests
    @classmethod
    def setUpClass(cls):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        cls.driver = webdriver.Chrome(PATH, options=chrome_options)
        cls.driver.get(URL)

    # Close browser and quit after all tests
    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def test_countries_instance_athlete(self):
        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, "card"))
            )
        except Exception as ex:
            print(ex)
            return
        time.sleep(5)
        assert self.driver.current_url == "https://www.goingforgold.me/countries/6"
        element2 = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div[2]/div/div/div[4]/div[2]/p[3]/div/div/div[1]/a'
        )
        self.driver.execute_script("arguments[0].click();", element2)
        assert self.driver.current_url == "https://www.goingforgold.me/athletes/140"
        time.sleep(5)
        element = self.driver.find_element_by_tag_name("h1")
        assert element.text == "Emily Abbot"
        element3 = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div[2]/div/div/div[1]/button'
        )
        self.driver.execute_script("arguments[0].click();", element3)
        assert self.driver.current_url == "https://www.goingforgold.me/countries/6"

    def test_countries_instance_event(self):
        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, "card"))
            )
        except Exception as ex:
            print(ex)
            return
        time.sleep(5)
        assert self.driver.current_url == "https://www.goingforgold.me/countries/6"
        element2 = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div[2]/div/div/div[4]/div[2]/p[4]/div/div/div[1]/a'
        )
        self.driver.execute_script("arguments[0].click();", element2)
        assert self.driver.current_url == "https://www.goingforgold.me/events/122"
        time.sleep(5)
        element = self.driver.find_element_by_tag_name("h1")
        assert element.text == "MEN'S 400M FREESTYLE"
        element3 = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div[2]/div/div/div[1]/div/button'
        )
        self.driver.execute_script("arguments[0].click();", element3)
        assert self.driver.current_url == "https://www.goingforgold.me/countries/6"


if __name__ == "__main__":
    PATH = sys.argv[1]
    unittest.main(argv=["first-arg-is-ignored"])
