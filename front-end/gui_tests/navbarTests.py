import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options

import sys

PATH = "./front-end/gui_tests/chromedriver"
URL = "https://goingforgold.me/"


class TestNavbar(unittest.TestCase):

    # Get drivers and run website before all tests
    @classmethod
    def setUpClass(cls):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        cls.driver = webdriver.Chrome(PATH, options=chrome_options)
        cls.driver.get(URL)

    # Close browser and quit after all tests
    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def testAbout(self):
        element2 = self.driver.find_element_by_xpath(
            '//*[@id="basic-navbar-nav"]/div/a[2]'
        )
        self.driver.execute_script("arguments[0].click();", element2)
        assert self.driver.current_url == "https://www.goingforgold.me/about"
        element = self.driver.find_element_by_tag_name("h3")
        assert element.text == "About our Website"
        self.driver.back()
        assert self.driver.current_url == "https://www.goingforgold.me/"

    def testAthletes(self):
        element2 = self.driver.find_element_by_xpath(
            '//*[@id="basic-navbar-nav"]/div/a[5]'
        )
        self.driver.execute_script("arguments[0].click();", element2)
        assert self.driver.current_url == "https://www.goingforgold.me/athletes?p=1"
        element = self.driver.find_element_by_tag_name("h1")
        assert element.text == "Athletes"
        self.driver.back()
        assert self.driver.current_url == "https://www.goingforgold.me/"

    def testCountries(self):
        element2 = self.driver.find_element_by_xpath(
            '//*[@id="basic-navbar-nav"]/div/a[4]'
        )
        self.driver.execute_script("arguments[0].click();", element2)
        assert self.driver.current_url == "https://www.goingforgold.me/countries?p=1"
        element = self.driver.find_element_by_tag_name("h1")
        assert element.text == "Countries"
        self.driver.back()
        assert self.driver.current_url == "https://www.goingforgold.me/"

    def testEvents(self):
        element2 = self.driver.find_element_by_xpath(
            '//*[@id="basic-navbar-nav"]/div/a[3]'
        )
        self.driver.execute_script("arguments[0].click();", element2)
        assert self.driver.current_url == "https://www.goingforgold.me/events?p=1"
        element = self.driver.find_element_by_tag_name("h1")
        assert element.text == "Events"
        self.driver.back()
        assert self.driver.current_url == "https://www.goingforgold.me/"

    def testSplash(self):
        element2 = self.driver.find_element_by_xpath(
            '//*[@id="basic-navbar-nav"]/div/a[1]'
        )
        self.driver.execute_script("arguments[0].click();", element2)
        assert self.driver.current_url == "https://www.goingforgold.me/"
        element = self.driver.find_element_by_tag_name("h1")
        assert element.text == "Going For Gold"

    def testSearch(self):
        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, "card"))
            )
        except Exception as ex:
            print(ex)
            return
        time.sleep(5)
        assert self.driver.current_url == "https://www.goingforgold.me/"
        element2 = self.driver.find_element_by_xpath(
            "/html/body/div/div/div[1]/nav/div/div/div/input"
        )
        my_string = "Tokyo"
        self.driver.execute_script(
            "arguments[0].setAttribute('value', '" + my_string + "');", element2
        )
        element2b = self.driver.find_element_by_xpath(
            "/html/body/div/div/div[1]/nav/div/div/div/button"
        )
        self.driver.execute_script("arguments[0].click();", element2b)
        assert (
            self.driver.current_url == "https://www.goingforgold.me/search?p=1&q=Tokyo"
        )
        time.sleep(5)
        element2 = self.driver.find_element_by_xpath(
            "/html/body/div/div/div[1]/nav/div/div/div/input"
        )
        my_string = "Swimming"
        self.driver.execute_script(
            "arguments[0].setAttribute('value', '" + my_string + "');", element2
        )
        element2b = self.driver.find_element_by_xpath(
            "/html/body/div/div/div[1]/nav/div/div/div/button"
        )
        self.driver.execute_script("arguments[0].click();", element2b)
        assert (
            self.driver.current_url
            == "https://www.goingforgold.me/search?p=1&q=Swimming"
        )


if __name__ == "__main__":
    PATH = sys.argv[1]
    unittest.main(argv=["first-arg-is-ignored"])
