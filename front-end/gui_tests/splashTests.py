import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options

import sys

PATH = "./front-end/gui_tests/chromedriver"
URL = "https://goingforgold.me/"


class TestSplash(unittest.TestCase):

    # Get drivers and run website before all tests
    @classmethod
    def setUpClass(cls):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        cls.driver = webdriver.Chrome(PATH, options=chrome_options)
        cls.driver.get(URL)

    # Close browser and quit after all tests
    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def testSplash(self):
        self.driver.find_element_by_tag_name("div").click()
        element = self.driver.find_element_by_tag_name("h1")
        assert element.text != ""

    def testCountriesBtn(self):
        element2 = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div[2]/div/div[2]/div/div/div[2]/div/div/div/a'
        )
        self.driver.execute_script("arguments[0].click();", element2)
        assert self.driver.current_url == "https://www.goingforgold.me/countries?p=1"
        element = self.driver.find_element_by_tag_name("h1")
        assert element.text == "Countries"
        self.driver.back()
        assert self.driver.current_url == "https://www.goingforgold.me/"

    def testAthletesBtn(self):
        element2 = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div[2]/div/div[2]/div/div/div[1]/div/div/div/a'
        )
        self.driver.execute_script("arguments[0].click();", element2)
        assert self.driver.current_url == "https://www.goingforgold.me/athletes?p=1"
        element = self.driver.find_element_by_tag_name("h1")
        assert element.text == "Athletes"
        self.driver.back()
        assert self.driver.current_url == "https://www.goingforgold.me/"

    def testEventsBtn(self):
        element2 = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div[2]/div/div[2]/div/div/div[3]/div/div/div/a'
        )
        self.driver.execute_script("arguments[0].click();", element2)
        assert self.driver.current_url == "https://www.goingforgold.me/events?p=1"
        element = self.driver.find_element_by_tag_name("h1")
        assert element.text == "Events"
        self.driver.back()
        assert self.driver.current_url == "https://www.goingforgold.me/"


if __name__ == "__main__":
    PATH = sys.argv[1]
    unittest.main(argv=["first-arg-is-ignored"])
