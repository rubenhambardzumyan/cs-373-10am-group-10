import React from "react";
import Bar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { Link } from "react-router-dom";
import Container from "react-bootstrap/Container";
import Image from "react-bootstrap/Image";
import logo from "../images/logo.jpg";
import Button from "react-bootstrap/Button";
import FormControl from "react-bootstrap/FormControl";
import { AiOutlineSearch } from "react-icons/ai";

import "bootstrap/dist/css/bootstrap.min.css";

function handleSearch() {
  const toStr = "/search?p=1&q=" + textInput.current.value;
  window.location.assign(`${toStr}`);
}
const textInput: any = React.createRef();
// qParam store any search input given by the user
const qParams = null;
// searchdStr stored the previous input given by user to display in the search bar
const searchedStr = qParams != null && qParams != "" ? qParams : "";
export default function Navbar() {
  return (
    <div>
      <Bar style={{ background: "rgba(0, 0, 0)" }} variant="dark" expand="lg">
        <Container>
          <Bar.Toggle aria-controls="basic-navbar-nav" />
          <Bar.Collapse id="basic-navbar-nav">
            <Image style={{ width: "15%", height: "15%" }} src={logo} />
            <Nav className="container-fluid">
              {/*we have the tabs that link to other pages*/}
              <Nav.Link as={Link} to="/">
                Splash
              </Nav.Link>
              <Nav.Link as={Link} to="/about">
                About
              </Nav.Link>
              <Nav.Link as={Link} to="/events?p=1">
                Events
              </Nav.Link>
              <Nav.Link as={Link} to="/countries?p=1">
                Countries
              </Nav.Link>
              <Nav.Link as={Link} to="/athletes?p=1">
                Athletes
              </Nav.Link>
              <Nav.Link as={Link} to="/visualizations">
                Visualizations
              </Nav.Link>
              <Nav.Link as={Link} to="/ProviderVisualization">
                Provider Visualization
              </Nav.Link>
              {/* this is our search bar that searches from all 3 models */}
              <FormControl
                className="mr-sm-2"
                type="text"
                placeholder="Search"
                defaultValue={searchedStr}
                ref={textInput}
                onKeyPress={(event: any) => {
                  if (event.key === "Enter") {
                    handleSearch();
                  }
                }}
                style={{ width: "200px" }}
              />
              <Button
                className="searchButton"
                variant="info"
                onClick={() => handleSearch()}
              >
                <AiOutlineSearch className="searchIcon" />
              </Button>
            </Nav>
          </Bar.Collapse>
        </Container>
      </Bar>
    </div>
  );
}
