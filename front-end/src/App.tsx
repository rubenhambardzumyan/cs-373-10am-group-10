import React from "react";
import "./App.css";
import Splash from "./pages/Splash";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <div>
      <Splash />
    </div>
  );
}

export default App;
