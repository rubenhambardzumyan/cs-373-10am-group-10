// https://enzymejs.github.io/enzyme/docs/api/ShallowWrapper/find.html
// Caitlin's repo: https://gitlab.com/caitlinlien/cs373-sustainability/-/blob/master/frontend/src/tests/tests.tsx
// https://jestjs.io/docs/getting-started#using-typescript
// https://github.com/cedrickchee/react-typescript-jest-enzyme-testing
// https://jestjs.io/docs/getting-started
// https://www.codementor.io/@rajjeet/add-jest-to-your-typescript-project-with-4-easy-steps-1do5lhfjb1

import React from "react";
import App from "../App";
import Splash from "../pages/Splash";
import About from "../pages/About";
import Countries from "../pages/Countries";
import CountriesDetails from "../pages/CountriesDetails";
import Athletes from "../pages/Athletes";
import Events from "../pages/Events";
import EventsDetails from "../pages/EventsDetails";
import CountriesSearch from "../pages/CountriesSearch";
import EventsSearch from "../pages/EventsSearch";

import { expect } from "chai";
import * as Enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { shallow, mount } from "enzyme";
import {
  Card,
  CardGroup,
  Container,
  Row,
  Col,
  Button,
  Image,
  Nav,
  Dropdown,
  FormControl,
} from "react-bootstrap";
import Bar from "react-bootstrap/Navbar";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Navbar from "../components/Navbar";
import MUIDataTable from "mui-datatables";
import AthletesDetails from "../pages/AthletesDetails";
import { QueryClient, QueryClientProvider } from "react-query";
import Multiselect from "multiselect-react-dropdown";
import { AiOutlineSearch } from "react-icons/ai";

const queryClient = new QueryClient();

Enzyme.configure({
  adapter: new Adapter(),
});

/* App page test */
it("Get App Page", async () => {
  const wrapper = shallow(<App />);
  expect(wrapper).to.not.be.undefined;
  expect(wrapper).to.have.length(1);
  expect(wrapper.find("div")).to.have.length(1);
  expect(wrapper.find(Splash)).to.have.length(1);
});

/* Splash page test */
it("Get Splash Page", async () => {
  const wrapper = shallow(<Splash />);
  expect(wrapper).to.not.be.undefined;
  expect(wrapper).to.have.length(1);
  expect(wrapper.find("div")).to.have.length(4);
  expect(wrapper.find("img")).to.have.length(1);
  expect(wrapper.find(Container)).to.have.length(1);
  expect(wrapper.find(Row)).to.have.length(1);
  expect(wrapper.find(Col)).to.have.length(3);
  expect(wrapper.find(Card.Img)).to.have.length(3);
  expect(wrapper.find(Card)).to.have.length(3);
  expect(wrapper.find(Button)).to.have.length(3);
});

it("Get About Page", async () => {
  const wrapper = shallow(<About />);
  expect(wrapper).to.not.be.undefined;
  expect(wrapper).to.have.length(1);
  expect(wrapper.find("div")).to.have.length(1);
  expect(wrapper.find("h1")).to.have.length(1);
  expect(wrapper.find(Card)).to.have.length(27);
  expect(wrapper.find(CardGroup)).to.have.length(4);
  expect(wrapper.find(Row)).to.have.length(6);
  expect(wrapper.find(Col)).to.have.length(25);
});

it("Get Countries view all Page", async () => {
  const wrapper = mount(
    <QueryClientProvider client={queryClient}>
      <Countries />
    </QueryClientProvider>
  );
  expect(wrapper).to.not.be.undefined;
  expect(wrapper).to.have.length(1);
  expect(wrapper.find("div")).to.have.length(71);
  expect(wrapper.find(MUIDataTable)).to.have.length(1);
  expect(wrapper.find("h1")).to.have.length(1);
  expect(wrapper.find("h5")).to.have.length(5);
  expect(wrapper.find(Button)).to.have.length(15);
  expect(wrapper.find(Container)).to.have.length(1);
  expect(wrapper.find(Row)).to.have.length(7);
  expect(wrapper.find(Col)).to.have.length(15);
});

it("Get Events view all Page", async () => {
  const wrapper = mount(
    <QueryClientProvider client={queryClient}>
      <Events />
    </QueryClientProvider>
  );
  expect(wrapper).to.not.be.undefined;
  expect(wrapper).to.have.length(1);
  expect(wrapper.find("div")).to.have.length(58);
  expect(wrapper.find("img")).to.have.length(2);
  expect(wrapper.find(MUIDataTable)).to.have.length(1);
  expect(wrapper.find("h1")).to.have.length(1);
  expect(wrapper.find("h5")).to.have.length(4);
  expect(wrapper.find(Button)).to.have.length(13);
  expect(wrapper.find(Container)).to.have.length(1);
  expect(wrapper.find(Row)).to.have.length(5);
  expect(wrapper.find(Col)).to.have.length(11);
});

it("Get Countries Dynamic Instance Page", async () => {
  const wrapper = shallow(
    <BrowserRouter>
      <CountriesDetails />
    </BrowserRouter>
  );
  expect(wrapper).to.not.be.undefined;
  expect(wrapper).to.have.length(1);
});

it("Get Events Dynamic Instance Page", async () => {
  const wrapper = shallow(
    <BrowserRouter>
      <EventsDetails />
    </BrowserRouter>
  );
  expect(wrapper).to.not.be.undefined;
  expect(wrapper).to.have.length(1);
});

it("Get Navbar component", async () => {
  const wrapper = shallow(<Navbar />);
  expect(wrapper).to.not.be.undefined;
  expect(wrapper).to.have.length(1);
  expect(wrapper.find("div")).to.have.length(1);
  expect(wrapper.find(Bar)).to.have.length(1);
  expect(wrapper.find(Container)).to.have.length(1);
  expect(wrapper.find(Nav)).to.have.length(1);
  expect(wrapper.find(Nav.Link)).to.have.length(7);
  expect(wrapper.find(Image)).to.have.length(1);
});

it("Get Athletes View All Page", async () => {
  const wrapper = mount(
    <QueryClientProvider client={queryClient}>
      <Athletes />
    </QueryClientProvider>
  );
  expect(wrapper).to.not.be.undefined;
  expect(wrapper).to.have.length(1);
  expect(wrapper.find("div")).to.have.length(66);
  expect(wrapper.find("h1")).to.have.length(1);
  expect(wrapper.find(CardGroup)).to.have.length(1);
  expect(wrapper.find(Card)).to.have.length(1);
  expect(wrapper.find(Button)).to.have.length(15);
  expect(wrapper.find(Container)).to.have.length(2);
  expect(wrapper.find(Row)).to.have.length(10);
  expect(wrapper.find(Col)).to.have.length(16);
});

it("Get Athletes Dynamic Instance Page", async () => {
  const wrapper = shallow(
    <BrowserRouter>
      <AthletesDetails />
    </BrowserRouter>
  );
  expect(wrapper).to.not.be.undefined;
  expect(wrapper).to.have.length(1);
});

it("Filter tests", async () => {
  let wrapper = mount(
    <QueryClientProvider client={queryClient}>
      <Athletes />
    </QueryClientProvider>
  );
  expect(wrapper).to.not.be.undefined;
  expect(wrapper).to.have.length(1);
  expect(wrapper.find(Multiselect)).to.have.length(8);
  wrapper = mount(
    <QueryClientProvider client={queryClient}>
      <Countries />
    </QueryClientProvider>
  );
  expect(wrapper).to.not.be.undefined;
  expect(wrapper).to.have.length(1);
  expect(wrapper.find(Multiselect)).to.have.length(7);
  wrapper = mount(
    <QueryClientProvider client={queryClient}>
      <Events />
    </QueryClientProvider>
  );
  expect(wrapper).to.not.be.undefined;
  expect(wrapper).to.have.length(1);
  expect(wrapper.find(Multiselect)).to.have.length(5);
});

it("Sort tests", async () => {
  let wrapper = mount(
    <QueryClientProvider client={queryClient}>
      <Athletes />
    </QueryClientProvider>
  );
  expect(wrapper).to.not.be.undefined;
  expect(wrapper).to.have.length(1);
  expect(wrapper.find(Dropdown)).to.have.length(6);
  wrapper = mount(
    <QueryClientProvider client={queryClient}>
      <Countries />
    </QueryClientProvider>
  );
  expect(wrapper).to.not.be.undefined;
  expect(wrapper).to.have.length(1);
  expect(wrapper.find(Dropdown)).to.have.length(5);
  wrapper = mount(
    <QueryClientProvider client={queryClient}>
      <Events />
    </QueryClientProvider>
  );
  expect(wrapper).to.not.be.undefined;
  expect(wrapper).to.have.length(1);
  expect(wrapper.find(Dropdown)).to.have.length(4);
});

it("Search tests", async () => {
  let wrapper = mount(
    <QueryClientProvider client={queryClient}>
      <Athletes />
    </QueryClientProvider>
  );
  expect(wrapper).to.not.be.undefined;
  expect(wrapper).to.have.length(1);
  expect(wrapper.find(FormControl)).to.have.length(1);
  expect(wrapper.find(".searchButton")).to.have.length(2);
  expect(wrapper.find(AiOutlineSearch)).to.have.length(1);
  wrapper = mount(
    <QueryClientProvider client={queryClient}>
      <CountriesSearch />
    </QueryClientProvider>
  );
  expect(wrapper).to.not.be.undefined;
  expect(wrapper).to.have.length(1);
  expect(wrapper.find(FormControl)).to.have.length(1);
  expect(wrapper.find(".searchButton")).to.have.length(2);
  expect(wrapper.find(AiOutlineSearch)).to.have.length(1);
  wrapper = mount(
    <QueryClientProvider client={queryClient}>
      <EventsSearch />
    </QueryClientProvider>
  );
  expect(wrapper).to.not.be.undefined;
  expect(wrapper).to.have.length(1);
  expect(wrapper.find(FormControl)).to.have.length(1);
  expect(wrapper.find(".searchButton")).to.have.length(2);
  expect(wrapper.find(AiOutlineSearch)).to.have.length(1);
});
