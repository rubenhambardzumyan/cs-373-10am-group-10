import React, { useEffect, useState } from "react";
import {
  Bar,
  Tooltip,
  ScatterChart,
  XAxis,
  YAxis,
  Scatter,
  Legend,
  RadarChart,
  PolarAngleAxis,
  PolarRadiusAxis,
  Radar,
  PolarGrid,
  BarChart,
  ResponsiveContainer,
} from "recharts";
import Container from "react-bootstrap/Container";
import "./Visualizations.css";

function ProviderVisualization() {
  useEffect(() => {
    fetchUnivs();
    fetchHousing();
    fetchAmenities();
  }, []);

  const [univs, setUnivs] = useState([]);
  const [houses, setHouses] = useState([]);
  const [amenities, setAmenities] = useState([]);
  // fetch universities from Campus Catalog's API
  const fetchUnivs = async () => {
    try {
      const res = await fetch(
        `https://api.campuscatalog.me/universities?per_page=189`
      );
      const uni = await res.json();
      setUnivs(uni[1].universities);
    } catch (e) {
      console.log("error");
      setUnivs([]);
    }
  };
  // fetch housing from Campus Catalog's API
  const fetchHousing = async () => {
    try {
      const res = await fetch(
        `https://api.campuscatalog.me/housing?per_page=1000`
      );
      const houses = await res.json();
      setHouses(houses[1].properties);
    } catch (e) {
      console.log("error");
      setHouses([]);
    }
  };
  // fetch amenities from Campus Catalog's API
  const fetchAmenities = async () => {
    try {
      const res = await fetch(
        `https://api.campuscatalog.me/amenities?per_page=1476`
      );
      const amenity = await res.json();
      setAmenities(amenity[1].amenities);
    } catch (e) {
      console.log("error");
      setAmenities([]);
    }
  };

  // project universitites data into an array with necessary columns
  const univData = univs?.map((univ: any) => ({
    name: univ.univ_name,
    val1: univ.acceptance_rate,
    val2: univ.avg_cost_attendance,
  }));

  // project housing data into an array with necessary columns
  const houseData = houses?.map((house: any) => ({
    name: house.property_name,
    val1: house.walk_score,
    val2: house.transit_score,
  }));

  const amenitiesData = amenities?.map((amenity: any) => ({
    city: amenity.city,
  }));
  function tallyAmenitiesPerCity(arr: any, key: any) {
    //https://stackoverflow.com/questions/44829459/javascript-count-number-values-repeated-in-a-jsonarray-items
    const arr2: any[] = [];
    arr.forEach((x: any) => {
      if (
        arr2.some((val: any) => {
          return val[key] == x[key];
        })
      ) {
        arr2.forEach((k: any) => {
          if (k[key] === x[key]) {
            k["num_amenities"]++;
          }
        });
      } else {
        const a: any = {};
        a[key] = x[key];
        a["num_amenities"] = 1;
        arr2.push(a);
      }
    });
    return arr2;
  }
  const amenitiesNumber = tallyAmenitiesPerCity(amenitiesData, "city");

  // Customize Visualizatin Tooltip
  const CustomTooltip = ({ active, payload }: any) => {
    if (active && payload && payload.length) {
      return (
        <div className="custom-tooltip">
          <p>{`Name : ${payload[0].payload.name}`}</p>
          <p>{`Acceptance Rate : ${payload[0].payload.val1} %`}</p>
          <p>{`Average Cost of Attendance :  $ ${payload[0].payload.val2}`}</p>
        </div>
      );
    }
    return null;
  };
  const CustomTooltip2 = ({ active, payload }: any) => {
    if (active && payload && payload.length) {
      return (
        <div className="custom-tooltip">
          <p>{`Property : ${payload[0].payload.name}`}</p>
          <p>{`Walk Score : ${payload[0].payload.val1} `}</p>
          <p>{`Transit Score : ${payload[0].payload.val2}`}</p>
        </div>
      );
    }
    return null;
  };

  return (
    <Container className="viz-div">
      <h1 className="title">Provider Visualizations</h1>

      {/* First Visualization */}
      <h4 className="sub-title">
        Plot of Acceptance Rates versus Average Cost of Attendance for
        Universities
      </h4>
      <ResponsiveContainer width="100%" height={600}>
        <ScatterChart width={1300} height={600} data={univData}>
          <XAxis dataKey="val1" name="Acceptance Rate" unit="%" />
          <YAxis dataKey="val2" name="Average Cost of Attendance" unit="$" />
          <Tooltip content={<CustomTooltip />} />
          <Scatter name="A" data={univData} fill="#8884d8" />
        </ScatterChart>
      </ResponsiveContainer>

      {/* Second Visualization */}
      <h4 className="sub-title">
        Radar Chart of the Walk Score versus the Transit Scores of Housing
        Properties
      </h4>
      <ResponsiveContainer width="100%" height={600}>
        <RadarChart
          outerRadius={500}
          width={1300}
          height={700}
          data={houseData}
        >
          <PolarGrid />
          <PolarAngleAxis dataKey="subject" />
          <PolarRadiusAxis angle={30} domain={[0, 150]} />
          <Tooltip content={<CustomTooltip2 />} />
          <Radar
            name="Walk Score"
            dataKey="val1"
            stroke="#8884d8"
            fill="#8884d8"
            fillOpacity={0.4}
          />
          <Radar
            name="Transit Score"
            dataKey="val2"
            stroke="#82ca9d"
            fill="#82ca9d"
            fillOpacity={0.4}
          />
          <Legend />
        </RadarChart>
      </ResponsiveContainer>

      {/* Third Visualization */}
      <h4 className="sub-title">Total Number of Amenities Per City</h4>
      <ResponsiveContainer width="100%" height={600}>
        <BarChart width={1300} height={700} data={amenitiesNumber}>
          <XAxis dataKey="city" />
          <YAxis />
          <Tooltip />
          <Bar
            dataKey="num_amenities"
            fill="#8884d8"
            name="Number of Amenities"
          />
        </BarChart>
      </ResponsiveContainer>
    </Container>
  );
}

export default ProviderVisualization;
