import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import CountriesDetails from "./CountriesDetails";

function CountriesCompare() {
  const queryParams = new URLSearchParams(window.location.search);
  const c1Params = queryParams.get("c1");
  const c1 = parseInt(c1Params != null && c1Params != "" ? c1Params : "1");
  const propsObj1 = { params: { id: c1 } };
  const c2Params = queryParams.get("c2");
  const c2 = parseInt(c2Params != null && c2Params != "" ? c2Params : "1");
  const propsObj2 = { params: { id: c2 } };
  return (
    <Container>
      <Row md={2}>
        <Col>
          <CountriesDetails match={propsObj1} />
        </Col>
        <Col>
          <CountriesDetails match={propsObj2} />
        </Col>
      </Row>
    </Container>
  );
}

export default CountriesCompare;
