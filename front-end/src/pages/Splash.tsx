import React from "react";
import athlete_pic from "../images/splash_page/images.jpg";
import countries_pic from "../images/splash_page/countries.jpg";
import events_pic from "../images/splash_page/events.jpg";
import { Container, Row, Col, Card, Button } from "react-bootstrap";
import "./Splash.css";

function Splash() {
  return (
    <div>
      <link
        href="https://fonts.googleapis.com/css?family=Lobster"
        rel="stylesheet"
        type="text/css"
      />

      {/* Splash page Image and Title */}
      <div className="img-container">
        <img
          src="https://images.pexels.com/photos/236937/pexels-photo-236937.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260"
          className="img-fluid"
          alt="Responsive image"
        ></img>
        <div className="centered">
          <h1 className="logo_font"> Going For Gold</h1>
          <h3 className="splash-desc">
            Information about Olympic athletes, events, and countries.
          </h3>
        </div>
      </div>

      {/* Our 3 cards displaying our 3 models in the Splash Page */}
      <div className="splash-background">
        <Container>
          <Row xs={1}>
            <Col md={4}>
              <Card
                className="card-splash"
                onClick={() => {
                  window.location.assign("/athletes?p=1");
                }}
              >
                <Card.Img src={athlete_pic} />
                <Card.Body>
                  <Card.Title>
                    <Button href="/athletes?p=1" variant="outline-light">
                      Athletes
                    </Button>
                  </Card.Title>
                  <Card.Text>Learn about record-holding Olympians.</Card.Text>
                </Card.Body>
              </Card>
            </Col>
            <Col md={4}>
              <Card
                className="card-splash"
                onClick={() => {
                  window.location.assign("/countries?p=1");
                }}
              >
                <Card.Img src={countries_pic} />
                <Card.Body>
                  <Card.Title>
                    <Button href="/countries?p=1" variant="outline-light">
                      Countries
                    </Button>
                  </Card.Title>
                  <Card.Text>
                    Learn about different Olympic countries.
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
            <Col md={4}>
              <Card
                className="card-splash"
                onClick={() => {
                  window.location.assign("/events?p=1");
                }}
              >
                <Card.Img src={events_pic} />
                <Card.Body>
                  <Card.Title>
                    <Button href="/events?p=1" variant="outline-light">
                      Events
                    </Button>
                  </Card.Title>
                  <Card.Text>Learn about various Olympic events.</Card.Text>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
}
export default Splash;
