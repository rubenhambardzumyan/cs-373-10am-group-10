import React, { useState } from "react";
import MUIDataTable from "mui-datatables";
import { useQuery } from "react-query";
import Multiselect from "multiselect-react-dropdown";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Dropdown from "react-bootstrap/Dropdown";
import Row from "react-bootstrap/Row";
import Button from "react-bootstrap/Button";
import "./Countries.css";

// used in MUIDataTable; defines the columns that will be shown on the table
const countryCol = [
  { label: "Country ID", name: "country_id", options: { display: false } },
  { label: "Name", name: "country_name" },
  { label: "Capital City", name: "country_capital_city" },
  { label: "Population", name: "country_population" },
  { label: "Region", name: "country_region" },
  { label: "Total Medals", name: "country_medals_total" },
];

// used in MUIDataTable; defines the countries table features
const options = {
  selectableRowsHideCheckboxes: true,
  onRowClick: (rowData: any) => {
    window.location.assign("/countries/" + rowData[0]);
  },
  print: false,
  download: false,
  pagination: false,
  filter: false,
  search: false,
  viewColumns: false,
  sort: false,
};

function Countries() {
  // Obtain parameters from the page URL
  const queryParams = new URLSearchParams(window.location.search);

  // Obtain active filters and sort
  const filteredName = queryParams.getAll("country_name").map((item: any) => ({
    name: item.replace(/U/g, "U-Z").replace(/-U-Z/g, "-U"),
  }));
  const filteredCity = queryParams
    .getAll("country_capital_city")
    .map((item: any) => ({
      name: item.replace(/U/g, "U-Z").replace(/-U-Z/g, "-U"),
    }));
  const filteredPop = queryParams
    .getAll("country_population")
    .map((item: any) => ({ name: item }));
  const filteredRegion = queryParams
    .getAll("country_region")
    .map((item: any) => ({ name: item }));
  const filteredMedals = queryParams
    .getAll("country_medals_total")
    .map((item: any) => ({ name: item }));
  const activeStr = queryParams.get("sort") + " " + queryParams.get("order");

  // Used for compare tool
  const [compare1, setCompare1] = useState(0);
  const [compare2, setCompare2] = useState(0);

  // obtain current pages and pagination
  const pParams = queryParams.get("p");
  const curPageNum = parseInt(pParams != null && pParams != "" ? pParams : "1");
  const [page, setPage] = useState(curPageNum);
  const [totalPages, setTotalPages] = useState(1);
  const [disablePrev, setDisablePrev] = useState(curPageNum == 1);
  const [disableNext, setDisableNext] = useState(false);

  const fetchItems = async () => {
    const res = await fetch(
      `https://api.goingforgold.me/api/countries${window.location.search}`
    );
    const countries = await res.json();
    // Set up paginations
    setTotalPages(countries.pages);
    if (page == countries.pages) {
      if (page == 1) {
        setDisableNext(true);
        setDisablePrev(true);
      } else if (page > 1) {
        setDisableNext(true);
        setDisablePrev(false);
      }
    } else if (page < countries.pages) {
      if (page == 1) {
        setDisableNext(false);
        setDisablePrev(true);
      } else if (page > 1) {
        setDisableNext(false);
        setDisablePrev(false);
      }
    }
    if (window.location.search != "?p=1")
      window.scrollTo({
        top: document.documentElement.scrollHeight,
        behavior: "smooth",
      });
    return countries.body;
  };
  const { data, status } = useQuery(["countries", page], fetchItems);

  //filter options for dropdown buttons
  const region_options = [
    { name: "Asia", id: 1 },
    { name: "Americas", id: 2 },
    { name: "Oceania", id: 3 },
    { name: "Europe", id: 4 },
    { name: "Africa", id: 5 },
  ];
  const population_options = [
    { name: "Show all", id: 7 },
    { name: "0-100,000", id: 1 },
    { name: "100,000-1,000,000", id: 2 },
    { name: "1,000,000-10,000,000", id: 3 },
    { name: "10,000,000-100,000,000", id: 4 },
    { name: "100,000,000-1,000,000,000", id: 5 },
    { name: "Above 1,000,000,000", id: 6 },
  ];
  const name_options = [
    { name: "Show all", id: 7 },
    { name: "A-F", id: 1 },
    { name: "F-K", id: 2 },
    { name: "K-P", id: 3 },
    { name: "P-U", id: 4 },
    { name: "U-Z", id: 5 },
  ];
  const medals_options = [
    { name: "Show all", id: 8 },
    { name: "0-5", id: 1 },
    { name: "5-20", id: 2 },
    { name: "20-50", id: 3 },
    { name: "50-100", id: 4 },
    { name: "100-500", id: 5 },
    { name: "500-1000", id: 6 },
    { name: "More than 1000", id: 7 },
  ];
  let choices = [""];
  choices = data?.map((item: any) => ({
    name: item.country_name,
    id: item.country_id,
  }));

  //handle filters
  function handleSelectRegion(selectedList: any, selectedItem: any) {
    queryParams.append("country_region", selectedItem.name);
    queryParams.set("p", "1");
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/countries?${toStr}`);
  }
  function handleRemoveRegion(selectedList: any, selectedItem: any) {
    const myCurSports_Arr = queryParams.getAll("country_region");
    const new_Arr = myCurSports_Arr.filter(
      (item: any) => item != selectedItem.name
    );
    queryParams.delete("country_region");
    new_Arr.forEach((item: any) => queryParams.append("country_region", item));
    queryParams.set("p", "1");
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/countries?${toStr}`);
  }
  function handleSelectPopulation(selectedList: any, selectedItem: any) {
    if (selectedItem.name === "Show all") {
      queryParams.delete("country_population");
      queryParams.set("p", "1");
      const toStr = queryParams.toString().replace(/\+/g, "%20");
      window.location.assign(`/countries?${toStr}`);
    } else {
      queryParams.set(
        "country_population",
        selectedItem.name.replace(/,/g, "").replace(/Above /g, "")
      );
      queryParams.set("p", "1");
      const toStr = queryParams.toString().replace(/\+/g, "%20");
      window.location.assign(`/countries?${toStr}`);
    }
  }
  function handleSelectMedals(selectedList: any, selectedItem: any) {
    if (selectedItem.name === "Show all") {
      queryParams.delete("country_medals_total");
      queryParams.set("p", "1");
      const toStr = queryParams.toString().replace(/\+/g, "%20");
      window.location.assign(`/countries?${toStr}`);
    } else {
      queryParams.set(
        "country_medals_total",
        selectedItem.name.replace(/More than /g, "")
      );
      queryParams.set("p", "1");
      const toStr = queryParams.toString().replace(/\+/g, "%20");
      window.location.assign(`/countries?${toStr}`);
    }
  }
  function handleSelectName(selectedList: any, selectedItem: any) {
    if (selectedItem.name === "Show all") {
      queryParams.delete("country_name");
      queryParams.set("p", "1");
      const toStr = queryParams.toString().replace(/\+/g, "%20");
      window.location.assign(`/countries?${toStr}`);
    } else {
      queryParams.set("country_name", selectedItem.name.replace(/-Z/g, ""));
      queryParams.set("p", "1");
      const toStr = queryParams.toString().replace(/\+/g, "%20");
      window.location.assign(`/countries?${toStr}`);
    }
  }
  function handleSelectCity(selectedList: any, selectedItem: any) {
    if (selectedItem.name === "Show all") {
      queryParams.delete("country_capital_city");
      queryParams.set("p", "1");
      const toStr = queryParams.toString().replace(/\+/g, "%20");
      window.location.assign(`/countries?${toStr}`);
    } else {
      queryParams.set(
        "country_capital_city",
        selectedItem.name.replace(/-Z/g, "")
      );
      queryParams.set("p", "1");
      const toStr = queryParams.toString().replace(/\+/g, "%20");
      window.location.assign(`/countries?${toStr}`);
    }
  }

  //handle pagination
  const handleFirst = () => {
    queryParams.set("p", "1");
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/countries?${toStr}`);
  };
  const handleLast = () => {
    const lastPage = "" + totalPages;
    queryParams.set("p", lastPage);
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/countries?${toStr}`);
  };
  const handlePrev = () => {
    const prev = "" + (page - 1);
    queryParams.set("p", prev);
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/countries?${toStr}`);
  };
  const handleNext = () => {
    const next = "" + (page + 1);
    queryParams.set("p", next);
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/countries?${toStr}`);
  };

  //handle sort
  function handleCountrySort(selectedItem: any) {
    queryParams.set("sort", selectedItem.split(" ")[0]);
    queryParams.set("order", selectedItem.split(" ")[1]);
    queryParams.set("p", "1");
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/countries?${toStr}`);
  }

  //handle compare tools
  function handleCompare1(selectedList: any, selectedItem: any) {
    setCompare1(selectedItem.id);
  }
  function handleCompare2(selectedList: any, selectedItem: any) {
    setCompare2(selectedItem.id);
  }

  return (
    <div>
      <link
        href="https://fonts.googleapis.com/css?family=Lobster"
        rel="stylesheet"
        type="text/css"
      />
      <link
        href="https://fonts.googleapis.com/css?family=Lobster"
        rel="stylesheet"
        type="text/css"
      />
      <div className="vid-container">
        <img
          className="country-img"
          src="https://images.unsplash.com/photo-1480714378408-67cf0d13bc1b?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=3870&q=80"
          alt="stadium"
        />
        <div className="vid-header">
          <h1>Countries</h1>
        </div>
        <div className="vid-body-1">
          <h5 className="vid-body-text">
            Here is a list of all the countries that have participated in the
            Olympics from 1896 to 2016.
          </h5>
        </div>
        <div className="vid-body-2">
          <h5 className="vid-body-text">
            This list can be sorted and filtered by name, capital city,
            population, country region, and total number of medals.
          </h5>
        </div>
      </div>
      <div className="filter-btns">
        <Container id="below-splash-img">
          <Row>
            <Col>
              <Button
                className="pagination-btn"
                onClick={() => {
                  window.location.assign(`/countriesSearch?p=1`);
                }}
                variant="outline-dark"
              >
                Search
              </Button>
            </Col>
            <Col>
              <Button
                className="pagination-btn"
                onClick={() => {
                  window.location.assign(`/countries?p=1`);
                }}
                variant="outline-dark"
              >
                View All
              </Button>
            </Col>
          </Row>
          <Row>
            <h5 className="filter-header">Filters</h5>
          </Row>
          <Row md={5}>
            <Col>
              <Multiselect
                options={name_options} // Options to display in the dropdown
                onSelect={handleSelectName} // Function will trigger on select event
                displayValue="name" // Property name to display in the dropdown options
                placeholder="Name"
                singleSelect
                selectedValues={filteredName}
              />
            </Col>
            <Col>
              <Multiselect
                options={name_options} // Options to display in the dropdown
                onSelect={handleSelectCity} // Function will trigger on select event
                displayValue="name" // Property name to display in the dropdown options
                placeholder="Capital City"
                singleSelect
                selectedValues={filteredCity}
              />
            </Col>
            <Col>
              <Multiselect
                options={population_options} // Options to display in the dropdown
                onSelect={handleSelectPopulation} // Function will trigger on select event
                displayValue="name" // Property name to display in the dropdown options
                placeholder="Population"
                singleSelect
                selectedValues={filteredPop}
              />
            </Col>
            <Col>
              <Multiselect
                options={region_options} // Options to display in the dropdown
                onSelect={handleSelectRegion} // Function will trigger on select event
                onRemove={handleRemoveRegion} // Function will trigger on remove event
                displayValue="name" // Property name to display in the dropdown options
                placeholder="Region"
                selectedValues={filteredRegion}
              />
            </Col>
            <Col>
              <Multiselect
                options={medals_options} // Options to display in the dropdown
                onSelect={handleSelectMedals} // Function will trigger on select event
                displayValue="name" // Property name to display in the dropdown options
                placeholder="Total Medals"
                singleSelect
                selectedValues={filteredMedals}
              />
            </Col>
          </Row>
          <Row>
            <h5 className="filter-header">Sorts</h5>
          </Row>
          <Row>
            {/* these are the sorts */}
            <Col>
              <Dropdown>
                <Dropdown.Toggle style={{ width: "100%" }}>
                  Name
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item
                    onClick={() => handleCountrySort("name asc")}
                    active={activeStr === "name asc"}
                  >
                    Ascending
                  </Dropdown.Item>
                  <Dropdown.Item
                    onClick={() => handleCountrySort("name desc")}
                    active={activeStr === "name desc"}
                  >
                    Descending
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </Col>
            <Col>
              <Dropdown>
                <Dropdown.Toggle style={{ width: "100%" }}>
                  Capital City
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item
                    onClick={() => handleCountrySort("capital asc")}
                    active={activeStr === "capital asc"}
                  >
                    Ascending
                  </Dropdown.Item>
                  <Dropdown.Item
                    onClick={() => handleCountrySort("capital desc")}
                    active={activeStr === "capital desc"}
                  >
                    Descending
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </Col>
            <Col>
              <Dropdown>
                <Dropdown.Toggle style={{ width: "100%" }}>
                  Population
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item
                    onClick={() => handleCountrySort("population asc")}
                    active={activeStr === "population asc"}
                  >
                    Ascending
                  </Dropdown.Item>
                  <Dropdown.Item
                    onClick={() => handleCountrySort("population desc")}
                    active={activeStr === "population desc"}
                  >
                    Descending
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </Col>
            <Col>
              <Dropdown>
                <Dropdown.Toggle style={{ width: "100%" }}>
                  Region
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item
                    onClick={() => handleCountrySort("region asc")}
                    active={activeStr === "region asc"}
                  >
                    Ascending
                  </Dropdown.Item>
                  <Dropdown.Item
                    onClick={() => handleCountrySort("region desc")}
                    active={activeStr === "region desc"}
                  >
                    Descending
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </Col>
            <Col>
              <Dropdown>
                <Dropdown.Toggle style={{ width: "100%" }}>
                  Total Medals
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item
                    onClick={() => handleCountrySort("total-medals asc")}
                    active={activeStr === "total-medals asc"}
                  >
                    Ascending
                  </Dropdown.Item>
                  <Dropdown.Item
                    onClick={() => handleCountrySort("total-medals desc")}
                    active={activeStr === "total-medals desc"}
                  >
                    Descending
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </Col>
          </Row>
          <Row>
            <h5 className="filter-header">Compare</h5>
          </Row>
          <Row>
            <Col>
              <Multiselect
                options={choices} // Options to display in the dropdown
                onSelect={handleCompare1} // Function will trigger on select event
                displayValue="name" // Property name to display in the dropdown options
                placeholder="Name"
                singleSelect
              />
            </Col>
            <Col>
              <Multiselect
                options={choices} // Options to display in the dropdown
                onSelect={handleCompare2} // Function will trigger on select event
                displayValue="name" // Property name to display in the dropdown options
                placeholder="Name"
                singleSelect
              />
            </Col>
            <Col md={2}>
              <Button
                className="compare-btn"
                variant="outline-dark"
                href={`/countries/compare?c1=${compare1}&c2=${compare2}`}
              >
                Compare
              </Button>
            </Col>
          </Row>
        </Container>
      </div>
      <div>
        {status === "error" && (
          <h3 className="waiting">Error Fetching data!</h3>
        )}
        {status === "loading" && <h3 className="waiting">Fetching data...</h3>}
        {/* this is the data table with some pieces of information about the countries */}
        <MUIDataTable
          title={""}
          data={data?.map((country: any) => {
            return [
              country?.country_id,
              country?.country_name,
              country?.country_capital_city &&
                country.country_capital_city.replace(/[[\]']/g, ""),
              country?.country_population,
              country?.country_region,
              parseInt(country?.country_medals_total),
            ];
          })}
          columns={countryCol}
          options={options}
        />
      </div>
      <div className="disp-footer">
        <div className="disp-instance">
          <h6>Showing {data?.length} Instances</h6>
          <h6>
            Page {page} of {totalPages}
          </h6>
        </div>
        <div className="disp-pagination">
          <Button
            className="pagination-btn"
            onClick={handleFirst}
            disabled={disablePrev}
            variant="outline-dark"
          >
            First
          </Button>
          <Button
            className="pagination-btn"
            onClick={handlePrev}
            disabled={disablePrev}
            variant="outline-dark"
          >
            Prev
          </Button>
          <Button
            className="pagination-btn"
            onClick={handlePrev}
            variant="outline-dark"
            disabled={disablePrev}
          >
            {page - 1}
          </Button>
          <Button className="pagination-btn" variant="outline-dark" active>
            {page}
          </Button>
          <Button
            className="pagination-btn"
            onClick={handleNext}
            variant="outline-dark"
            disabled={disableNext}
          >
            {page + 1}
          </Button>
          <Button
            className="pagination-btn"
            onClick={handleNext}
            disabled={disableNext}
            variant="outline-dark"
          >
            Next
          </Button>
          <Button
            className="pagination-btn"
            onClick={handleLast}
            disabled={disableNext}
            variant="outline-dark"
          >
            Last
          </Button>
        </div>
      </div>
    </div>
  );
}
export default Countries;
