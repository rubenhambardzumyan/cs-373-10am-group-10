import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import { useQuery } from "react-query";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import "./Search.css";
import Highlighter from "react-highlight-words";

function Search() {
  {
    /* get the results from search for all 3 models */
  }
  const [athletePages, setAthletePages] = useState(1);
  const [countryPages, setCountryPages] = useState(1);
  const [eventPages, setEventsPages] = useState(1);
  const searchValue = window.location.search.substring(7);

  // Obtain the results of the search parameter from all our APIs
  const fetchAthletes = async () => {
    const res = await fetch(
      `https://api.goingforgold.me/api/athletes${window.location.search}`
    );
    const athletes = await res.json();
    setAthletePages(athletes.pages);
    return athletes.body;
  };
  const athletesQuery = useQuery("athletes", fetchAthletes);
  const fetchCountries = async () => {
    const res = await fetch(
      `https://api.goingforgold.me/api/countries${window.location.search}`
    );
    const countries = await res.json();
    setCountryPages(countries.pages);
    return countries.body;
  };
  const countriesQuery = useQuery("countries", fetchCountries);
  const fetchEvents = async () => {
    const res = await fetch(
      `https://api.goingforgold.me/api/events${window.location.search}`
    );
    const events = await res.json();
    setEventsPages(events.pages);
    return events.body;
  };
  const eventsQuery = useQuery("events", fetchEvents);
  return (
    <div>
      <h1 className="title">Search Results for</h1>
      <h1 className="model-name">{searchValue}</h1>
      <h2 className="model-name">Athletes</h2>
      <Container>
        <Row
          xs="auto"
          sm="auto"
          md="auto"
          lg="auto"
          xl="auto"
          xxl={4}
          className="athlete-row"
        >
          {/* display all the athletes found with search with highlighted info*/}
          {athletesQuery.status === "error" && (
            <h3 className="waiting">Error Fetching data!</h3>
          )}
          {athletesQuery.status === "loading" && (
            <h3 className="waiting">Fetching data...</h3>
          )}
          {athletesQuery.status === "success" && athletePages == 0 && (
            <h3 className="waiting">No results found</h3>
          )}
          {athletesQuery.status === "success" &&
            athletesQuery.data?.map((athlete: any) => (
              <Col key={athlete.athlete_id}>
                <Card className="athlete-card-style">
                  <Card.Img
                    src={athlete.athlete_image}
                    className="athlete-card-image-style"
                  />
                  <Card.Body>
                    <Card.Title>
                      <h3>
                        <Button
                          variant="outline-light"
                          href={`/athletes/${athlete.athlete_id}`}
                        >
                          {
                            <Highlighter
                              highlightClassName="searchHighlight"
                              searchWords={searchValue.split(" ")}
                              textToHighlight={athlete.athlete_name}
                            />
                          }
                        </Button>
                      </h3>
                    </Card.Title>
                    <Card.Text>
                      Sport:{" "}
                      {
                        <Highlighter
                          highlightClassName="searchHighlight"
                          searchWords={searchValue.split(" ")}
                          textToHighlight={athlete.athlete_sport}
                        />
                      }
                    </Card.Text>
                    <Card.Text>
                      Country:{" "}
                      {
                        <Highlighter
                          highlightClassName="searchHighlight"
                          searchWords={searchValue.split(" ")}
                          textToHighlight={athlete.country_name}
                        />
                      }
                    </Card.Text>
                    <Card.Text>
                      {
                        <Highlighter
                          highlightClassName="searchHighlight"
                          searchWords={searchValue.split(" ")}
                          textToHighlight={athlete.num_gold_medals}
                        />
                      }{" "}
                      Gold Medal
                    </Card.Text>
                    <Card.Text>
                      {
                        <Highlighter
                          highlightClassName="searchHighlight"
                          searchWords={searchValue.split(" ")}
                          textToHighlight={athlete.num_silver_medals}
                        />
                      }{" "}
                      Silver Medal
                    </Card.Text>
                    <Card.Text>
                      {
                        <Highlighter
                          highlightClassName="searchHighlight"
                          searchWords={searchValue.split(" ")}
                          textToHighlight={athlete.num_bronze_medals}
                        />
                      }{" "}
                      Bronze Medal
                    </Card.Text>
                  </Card.Body>
                </Card>
              </Col>
            ))}
        </Row>
        {/* if there are more results, return a link to next page */}
        {athletePages > 1 && (
          <Row>
            <Card className="athlete-card-style">
              <Card.Text>
                There are {athletePages} more pages of results for Athletes.
                Click below to see more matches.
              </Card.Text>
              <Card.Text>
                <Button
                  variant="outline-light"
                  href={`/athletes?p=2&q=${searchValue}`}
                >
                  View Results
                </Button>
              </Card.Text>
            </Card>
          </Row>
        )}
      </Container>
      <h2 className="model-name">Countries</h2>
      <Container>
        <Row
          xs="auto"
          sm="auto"
          md="auto"
          lg="auto"
          xl="auto"
          xxl={4}
          className="athlete-row"
        >
          {/* return the found countries through search with highlghted info*/}
          {countriesQuery.status === "error" && (
            <h3 className="waiting">Error Fetching data!</h3>
          )}
          {countriesQuery.status === "loading" && (
            <h3 className="waiting">Fetching data...</h3>
          )}
          {countriesQuery.status === "success" && countryPages == 0 && (
            <h3 className="waiting">No results found</h3>
          )}
          {countriesQuery.status === "success" &&
            countriesQuery.data?.map((country: any) => (
              <Col key={country.country_id}>
                <Card className="athlete-card-style">
                  <Card.Img
                    src={country.country_flag}
                    className="flag-img-view"
                  />
                  <Card.Body>
                    <Card.Title>
                      <h3>
                        <Button
                          variant="outline-light"
                          href={`/countries/${country.country_id}`}
                        >
                          {
                            <Highlighter
                              highlightClassName="searchHighlight"
                              searchWords={searchValue.split(" ")}
                              textToHighlight={country.country_name}
                            />
                          }
                        </Button>
                      </h3>
                    </Card.Title>
                    <Card.Text>
                      Capital City:{" "}
                      {
                        <Highlighter
                          highlightClassName="searchHighlight"
                          searchWords={searchValue.split(" ")}
                          textToHighlight={country.country_capital_city}
                        />
                      }
                    </Card.Text>
                    <Card.Text>
                      Population:{" "}
                      {
                        <Highlighter
                          highlightClassName="searchHighlight"
                          searchWords={searchValue.split(" ")}
                          textToHighlight={"" + country.country_population}
                        />
                      }
                    </Card.Text>
                    <Card.Text>
                      Region:{" "}
                      {
                        <Highlighter
                          highlightClassName="searchHighlight"
                          searchWords={searchValue.split(" ")}
                          textToHighlight={country.country_region}
                        />
                      }
                    </Card.Text>
                    <Card.Text>
                      Total Medals:{" "}
                      {
                        <Highlighter
                          highlightClassName="searchHighlight"
                          searchWords={searchValue.split(" ")}
                          textToHighlight={"" + country.country_medals_total}
                        />
                      }
                    </Card.Text>
                  </Card.Body>
                </Card>
              </Col>
            ))}
        </Row>
        {/* return the next page of countries */}
        {countryPages > 1 && (
          <Row>
            <Card className="athlete-card-style">
              <Card.Text>
                There are {countryPages} more pages of results for Countries.
                Click below to see more matches.
              </Card.Text>
              <Card.Text>
                <Button
                  variant="outline-light"
                  href={`/countriesSearch?p=2&q=${searchValue}`}
                >
                  View Results
                </Button>
              </Card.Text>
            </Card>
          </Row>
        )}
      </Container>
      <h2 className="model-name">Events</h2>
      <Container>
        <Row
          xs="auto"
          sm="auto"
          md="auto"
          lg="auto"
          xl="auto"
          xxl={4}
          className="athlete-row"
        >
          {/*return the events found */}
          {eventsQuery.status === "error" && (
            <h3 className="waiting">Error Fetching data!</h3>
          )}
          {eventsQuery.status === "loading" && (
            <h3 className="waiting">Fetching data...</h3>
          )}
          {eventsQuery.status === "success" && eventPages == 0 && (
            <h3 className="waiting">No results found</h3>
          )}
          {eventsQuery.status === "success" &&
            eventsQuery.data?.map((event: any) => (
              <Col key={event.event_id}>
                <Card className="athlete-card-style">
                  <Card.Body>
                    <Card.Title>
                      <h3>
                        <Button
                          variant="outline-light"
                          href={`/events/${event.event_id}`}
                        >
                          {
                            <Highlighter
                              highlightClassName="searchHighlight"
                              searchWords={searchValue.split(" ")}
                              textToHighlight={event.event_name}
                            />
                          }
                        </Button>
                      </h3>
                    </Card.Title>
                    <Card.Text>
                      Sport:{" "}
                      {
                        <Highlighter
                          highlightClassName="searchHighlight"
                          searchWords={searchValue.split(" ")}
                          textToHighlight={event.sport}
                        />
                      }
                    </Card.Text>
                    <Card.Text>
                      Season:{" "}
                      {
                        <Highlighter
                          highlightClassName="searchHighlight"
                          searchWords={searchValue.split(" ")}
                          textToHighlight={event.season}
                        />
                      }
                    </Card.Text>
                    <Card.Text>
                      Most Recent Olympics:{" "}
                      {
                        <Highlighter
                          highlightClassName="searchHighlight"
                          searchWords={searchValue.split(" ")}
                          textToHighlight={event.most_recent_olympics}
                        />
                      }
                    </Card.Text>
                    <Card.Text>
                      Sport Type:{" "}
                      {
                        <Highlighter
                          highlightClassName="searchHighlight"
                          searchWords={searchValue.split(" ")}
                          textToHighlight={event.sport_type}
                        />
                      }
                    </Card.Text>
                  </Card.Body>
                </Card>
              </Col>
            ))}
        </Row>
        {/*return the next page of events if there are additional */}
        {eventPages > 1 && (
          <Row>
            <Card className="athlete-card-style">
              <Card.Text>
                There are {eventPages} more pages of results for Events. Click
                below to see more matches.
              </Card.Text>
              <Card.Text>
                <Button
                  variant="outline-light"
                  href={`/eventsSearch?p=2&q=${searchValue}`}
                >
                  View Results
                </Button>
              </Card.Text>
            </Card>
          </Row>
        )}
      </Container>
    </div>
  );
}

export default Search;
