import React, { useState } from "react";
import Card from "react-bootstrap/Card";
import CardGroup from "react-bootstrap/CardGroup";
import Col from "react-bootstrap/Col";
import Dropdown from "react-bootstrap/Dropdown";
import Row from "react-bootstrap/Row";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import "./Athletes.css";
import { useQuery } from "react-query";
import Multiselect from "multiselect-react-dropdown";
import FormControl from "react-bootstrap/FormControl";
import { AiOutlineSearch } from "react-icons/ai";
import Highlighter from "react-highlight-words";

function Athletes() {
  // Obtain parameters from the page URL
  const queryParams = new URLSearchParams(window.location.search);

  // Obtain active filters and sort
  const filteredSport = queryParams
    .getAll("athlete_sport")
    .map((item: any) => ({ name: item }));
  const filteredCountry = queryParams
    .getAll("country_name")
    .map((item: any) => ({ name: item }));
  const filteredName = queryParams.getAll("athlete_name").map((item: any) => ({
    name: item.replace(/U/g, "U-Z").replace(/-U-Z/g, "-U"),
  }));
  const filteredGold = queryParams
    .getAll("num_gold_medals")
    .map((item: any) => ({ name: item }));
  const filteredSilver = queryParams
    .getAll("num_silver_medals")
    .map((item: any) => ({ name: item }));
  const filteredBronze = queryParams
    .getAll("num_bronze_medals")
    .map((item: any) => ({ name: item }));
  const activeStr = queryParams.get("sort") + " " + queryParams.get("order");

  // obtain active search
  const qParams = queryParams.get("q");
  const searchedStr = qParams != null && qParams != "" ? qParams : "";

  // Used for compare tool
  const [compare1, setCompare1] = useState(0);
  const [compare2, setCompare2] = useState(0);

  // obtain current pages and pagination
  const pParams = queryParams.get("p");
  const curPageNum = parseInt(pParams != null && pParams != "" ? pParams : "1");
  const [page, setPage] = useState(curPageNum);
  const [totalPages, setTotalPages] = useState(1);
  const [disablePrev, setDisablePrev] = useState(curPageNum == 1);
  const [disableNext, setDisableNext] = useState(false);

  //search
  const textInput: any = React.useRef();

  // fetches the athletes using our API
  const fetchAthletes = async () => {
    const res = await fetch(
      `https://api.goingforgold.me/api/athletes${window.location.search}`
    );
    const athletes = await res.json();
    // Set up paginations
    setTotalPages(athletes.pages);
    if (page == athletes.pages) {
      if (page == 1) {
        setDisableNext(true);
        setDisablePrev(true);
      } else if (page > 1) {
        setDisableNext(true);
        setDisablePrev(false);
      }
    } else if (page < athletes.pages) {
      if (page == 1) {
        setDisableNext(false);
        setDisablePrev(true);
      } else if (page > 1) {
        setDisableNext(false);
        setDisablePrev(false);
      }
    }
    return athletes.body;
  };
  const { data, status } = useQuery(["countries", page], fetchAthletes);

  //filter options for dropdown buttons
  const sport_options = [
    { name: "Alpine skiing", id: 1 },
    { name: "Archery", id: 2 },
    { name: "Athletics", id: 3 },
    { name: "Badminton", id: 4 },
    { name: "Basketball", id: 5 },
    { name: "Biathlon", id: 6 },
    { name: "Bobsleigh", id: 7 },
    { name: "Boxing", id: 8 },
    { name: "Canoeing", id: 9 },
    { name: "Cross-country skiing", id: 10 },
    { name: "Curling", id: 11 },
    { name: "Cycling", id: 12 },
    { name: "Diving", id: 13 },
    { name: "Equestrian", id: 14 },
    { name: "Fencing", id: 15 },
    { name: "Figure skating", id: 17 },
    { name: "Football", id: 18 },
    { name: "Golf", id: 20 },
    { name: "Gymnastics", id: 21 },
    { name: "Handball", id: 22 },
    { name: "Ice hockey", id: 23 },
    { name: "Judo", id: 24 },
    { name: "Luge", id: 25 },
    { name: "Rowing", id: 28 },
    { name: "Sailing", id: 30 },
    { name: "Shooting", id: 31 },
    { name: "Skeleton", id: 33 },
    { name: "Snowboarding", id: 35 },
    { name: "Speed skating", id: 36 },
    { name: "Swimming", id: 37 },
    { name: "Table tennis", id: 39 },
    { name: "Taekwondo", id: 40 },
    { name: "Tennis", id: 41 },
    { name: "Triathlon", id: 42 },
    { name: "Volleyball", id: 43 },
    { name: "Water polo", id: 44 },
    { name: "Weightlifting", id: 45 },
    { name: "Wrestling", id: 46 },
  ];
  const country_options = [
    { name: "Afghanistan", id: 1 },
    { name: "Algeria", id: 2 },
    { name: "Argentina", id: 3 },
    { name: "Armenia", id: 4 },
    { name: "Australia", id: 5 },
    { name: "Austria", id: 6 },
    { name: "Azerbaijan", id: 7 },
    { name: "Bahamas", id: 8 },
    { name: "Bahrain", id: 9 },
    { name: "Barbados", id: 10 },
    { name: "Belarus", id: 11 },
    { name: "Belgium", id: 12 },
    { name: "Bermuda", id: 13 },
    { name: "Brazil", id: 14 },
    { name: "Bulgaria", id: 15 },
    { name: "Brazil", id: 16 },
    { name: "Cameroon", id: 17 },
    { name: "Canada", id: 18 },
    { name: "Chile", id: 19 },
    { name: "China", id: 21 },
    { name: "Colombia", id: 22 },
    { name: "Costa Rica", id: 23 },
    { name: "Croatia", id: 24 },
    { name: "Cuba", id: 25 },
    { name: "Czech Republic", id: 26 },
    { name: "Czechoslovakia", id: 27 },
    { name: "Denmark", id: 28 },
    { name: "Djibouti", id: 29 },
    { name: "Dominican Republic", id: 30 },
    { name: "Ecuador", id: 31 },
    { name: "Egypt", id: 32 },
    { name: "Eritrea", id: 33 },
    { name: "Estonia", id: 34 },
    { name: "Ethiopia", id: 35 },
    { name: "Fiji", id: 36 },
    { name: "Finland", id: 37 },
    { name: "France", id: 38 },
    { name: "Gabon", id: 39 },
    { name: "Georgia", id: 40 },
    { name: "Germany", id: 41 },
    { name: "Ghana", id: 42 },
    { name: "Great Britain", id: 43 },
    { name: "Greece", id: 44 },
    { name: "Grenada", id: 45 },
    { name: "Guatemala", id: 46 },
    { name: "Haiti", id: 47 },
    { name: "Hong Kong", id: 48 },
    { name: "Hungary", id: 49 },
    { name: "Iceland", id: 50 },
    { name: "India", id: 51 },
    { name: "Indonesia", id: 52 },
    { name: "Iran", id: 53 },
    { name: "Iraq", id: 54 },
    { name: "Ireland", id: 55 },
    { name: "Israel", id: 56 },
    { name: "Italy", id: 57 },
    { name: "Jamaica", id: 58 },
    { name: "Japan", id: 59 },
    { name: "Jordan", id: 60 },
    { name: "Kazakhstan", id: 61 },
    { name: "Kenya", id: 62 },
    { name: "Kosovo", id: 63 },
    { name: "North Korea", id: 64 },
    { name: "South Korea", id: 65 },
    { name: "Kuwait", id: 66 },
    { name: "Kyrgyzstan", id: 67 },
    { name: "Latvia", id: 68 },
    { name: "Lebanon", id: 69 },
    { name: "Lithuania", id: 70 },
    { name: "Luxembourg", id: 71 },
    { name: "Malaysia", id: 72 },
    { name: "Mexico", id: 73 },
    { name: "Mongolia", id: 74 },
    { name: "Morocco", id: 75 },
    { name: "Netherlands", id: 76 },
    { name: "New Zealand", id: 77 },
    { name: "Nigeria", id: 78 },
    { name: "Norway", id: 79 },
    { name: "Pakistan", id: 80 },
    { name: "Philippines", id: 81 },
    { name: "Poland", id: 82 },
    { name: "Portugal", id: 83 },
    { name: "Puerto Rico", id: 84 },
    { name: "Qatar", id: 85 },
    { name: "Romania", id: 86 },
    { name: "Russia", id: 87 },
    { name: "Saudi Arabia", id: 88 },
    { name: "Serbia", id: 89 },
    { name: "Singapore", id: 90 },
    { name: "Slovakia", id: 91 },
    { name: "Slovenia", id: 92 },
    { name: "South Africa", id: 93 },
    { name: "Spain", id: 94 },
    { name: "Sri Lanka", id: 95 },
    { name: "Sudan", id: 96 },
    { name: "Sweden", id: 97 },
    { name: "Switzerland", id: 98 },
    { name: "Chinese Taipei", id: 99 },
    { name: "Tajikistan", id: 100 },
    { name: "Tanzania", id: 101 },
    { name: "Thailand", id: 102 },
    { name: "Tonga", id: 103 },
    { name: "Tunisia", id: 104 },
    { name: "Turkey", id: 105 },
    { name: "Uganda", id: 106 },
    { name: "Ukraine", id: 107 },
    { name: "United Arab Emirates", id: 108 },
    { name: "United States", id: 109 },
    { name: "USA", id: 109 },
    { name: "Uruguay", id: 110 },
    { name: "Uzbekistan", id: 111 },
    { name: "Venezuela", id: 112 },
    { name: "Vietnam", id: 113 },
    { name: "Yugoslavia", id: 114 },
    { name: "Zambia", id: 115 },
  ];
  const name_options = [
    { name: "Show all", id: 7 },
    { name: "A-F", id: 1 },
    { name: "F-K", id: 2 },
    { name: "K-P", id: 3 },
    { name: "P-U", id: 4 },
    { name: "U-Z", id: 5 },
  ];
  const medals_options = [
    { name: "0", id: 10 },
    { name: "1", id: 1 },
    { name: "2", id: 2 },
    { name: "3", id: 3 },
    { name: "4", id: 4 },
    { name: "5", id: 5 },
    { name: "6", id: 6 },
    { name: "7", id: 7 },
    { name: "8", id: 8 },
    { name: "9", id: 9 },
  ];
  let choices = [""];
  choices = data?.map((item: any) => ({
    name: item.athlete_name,
    id: item.athlete_id,
  }));

  //handle filters
  function handleSelectSport(selectedList: any, selectedItem: any) {
    queryParams.append("athlete_sport", selectedItem.name);
    queryParams.set("p", "1");
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/athletes?${toStr}`);
  }
  function handleRemoveSport(selectedList: any, selectedItem: any) {
    const myCurSports_Arr = queryParams.getAll("athlete_sport");
    const new_Arr = myCurSports_Arr.filter(
      (item: any) => item != selectedItem.name
    );
    queryParams.delete("athlete_sport");
    new_Arr.forEach((item: any) => queryParams.append("athlete_sport", item));
    queryParams.set("p", "1");
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/athletes?${toStr}`);
  }
  function handleSelectCountry(selectedList: any, selectedItem: any) {
    queryParams.append("country_name", selectedItem.name);
    queryParams.set("p", "1");
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/athletes?${toStr}`);
  }
  function handleRemoveCountry(selectedList: any, selectedItem: any) {
    const all_countries = queryParams.getAll("country_name");
    const new_Arr = all_countries.filter(
      (item: any) => item != selectedItem.name
    );
    queryParams.delete("country_name");
    new_Arr.forEach((item: any) => queryParams.append("country_name", item));
    queryParams.set("p", "1");
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/athletes?${toStr}`);
  }
  function handleSelectGold(selectedList: any, selectedItem: any) {
    queryParams.append("num_gold_medals", selectedItem.name);
    queryParams.set("p", "1");
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/athletes?${toStr}`);
  }
  function handleRemoveGold(selectedList: any, selectedItem: any) {
    const all_countries = queryParams.getAll("num_gold_medals");
    const new_Arr = all_countries.filter(
      (item: any) => item != selectedItem.name
    );
    queryParams.delete("num_gold_medals");
    new_Arr.forEach((item: any) => queryParams.append("num_gold_medals", item));
    queryParams.set("p", "1");
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/athletes?${toStr}`);
  }
  function handleSelectSilver(selectedList: any, selectedItem: any) {
    queryParams.append("num_silver_medals", selectedItem.name);
    queryParams.set("p", "1");
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/athletes?${toStr}`);
  }
  function handleRemoveSilver(selectedList: any, selectedItem: any) {
    const all_countries = queryParams.getAll("num_silver_medals");
    const new_Arr = all_countries.filter(
      (item: any) => item != selectedItem.name
    );
    queryParams.delete("num_silver_medals");
    new_Arr.forEach((item: any) =>
      queryParams.append("num_silver_medals", item)
    );
    queryParams.set("p", "1");
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/athletes?${toStr}`);
  }
  function handleSelectBronze(selectedList: any, selectedItem: any) {
    queryParams.append("num_bronze_medals", selectedItem.name);
    queryParams.set("p", "1");
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/athletes?${toStr}`);
  }
  function handleRemoveBronze(selectedList: any, selectedItem: any) {
    const all_countries = queryParams.getAll("num_bronze_medals");
    const new_Arr = all_countries.filter(
      (item: any) => item != selectedItem.name
    );
    queryParams.delete("num_bronze_medals");
    new_Arr.forEach((item: any) =>
      queryParams.append("num_bronze_medals", item)
    );
    queryParams.set("p", "1");
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/athletes?${toStr}`);
  }
  function handleSelectName(selectedList: any, selectedItem: any) {
    if (selectedItem.name === "Show all") {
      queryParams.delete("athlete_name");
      queryParams.set("p", "1");
      const toStr = queryParams.toString().replace(/\+/g, "%20");
      window.location.assign(`/athletes?${toStr}`);
    } else {
      queryParams.set("athlete_name", selectedItem.name.replace(/-Z/g, ""));
      queryParams.set("p", "1");
      const toStr = queryParams.toString().replace(/\+/g, "%20");
      window.location.assign(`/athletes?${toStr}`);
    }
  }

  //handle pagination
  const handleFirst = () => {
    queryParams.set("p", "1");
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/athletes?${toStr}`);
  };
  const handleLast = () => {
    const lastPage = "" + totalPages;
    queryParams.set("p", lastPage);
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/athletes?${toStr}`);
  };
  const handlePrev = () => {
    const prev = "" + (page - 1);
    queryParams.set("p", prev);
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/athletes?${toStr}`);
  };
  const handleNext = () => {
    const next = "" + (page + 1);
    queryParams.set("p", next);
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/athletes?${toStr}`);
  };
  //handle search
  function handleSearch() {
    queryParams.set("q", textInput.current.value);
    queryParams.set("p", "1");
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/athletes?${toStr}`);
  }

  //handle sort
  function handleAthletesSort(selectedItem: any) {
    queryParams.set("sort", selectedItem.split(" ")[0]);
    queryParams.set("order", selectedItem.split(" ")[1]);
    queryParams.set("p", "1");
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/athletes?${toStr}`);
  }

  //handle compare tools
  function handleCompare1(selectedList: any, selectedItem: any) {
    setCompare1(selectedItem.id);
  }
  function handleCompare2(selectedList: any, selectedItem: any) {
    setCompare2(selectedItem.id);
  }

  return (
    <div>
      <link
        href="https://fonts.googleapis.com/css?family=Lobster"
        rel="stylesheet"
        type="text/css"
      />
      <CardGroup className="container">
        <Row className="container">
          <Card
            style={{ backgroundColor: "#ffffff00", borderColor: "#ffffff00" }}
          >
            <Card.Body>
              <Card.Title>
                <h1 className="athlete-header">Athletes</h1>
              </Card.Title>
              <Card.Text className="athlete-body">
                Here is a list of all the athletes who have participated in the
                Olympics from 1896 to 2016.
              </Card.Text>
              <Card.Text className="athlete-body">
                This list can be sorted by athlete name, sport, and country.
                This list can also be filtered by name, sport, country and
                number of medals.
              </Card.Text>
            </Card.Body>
          </Card>
        </Row>
      </CardGroup>

      <Container>
        <Row>
          <h5 className="header">Filters</h5>
        </Row>
        <Row md={6}>
          <Col>
            <Multiselect
              options={name_options} // Options to display in the dropdown
              onSelect={handleSelectName} // Function will trigger on select event
              displayValue="name" // Property name to display in the dropdown options
              placeholder="Name"
              singleSelect
              selectedValues={filteredName}
            />
          </Col>
          <Col>
            <Multiselect
              options={sport_options} // Options to display in the dropdown
              onSelect={handleSelectSport} // Function will trigger on select event
              onRemove={handleRemoveSport} // Function will trigger on remove event
              displayValue="name" // Property name to display in the dropdown options
              placeholder="Sport"
              selectedValues={filteredSport}
            />
          </Col>
          <Col>
            <Multiselect
              options={country_options} // Options to display in the dropdown
              onSelect={handleSelectCountry} // Function will trigger on select event
              onRemove={handleRemoveCountry} // Function will trigger on remove event
              displayValue="name" // Property name to display in the dropdown options
              placeholder="Country"
              selectedValues={filteredCountry}
            />
          </Col>
          <Col>
            <Multiselect
              options={medals_options} // Options to display in the dropdown
              onSelect={handleSelectGold} // Function will trigger on select event
              onRemove={handleRemoveGold} // Function will trigger on remove event
              displayValue="name" // Property name to display in the dropdown options
              placeholder="Gold Medals"
              selectedValues={filteredGold}
            />
          </Col>
          <Col>
            <Multiselect
              options={medals_options} // Options to display in the dropdown
              onSelect={handleSelectSilver} // Function will trigger on select event
              onRemove={handleRemoveSilver} // Function will trigger on remove event
              displayValue="name" // Property name to display in the dropdown options
              placeholder="Silver Medals"
              selectedValues={filteredSilver}
            />
          </Col>
          <Col>
            <Multiselect
              options={medals_options} // Options to display in the dropdown
              onSelect={handleSelectBronze} // Function will trigger on select event
              onRemove={handleRemoveBronze} // Function will trigger on remove event
              displayValue="name" // Property name to display in the dropdown options
              placeholder="Bronze Medals"
              selectedValues={filteredBronze}
            />
          </Col>
        </Row>
        <Row>
          <h5 className="header">Sorts</h5>
        </Row>
        <Row>
          <Col>
            {/* these are the dropdown menus for sorts */}
            <Dropdown>
              <Dropdown.Toggle style={{ width: "100%" }}>Name</Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item
                  onClick={() => handleAthletesSort("name asc")}
                  active={activeStr === "name asc"}
                >
                  Ascending
                </Dropdown.Item>
                <Dropdown.Item
                  onClick={() => handleAthletesSort("name desc")}
                  active={activeStr === "name desc"}
                >
                  Descending
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Col>
          <Col>
            <Dropdown>
              <Dropdown.Toggle style={{ width: "100%" }}>Sport</Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item
                  onClick={() => handleAthletesSort("sport asc")}
                  active={activeStr === "sport asc"}
                >
                  Ascending
                </Dropdown.Item>
                <Dropdown.Item
                  onClick={() => handleAthletesSort("sport desc")}
                  active={activeStr === "sport desc"}
                >
                  Descending
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Col>
          <Col>
            <Dropdown>
              <Dropdown.Toggle style={{ width: "100%" }}>
                Country
              </Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item
                  onClick={() => handleAthletesSort("country asc")}
                  active={activeStr === "country asc"}
                >
                  Ascending
                </Dropdown.Item>
                <Dropdown.Item
                  onClick={() => handleAthletesSort("country desc")}
                  active={activeStr === "country desc"}
                >
                  Descending
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Col>
          <Col>
            <Dropdown>
              <Dropdown.Toggle style={{ width: "100%" }}>
                Gold Medals
              </Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item
                  onClick={() => handleAthletesSort("gold-medals asc")}
                  active={activeStr === "gold-medals asc"}
                >
                  Ascending
                </Dropdown.Item>
                <Dropdown.Item
                  onClick={() => handleAthletesSort("gold-medals desc")}
                  active={activeStr === "gold-medals desc"}
                >
                  Descending
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Col>
          <Col>
            <Dropdown>
              <Dropdown.Toggle style={{ width: "100%" }}>
                Silver Medals
              </Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item
                  onClick={() => handleAthletesSort("silver-medals asc")}
                  active={activeStr === "silver-medals asc"}
                >
                  Ascending
                </Dropdown.Item>
                <Dropdown.Item
                  onClick={() => handleAthletesSort("silver-medals desc")}
                  active={activeStr === "silver-medals desc"}
                >
                  Descending
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Col>
          <Col>
            <Dropdown>
              <Dropdown.Toggle style={{ width: "100%" }}>
                Bronze Medals
              </Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item
                  onClick={() => handleAthletesSort("bronze-medals asc")}
                  active={activeStr === "bronze-medals asc"}
                >
                  Ascending
                </Dropdown.Item>
                <Dropdown.Item
                  onClick={() => handleAthletesSort("bronze-medals desc")}
                  active={activeStr === "bronze-medals desc"}
                >
                  Descending
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Col>
        </Row>
        <Row>
          <h5 className="header">Search</h5>
        </Row>
        <Row>
          <Col>
            <FormControl
              className="mr-sm-2"
              type="text"
              placeholder="Search"
              defaultValue={searchedStr}
              ref={textInput}
              onKeyPress={(event: any) => {
                if (event.key === "Enter") {
                  handleSearch();
                }
              }}
            />
          </Col>
          <Button
            className="searchButton"
            variant="info"
            onClick={() => handleSearch()}
          >
            <AiOutlineSearch className="searchIcon" />
          </Button>
        </Row>
        <Row>
          <h5 className="header">Compare</h5>
        </Row>
        <Row>
          <Col>
            <Multiselect
              options={choices} // Options to display in the dropdown
              onSelect={handleCompare1} // Function will trigger on select event
              displayValue="name" // Property name to display in the dropdown options
              placeholder="Name"
              singleSelect
            />
          </Col>
          <Col>
            <Multiselect
              options={choices} // Options to display in the dropdown
              onSelect={handleCompare2} // Function will trigger on select event
              displayValue="name" // Property name to display in the dropdown options
              placeholder="Name"
              singleSelect
            />
          </Col>
          <Col md={2}>
            <Button
              className="compare-btn"
              variant="outline-dark"
              href={`/athletes/compare?c1=${compare1}&c2=${compare2}`}
            >
              Compare
            </Button>
          </Col>
        </Row>
      </Container>

      <Container>
        <Row
          xs="auto"
          sm="auto"
          md="auto"
          lg="auto"
          xl="auto"
          xxl={4}
          className="athlete-row"
        >
          {/* we have status messages for the users conv */}
          {status === "error" && (
            <h3 className="waiting">Error Fetching data!</h3>
          )}
          {status === "loading" && (
            <h3 className="waiting">Fetching data...</h3>
          )}
          {status === "success" && totalPages == 0 && (
            <h3 className="waiting">No results found</h3>
          )}
          {/* these are the cards that are shown to the user and any search info is highlighted */}
          {status === "success" &&
            data?.map((athlete: any) => (
              <Col key={athlete.athlete_id}>
                <Card className="athlete-card-style">
                  <Card.Img
                    src={athlete.athlete_image}
                    className="athlete-card-image-style"
                  />
                  <Card.Body>
                    <Card.Title>
                      <h3>
                        <Button
                          variant="outline-light"
                          href={`/athletes/${athlete.athlete_id}`}
                        >
                          <Highlighter
                            highlightClassName="searchHighlight"
                            searchWords={searchedStr.split(" ")}
                            textToHighlight={athlete.athlete_name}
                          />
                        </Button>
                      </h3>
                    </Card.Title>
                    <Card.Text>
                      Sport:{" "}
                      {
                        <Highlighter
                          highlightClassName="searchHighlight"
                          searchWords={searchedStr.split(" ")}
                          textToHighlight={athlete.athlete_sport}
                        />
                      }
                    </Card.Text>
                    <Card.Text>
                      Country:{" "}
                      {
                        <Highlighter
                          highlightClassName="searchHighlight"
                          searchWords={searchedStr.split(" ")}
                          textToHighlight={athlete.country_name}
                        />
                      }
                    </Card.Text>
                    <Card.Text>
                      {
                        <Highlighter
                          highlightClassName="searchHighlight"
                          searchWords={searchedStr.split(" ")}
                          textToHighlight={athlete.num_gold_medals}
                        />
                      }{" "}
                      Gold Medal
                    </Card.Text>
                    <Card.Text>
                      {
                        <Highlighter
                          highlightClassName="searchHighlight"
                          searchWords={searchedStr.split(" ")}
                          textToHighlight={athlete.num_silver_medals}
                        />
                      }{" "}
                      Silver Medal
                    </Card.Text>
                    <Card.Text>
                      {
                        <Highlighter
                          highlightClassName="searchHighlight"
                          searchWords={searchedStr.split(" ")}
                          textToHighlight={athlete.num_bronze_medals}
                        />
                      }{" "}
                      Bronze Medal
                    </Card.Text>
                  </Card.Body>
                </Card>
              </Col>
            ))}
        </Row>
      </Container>
      {/* pagination */}
      <div className="disp-footer">
        <div className="disp-instance">
          <h6>Showing {data?.length} Instances</h6>
          <h6>
            Page {page} of {totalPages}
          </h6>
        </div>
        <div className="disp-pagination">
          <Button
            className="pagination-btn"
            onClick={handleFirst}
            disabled={disablePrev}
            variant="outline-dark"
          >
            First
          </Button>
          <Button
            className="pagination-btn"
            onClick={handlePrev}
            disabled={disablePrev}
            variant="outline-dark"
          >
            Prev
          </Button>
          <Button
            className="pagination-btn"
            onClick={handlePrev}
            variant="outline-dark"
            disabled={disablePrev}
          >
            {page - 1}
          </Button>
          <Button className="pagination-btn" variant="outline-dark" active>
            {page}
          </Button>
          <Button
            className="pagination-btn"
            onClick={handleNext}
            variant="outline-dark"
            disabled={disableNext}
          >
            {page + 1}
          </Button>
          <Button
            className="pagination-btn"
            onClick={handleNext}
            disabled={disableNext}
            variant="outline-dark"
          >
            Next
          </Button>
          <Button
            className="pagination-btn"
            onClick={handleLast}
            disabled={disableNext}
            variant="outline-dark"
          >
            Last
          </Button>
        </div>
      </div>
    </div>
  );
}
export default Athletes;
