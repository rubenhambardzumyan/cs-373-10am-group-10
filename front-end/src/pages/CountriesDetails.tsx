import React, { useState, useEffect } from "react";
import Card from "react-bootstrap/Card";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import ListGroup from "react-bootstrap/ListGroup";
import "./CountriesDetails.css";

type CountryDetailsType = {
  country_capital_city: string;
  country_gini: string;
  country_medals_total: string;
  country_name: string;
  country_population: string;
  country_region: string;
  country_flag: string;
  country_language: string;
  country_map: string;
  country_num_bronze: number;
  country_num_gold: number;
  country_num_olympics: number;
  country_num_silver: number;
};

function CountriesDetails(props: any) {
  useEffect(() => {
    fetchItem();
  }, []);

  const [country, setCountry] = useState<CountryDetailsType | undefined>(
    undefined
  );
  const [events, setEvents] = useState([]);
  const [athletes, setAthletes] = useState([]);

  // obtain the appropriate country instance specified in the URL from our API
  const fetchItem = async () => {
    const data = await fetch(
      `https://api.goingforgold.me/api/countries/${props.match.params.id}`
    );
    const country = await data.json();
    setCountry(country.country);
    setEvents(country.events);
    setAthletes(country.athletes);
  };

  return (
    <div className="country-background">
      <Container>
        <Card className="country-card" style={{ backgroundColor: "#D6AF36" }}>
          <Card.Body>
            <Container>
              <Button
                className="btn-mine"
                onClick={() => window.history.back()}
                variant="outline-light"
              >
                &lt;
              </Button>
            </Container>
          </Card.Body>
          <Card.Body>
            <h1 className="country-name">
              {country && country.country_name.toUpperCase()}
            </h1>
          </Card.Body>
          <Card.Body>
            <Card.Img
              className="flag-img"
              variant="top"
              src={country && country.country_flag}
            />
          </Card.Body>
          <ListGroup variant="flush">
            <ListGroup.Item>
              <Card.Title>Background Information</Card.Title>
              <Card.Text className="country-card-text">
                <Container>
                  <Row>
                    <Col className="country-info">
                      <div>
                        <h3>Region</h3>
                        <h5>{country && country.country_region}</h5>
                      </div>
                    </Col>
                    <Col className="country-info">
                      <div>
                        <h3>Capital City</h3>
                        <h5>{country && country.country_capital_city}</h5>
                      </div>
                    </Col>
                    <Col className="country-info">
                      <div>
                        <h3>Language</h3>
                        <h5>{country && country.country_language}</h5>
                      </div>
                    </Col>
                  </Row>
                  <Row>
                    <Col className="country-info">
                      <div>
                        <h3>Population</h3>
                        <h5>{country && country.country_population}</h5>
                      </div>
                    </Col>
                    <Col className="country-info">
                      <div>
                        <h3>GINI Coefficient</h3>
                        <h5>{country && country.country_gini}</h5>
                      </div>
                    </Col>
                  </Row>
                </Container>
              </Card.Text>
            </ListGroup.Item>
            <ListGroup.Item>
              <Card.Title>Olympic Facts</Card.Title>
              <img
                src="https://www.pngall.com/wp-content/uploads/2017/05/Olympic-Rings-Free-Download-PNG.png"
                className="olympic-logo"
                alt="olympic logo"
              />
              <Card.Text className="country-card-text">
                Total Number of Medals -{" "}
                {country && country.country_medals_total}
              </Card.Text>
              <Card.Text className="country-card-text">
                <Container>
                  <Row>
                    <Col>
                      <div className="gold-medal">
                        <div className="medal-text">G</div>
                      </div>
                      {country && country.country_num_gold}
                    </Col>
                    <Col>
                      <div className="silver-medal">
                        <div className="medal-text">S</div>
                      </div>
                      {country && country.country_num_silver}
                    </Col>
                    <Col>
                      <div className="bronze-medal">
                        <div className="medal-text">B</div>
                      </div>
                      {country && country.country_num_bronze}
                    </Col>
                  </Row>
                </Container>
              </Card.Text>
              <Card.Text className="country-card-text">
                <h3>Related Athletes</h3>
                <Container>
                  {/* limit to 5 athletes that are linked to this Country */}
                  <Row>
                    {athletes.slice(0, 5).map((athlete: any) => (
                      <Col key={athlete && athlete.athlete_id}>
                        <Button
                          className="athlete-name"
                          href={`/athletes/${athlete && athlete.athlete_id}`}
                          variant="outline-dark"
                        >
                          {athlete && athlete.athlete_name}
                        </Button>
                      </Col>
                    ))}
                  </Row>
                </Container>
              </Card.Text>
              <Card.Text className="country-card-text">
                <h3>Related Events</h3>
                <Container>
                  <Row>
                    {events.slice(0, 5).map((event: any) => (
                      <Col key={event && event.event_id}>
                        <Button
                          className="athlete-name"
                          href={`/events/${event && event.event_id}`}
                          variant="outline-dark"
                        >
                          {event && event.event_name}
                        </Button>
                      </Col>
                    ))}
                  </Row>
                </Container>
              </Card.Text>
              <Card.Text className="country-card-text">
                <Container fluid>
                  <Row className="justify-content-center" xs="auto">
                    <Col>
                      <div className="num-games-1">
                        <h1 className="num-games-text">
                          {" "}
                          {country && country.country_num_olympics}
                        </h1>
                      </div>
                    </Col>
                    <Col>
                      <div className="num-games-2">
                        <img
                          src="https://www.pngall.com/wp-content/uploads/2017/05/Olympic-Rings-PNG.png"
                          className="olympic-logo-2"
                          alt="olympic logo"
                        />
                      </div>
                    </Col>
                    <Col>
                      <div className="num-games-3">
                        <h1 className="num-games-text">Games</h1>
                      </div>
                    </Col>
                  </Row>
                </Container>
              </Card.Text>
            </ListGroup.Item>
            {/* G ogle Map media*/}
            {/* Go */}
            <ListGroup.Item>
              <div className="iframe-container">
                <iframe
                  className="responsive-iframe"
                  frameBorder="0"
                  src={`https://www.google.com/maps/embed/v1/place?key=AIzaSyAfWv5xRzILr3h4cA_DS8PRfjs0lua-HBc&q=${
                    country && country.country_name
                  }`}
                ></iframe>
              </div>
            </ListGroup.Item>
          </ListGroup>
        </Card>
      </Container>
    </div>
  );
}
export default CountriesDetails;
