import React, { useEffect, useState } from "react";
import {
  Treemap,
  Tooltip,
  BarChart,
  Bar,
  CartesianGrid,
  XAxis,
  YAxis,
  RadarChart,
  PolarAngleAxis,
  PolarRadiusAxis,
  Radar,
  PolarGrid,
  Legend,
  ResponsiveContainer,
  PieChart,
  Pie,
} from "recharts";
import Container from "react-bootstrap/Container";
import "./Visualizations.css";

function Visualizations() {
  useEffect(() => {
    fetchCountries();
    fetchAthletes();
    fetchEvents();
  }, []);

  const [countries, setCountries] = useState([]);
  const [athletes, setAthletes] = useState([]);
  const [events, setEvents] = useState([]);

  // fetch countries from Going for Gold API
  const fetchCountries = async () => {
    try {
      const res = await fetch(`https://api.goingforgold.me/api/countries?p=-1`);
      const countries = await res.json();
      setCountries(countries.body);
    } catch (e) {
      console.log("error");
      setCountries([]);
    }
  };
  // fetch athletes from Going for Gold API
  const fetchAthletes = async () => {
    try {
      const res = await fetch(`https://api.goingforgold.me/api/athletes?p=-1`);
      const athletes = await res.json();
      setAthletes(athletes.body);
    } catch (e) {
      console.log("error");
      setAthletes([]);
    }
  };
  // fetch events from Going for Gold API
  const fetchEvents = async () => {
    try {
      const res = await fetch(`https://api.goingforgold.me/api/events?p=-1`);
      const events = await res.json();
      setEvents(events.body);
    } catch (e) {
      console.log("error");
      setEvents([]);
    }
  };

  // project countries into an array with necessary columns
  let countryData = countries.map((country: any) => ({
    name: country.country_name,
    medals: country.country_medals_total,
  }));
  countryData = countryData.splice(0, 156);

  // project athletes into an array with necessary columns
  const athleteData = athletes.map((athlete: any) => ({
    name: athlete.athlete_name,
    sport: athlete.athlete_sport,
  }));
  // project events into an array with necessary columns
  const eventData = events.map((event: any) => ({
    name: event.event_name,
    sport: event.sport,
  }));
  function tallyKeys(arr: any, key: any) {
    // Resources used:
    // https://stackoverflow.com/questions/44829459/javascript-count-number-values-repeated-in-a-jsonarray-items
    // https://www.geeksforgeeks.org/how-to-count-number-of-occurrences-of-repeated-names-in-an-array-of-objects-in-javascript/
    const arr2: any[] = [];
    arr.forEach((x: any) => {
      if (
        arr2.some((val: any) => {
          return val[key] == x[key];
        })
      ) {
        arr2.forEach((k: any) => {
          if (k[key] === x[key]) {
            k["num_instances"]++;
          }
        });
      } else {
        const a: any = {};
        a[key] = x[key];
        a["num_instances"] = 1;
        arr2.push(a);
      }
    });
    return arr2;
  }
  const num_athlete_per_sport = tallyKeys(athleteData, "sport");
  const num_events_per_sport = tallyKeys(eventData, "sport");

  // Customize Visualizatin Tooltip
  const CustomTooltip = ({ active, payload }: any) => {
    if (active && payload && payload.length) {
      return (
        <div className="custom-tooltip">
          <p>{`Country: ${payload[0].payload.name}`}</p>
          <p>{`Medals: ${payload[0].payload.medals}`}</p>
        </div>
      );
    }
    return null;
  };
  const CustomTooltip2 = ({ active, payload, label }: any) => {
    if (active && payload && payload.length) {
      return (
        <div className="custom-tooltip">
          <p>{`Sport: ${payload[0].payload.sport}`}</p>
          <p>{`Number of Events ${payload[0].payload.num_instances}`}</p>
        </div>
      );
    }
    return null;
  };

  return (
    <Container className="viz-div">
      <h1 className="title">Visualizations</h1>

      {/* First Visualization */}
      <h4 className="sub-title">Total Number of Medals for each Country</h4>
      <ResponsiveContainer width="100%" height={600}>
        <Treemap data={countryData} dataKey="medals">
          <Tooltip content={<CustomTooltip />} />
        </Treemap>
      </ResponsiveContainer>

      {/* Second Visualization */}
      <h4 className="sub-title">
        Total Number of Athletes for each type of Sport
      </h4>
      <ResponsiveContainer width="100%" height={600}>
        <BarChart width={1300} height={600} data={num_athlete_per_sport}>
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="sport" />
          <YAxis />
          <Tooltip />
          <Bar
            dataKey="num_instances"
            fill="#8884d8"
            name="Number of Athletes"
          />
        </BarChart>
      </ResponsiveContainer>

      {/* Third Visualization */}
      <h4 className="sub-title">Total Number of Events Per Sport</h4>
      <ResponsiveContainer width="100%" height={600}>
        <PieChart width={600} height={600}>
          <Tooltip content={<CustomTooltip2 />} />
          <Pie
            data={num_events_per_sport}
            dataKey="num_instances"
            nameKey="sport"
            cx="50%"
            cy="50%"
            outerRadius={250}
            fill="#8884d8"
            label
          />
        </PieChart>
      </ResponsiveContainer>
    </Container>
  );
}

export default Visualizations;
