import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Card from "react-bootstrap/Card";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import ListGroup from "react-bootstrap/ListGroup";
import Table from "react-bootstrap/Table";
import "./AthletesDetails.css";
import Ratio from "react-bootstrap/Ratio";

type AthleteDetailsType = {
  athlete_id: number;
  athlete_DOB: string;
  athlete_birthplace: string;
  athlete_games: string;
  athlete_height: string;
  athlete_image: string;
  athlete_name: string;
  athlete_sport: string;
  country_id: number;
  country_name: string;
  event_results: string;
  num_bronze_medals: string;
  num_gold_medals: string;
  num_silver_medals: string;
};

type EventResultType = {
  Year: [];
  Event: [];
  Result: [];
  event_id: [];
};

function AthletesDetails(props: any) {
  const [athlete, setAthlete] = useState<AthleteDetailsType>();
  const [myObj, setEventsResult] = useState<EventResultType>();
  const [flag, setFlag] = useState<boolean>();
  const [athVideo, setVideo] = useState([]);

  useEffect(() => {
    fetchItem();
  }, []);

  useEffect(() => {
    if (flag) getEventResult();
  }, [flag]);

  // obtain the appropriate athlete instance specified in the URL from our API
  const fetchItem = async () => {
    const data = await fetch(
      `https://api.goingforgold.me/api/athletes/${props.match.params.id}`
    );
    const athlete = await data.json();
    setAthlete(athlete.athletes);

    // Perform a youtube video search of the Athlete and Sport using Youtube Search API
    // key will be disabled at the end of Fall 2021 Semester
    const myKey = "AIzaSyB_nupj3lsAnHvAtZansZM6JcRXdKlWlt0";
    if (athlete && athlete.athletes) {
      const searchStr = athlete.athletes.athlete_name.replace(/\s/g, "-");
      const sportStr = athlete.athletes.athlete_sport.replace(/\s/g, "-");
      const data = await fetch(
        `https://www.googleapis.com/youtube/v3/search?part=snippet&q=olympics-${searchStr}-${sportStr}&key=${myKey}&maxResults=1`
      );
      const athVideo = await data.json();
      setVideo(athVideo.items);
    }
    setFlag(true);
  };

  // helper function to handle the event result we have from out JSON. Mainly fixes the formatting and allow us to render the result as JSON instead of a string
  const getEventResult = () => {
    const one_str = athlete?.event_results;
    const two_str = one_str?.replace(/""/g, '"');
    const three_str = two_str?.replace(/'s/g, "^s");
    const str4 = three_str
      ?.replace(/'/g, '"')
      .replace(/\\"{/g, '"{')
      .replace(/}\\"/g, '}"');
    const str5 = str4
      ?.replace(/\^s/g, "'s")
      .replace(/nan/g, '"nan"')
      .replace(/"{/g, "{")
      .replace(/}"/g, "}");
    console.log(str5);
    if (str5) {
      const myObj = JSON.parse(str5);
      setEventsResult(myObj);
    }
  };

  return (
    <div className="athlete-background">
      <Container>
        <Card className="athlete-card">
          <Container>
            <Button onClick={() => window.history.back()} className="back-btn">
              &lt; Athletes
            </Button>
          </Container>
          <h1>{athlete && athlete.athlete_name}</h1>
          <Card.Body>
            <Card.Img
              className="athlete-pic"
              variant="top"
              src={athlete && athlete.athlete_image}
            />
          </Card.Body>
          <ListGroup>
            <ListGroup.Item>
              <Card.Title>
                <h3>Background Information</h3>
              </Card.Title>
              <Card.Text>
                <Container>
                  <Row>
                    <Col className="athlete-info">
                      <div>
                        <h3>Data of Birth</h3>
                        <h5>{athlete && athlete.athlete_DOB}</h5>
                      </div>
                    </Col>
                    <Col className="athlete-info">
                      <div>
                        <h3>Birth Place</h3>
                        <h5>{athlete && athlete.athlete_birthplace}</h5>
                      </div>
                    </Col>
                    <Col className="athlete-info">
                      <div>
                        <h3>Games</h3>
                        <h5>{athlete && athlete.athlete_games}</h5>
                      </div>
                    </Col>
                  </Row>
                  <Row>
                    <Col className="athlete-info">
                      <div>
                        <h3>Height</h3>
                        <h5>{athlete && athlete.athlete_height}</h5>
                      </div>
                    </Col>
                    <Col className="athlete-info">
                      <div>
                        <h3>Sport</h3>
                        <h5>{athlete && athlete.athlete_sport}</h5>
                      </div>
                    </Col>
                    <Col className="athlete-info">
                      <div>
                        <h3>Country</h3>
                        <Button
                          variant="outline-dark"
                          href={`/countries/${athlete && athlete.country_id}`}
                        >
                          {athlete && athlete.country_name}
                        </Button>
                      </div>
                    </Col>
                  </Row>
                </Container>
              </Card.Text>
            </ListGroup.Item>
            <ListGroup.Item>
              <Card.Title>
                <h3>Olympic Achivements</h3>
              </Card.Title>
              <Card.Text>
                <Container>
                  <Row>
                    <Col>
                      <div className="gold-medal">
                        <div className="medal-text">G</div>
                      </div>
                      {athlete && athlete.num_gold_medals}
                    </Col>
                    <Col>
                      <div className="silver-medal">
                        <div className="medal-text">S</div>
                      </div>
                      {athlete && athlete.num_silver_medals}
                    </Col>
                    <Col>
                      <div className="bronze-medal">
                        <div className="medal-text">B</div>
                      </div>
                      {athlete && athlete.num_bronze_medals}
                    </Col>
                  </Row>
                </Container>
              </Card.Text>
            </ListGroup.Item>
            <ListGroup.Item>
              <Card.Title>
                <h1>Event Results</h1>
              </Card.Title>
              {/* we display any previous results of this athlete in Olympics */}
              <Table responsive>
                <tbody>
                  <tr>
                    <td>Year</td>
                    {myObj?.Year.map((yr: number) => (
                      <td key={athlete?.athlete_id}>{yr}</td>
                    ))}
                  </tr>
                  <tr>
                    <td>Event</td>
                    {myObj?.Event.map((yr: number, index) => (
                      <td key={athlete?.athlete_id}>
                        <Link to={`/events/${myObj?.event_id[index]}`}>
                          {yr}
                        </Link>
                      </td>
                    ))}
                  </tr>
                  <tr>
                    <td>Result</td>
                    {myObj?.Result.map((yr: number) => (
                      <td key={athlete?.athlete_id}>{yr}</td>
                    ))}
                  </tr>
                </tbody>
              </Table>
            </ListGroup.Item>
            {/* Youtube Video media */}
            <ListGroup.Item>
              {athVideo?.map((vid: any) => (
                <div key={vid.id.videoId}>
                  <Ratio aspectRatio="16x9">
                    <iframe
                      src={`https://www.youtube.com/embed/${vid.id.videoId}`}
                      title={vid.snippet.title}
                      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                      allowFullScreen
                    />
                  </Ratio>
                </div>
              ))}
            </ListGroup.Item>
          </ListGroup>
        </Card>
      </Container>
    </div>
  );
}

export default AthletesDetails;
