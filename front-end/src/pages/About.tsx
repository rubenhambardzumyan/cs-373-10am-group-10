import React from "react";
import Card from "react-bootstrap/Card";
import CardGroup from "react-bootstrap/CardGroup";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { useState, useEffect } from "react";
import axios from "axios";
import "./About.css";
import ruben from "../images/team-members/ruben.jpg";
import sindhu from "../images/team-members/sindhu.jpg";
import varad from "../images/team-members/varad.png";
import victoria from "../images/team-members/victoria.png";
import thomas from "../images/team-members/thomas.jpg";
// this interface stores information about each Group member in our team
interface GroupMember {
  name: string;
  bio: string;
  numIssuesOpened: number;
  numIssuesClosed: number;
  numUnitTests: number;
  numCommits: number;
  email: string;
  gitlabId: string;
  color: string;
  picture: string;
  devRole: string;
}

interface ContributorData {
  email: string;
  commits: number;
}

interface IssueStats {
  statistics: {
    counts: {
      closed: number;
      opened: number;
    };
  };
}

function About() {
  const [totalNumCommits, setTotalNumCommits] = useState<number>(0);
  const [totalNumIssuesOpened, setTotalNumIssuesOpened] = useState<number>(0);
  const [totalNumIssuesClosed, setTotalNumIssuesClosed] = useState<number>(0);

  // Stores information about each group member
  const [groupMembers, setGroupMembers] = useState<GroupMember[]>([
    {
      name: "Ruben Hambardzumyan",
      bio: "I'm a senior CS major at UT from Plano, TX! A few hobbies I enjoy are bouldering, hiking, and exploring new coffee shops!",
      numIssuesOpened: 0,
      numIssuesClosed: 0,
      numUnitTests: 13,
      numCommits: 0,
      email: "rubenhambardzumyan@gmail.com",
      gitlabId: "rubenhambardzumyan",
      color: "rgba(83, 113, 255, 1)",
      picture: ruben,
      devRole: "Backend",
    },
    {
      name: "Sindhu Nemana",
      bio: "I'm a junior CS and Finance major at UT from Plano, TX! I like to swim in my free time as a part of the Longhorn Swim Club!",
      numIssuesOpened: 0,
      numIssuesClosed: 0,
      numUnitTests: 9,
      numCommits: 0,
      email: "thingimajig13@gmail.com",
      gitlabId: "sinemana",
      color: "rgba(237, 28, 36, 1)",
      picture: sindhu,
      devRole: "Backend",
    },
    {
      name: "Varad Thorat",
      bio: "I am a junior Computer Science major with a Business minor at UT Austin from Plano, TX. My hobbies include playing tennis, drawing, and playing the piano!",
      numIssuesOpened: 0,
      numIssuesClosed: 0,
      numUnitTests: 10,
      numCommits: 0,
      email: "varadthorat2@gmail.com",
      gitlabId: "varadthorat",
      color: "rgba(4, 52, 84, 1)",
      picture: varad,
      devRole: "Frontend",
    },
    {
      name: "Victoria Hall",
      bio: "I'm a junior Computer Science major from Plano, Texas! In my free time, I enjoy running, reading, and binge watching new TV shows.",
      numIssuesOpened: 0,
      numIssuesClosed: 0,
      numUnitTests: 36,
      numCommits: 0,
      email: "victoria.hall@utexas.edu",
      gitlabId: "victoriahall",
      color: "rgba(247, 147, 29, 1)",
      picture: victoria,
      devRole: "Backend",
    },
    {
      name: "Thomas Budiman",
      bio: "I'm a Computer Science Student at UT Austin from Indonesia. Some hobbies I enjoy include table tennis, traveling, and exploring new food.",
      numIssuesOpened: 0,
      numIssuesClosed: 0,
      numUnitTests: 20,
      numCommits: 0,
      email: "thomas.ibudiman@gmail.com",
      gitlabId: "thomas1b",
      color: "rgba(140, 198, 63, 1)",
      picture: thomas,
      devRole: "Frontend",
    },
  ]);

  useEffect(() => {
    const groupMembersCopy: GroupMember[] = [...groupMembers];
    axios
      .get(
        "https://gitlab.com/api/v4/projects/29854456/repository/contributors"
      )
      .then((response) => {
        const groupMembersCopy: GroupMember[] = [...groupMembers];
        const contributorsList: ContributorData[] = response.data;
        const emailCommits: Map<string, number> = new Map<string, number>();
        let numCommitsCount = 0;
        contributorsList.forEach((contributor: ContributorData) => {
          emailCommits.set(contributor.email, contributor.commits);
          numCommitsCount += contributor.commits;
        });
        setTotalNumCommits(numCommitsCount);

        groupMembersCopy.forEach((groupMember: GroupMember) => {
          const memberNumCommits: number | undefined = emailCommits.get(
            groupMember.email
          );
          if (memberNumCommits !== undefined) {
            groupMember.numCommits = memberNumCommits;
          }
        });
        setGroupMembers(groupMembersCopy);
      });

    groupMembersCopy.forEach((groupMember: GroupMember) => {
      axios
        .get(
          "https://gitlab.com/api/v4/projects/29854456/issues_statistics?assignee_username=" +
            groupMember.gitlabId
        )
        .then((response) => {
          const groupMembersCopy: GroupMember[] = [...groupMembers];
          const issueStatsList: IssueStats = response.data;
          groupMember.numIssuesOpened = issueStatsList.statistics.counts.opened;
          groupMember.numIssuesClosed = issueStatsList.statistics.counts.closed;
          setGroupMembers(groupMembersCopy);
        });
    });

    axios
      .get("https://gitlab.com/api/v4/projects/29854456/issues_statistics")
      .then((response) => {
        const overallIssueStats: IssueStats = response.data;
        setTotalNumIssuesOpened(overallIssueStats.statistics.counts.opened);
        setTotalNumIssuesClosed(overallIssueStats.statistics.counts.closed);
      });
  }, []);

  return (
    <div>
      <link
        href="https://fonts.googleapis.com/css?family=Lobster"
        rel="stylesheet"
        type="text/css"
      />
      <h1 className="logo_font"> Going For Gold</h1>

      <h3 style={{ paddingBottom: "40px" }}>About our Website</h3>
      <CardGroup className="container">
        <Card
          className="about-card-style"
          style={{ backgroundColor: "rgba(255, 235, 144, 1)" }}
        >
          <Card.Body>
            <Card.Title>
              <h3>Purpose</h3>
            </Card.Title>
            <Card.Text>
              The goal of Going for Gold is to help the reader learn about
              different Olympic events that their country partakes in. We can
              inspire them to pursue different sports by giving them information
              about Olympic Athletes. It can also educate the reader on the
              inequalities of wealth between countries and how that affects
              their performance in the Olympic Games. Hopefully it will inspire
              audiences living in more wealthy countries to help address this
              issue of a disparity of wealth.
            </Card.Text>
            <Card.Text>
              Combining the data from our four disparate data sources will allow
              us to answer the following interesting questions:
              <ul>
                <li>
                  What trends can be seen for countries winning at a certain
                  event over the course of the years?
                </li>
                <li>
                  Is there a correlation between GDP or economic prosperity and
                  medals won at the Olympics?
                </li>
                <li>
                  How many Olympics do most athletes participate in during their
                  career as a professional?
                </li>
              </ul>
            </Card.Text>
          </Card.Body>
        </Card>
        <Card
          className="about-card-style"
          style={{ backgroundColor: "rgba(255, 235, 144, 1)" }}
        >
          <Card.Body>
            <Card.Title>
              <h3>Links</h3>
            </Card.Title>
            <Card.Text>
              GitLab Repository{" "}
              <a href="https://gitlab.com/rubenhambardzumyan/cs-373-10am-group-10">
                Link
              </a>
            </Card.Text>
            <Card.Text>
              API Documentation{" "}
              <a href="https://documenter.getpostman.com/view/17588807/UUy37RGZ#intro">
                Link
              </a>
            </Card.Text>
            <Card.Title>
              <h3>Statistics</h3>
            </Card.Title>
            <Card.Text>Total # commits: {totalNumCommits}</Card.Text>
            <Card.Text>Total # issues open: {totalNumIssuesOpened}</Card.Text>
            <Card.Text>Total # issues closed: {totalNumIssuesClosed}</Card.Text>
            <Card.Text>Total # unit tests: {88}</Card.Text>
          </Card.Body>
        </Card>
      </CardGroup>

      <br />

      <h3 style={{ paddingBottom: "40px" }}>About our Team Members</h3>

      <CardGroup className="container">
        <Row className="container">
          {groupMembers.map((groupMember) => (
            <Col key={groupMember.gitlabId}>
              <Card
                className="about-card-style"
                style={{ backgroundColor: groupMember.color }}
              >
                <Card.Img
                  style={{ width: "100%", height: "15vw", objectFit: "cover" }}
                  src={groupMember.picture}
                />
                <Card.Body>
                  <Card.Title style={{ color: "white" }}>
                    <h3>{groupMember.name}</h3>
                  </Card.Title>
                  <Card.Text style={{ color: "white" }}>
                    {groupMember.bio}
                  </Card.Text>
                  <Card.Text style={{ color: "white" }}>
                    Development role: {groupMember.devRole}
                  </Card.Text>
                  <Card.Text style={{ color: "white" }}>
                    # commits: {groupMember.numCommits}
                  </Card.Text>
                  <Card.Text style={{ color: "white" }}>
                    # issues open: {groupMember.numIssuesOpened}
                  </Card.Text>
                  <Card.Text style={{ color: "white" }}>
                    # issues closed: {groupMember.numIssuesClosed}
                  </Card.Text>
                  <Card.Text style={{ color: "white" }}>
                    # unit tests: {groupMember.numUnitTests}
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
          ))}
        </Row>
      </CardGroup>

      <br />

      <h3 style={{ paddingBottom: "40px" }}>Datasources Used</h3>

      <CardGroup className="container">
        <Row className="container" style={{ marginBottom: "15px" }}>
          <Col>
            <Card
              className="about-card-style"
              style={{ backgroundColor: "rgba(255, 235, 144, 1)" }}
            >
              <Card.Body>
                <Card.Title>
                  <h3>
                    <a href="https://olympics.fandom.com">
                      Olympics Fandom Wiki page
                    </a>
                  </h3>
                </Card.Title>
                <Card.Text>
                  Used for data regarding Olympics athletes and their date of
                  birth, events, country, birthplace, and number of medals.
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card
              className="about-card-style"
              style={{ backgroundColor: "rgba(255, 235, 144, 1)" }}
            >
              <Card.Body>
                <Card.Title>
                  <h3>
                    <a href="https://restfulcountries.com/api-documentation/version/1">
                      Restful Countries API
                    </a>
                  </h3>
                </Card.Title>
                <Card.Text>
                  Used for non-olympic related data regarding countries such as
                  gini coefficient, language, and population.
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card
              className="about-card-style"
              style={{ backgroundColor: "rgba(255, 235, 144, 1)" }}
            >
              <Card.Body>
                <Card.Title>
                  <h3>
                    <a href="https://www.mediawiki.org/wiki/API:Main_page">
                      MediaWiki API
                    </a>
                  </h3>
                </Card.Title>
                <Card.Text>
                  Used to scrape data from Wikipedia such as{" "}
                  <a href="https://en.wikipedia.org/wiki/Summer_Olympic_Games#List_of_Summer_Olympic_Games">
                    summer olympic games data
                  </a>
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card
              className="about-card-style"
              style={{ backgroundColor: "rgba(255, 235, 144, 1)" }}
            >
              <Card.Body>
                <Card.Title>
                  <h3>
                    <a href="https://en.wikipedia.org/wiki/All-time_Olympic_Games_medal_table">
                      Wikipedia pages
                    </a>
                  </h3>
                </Card.Title>
                <Card.Text>
                  Used for data on Olympic country medal data and event medal
                  data.
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </CardGroup>

      <br />

      <h3 style={{ paddingBottom: "40px" }}>Tools used</h3>

      <CardGroup className="container">
        <Row className="container" style={{ marginBottom: "15px" }}>
          <Col>
            <Card
              className="about-card-style"
              style={{
                backgroundColor: "rgba(255, 235, 144, 1)",
                height: "400px",
              }}
            >
              <Card.Body>
                <Card.Img
                  src={
                    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQsAAAC9CAMAAACTb6i8AAAAYFBMVEX///9h2vtT2Pta2ftQ1/v6/v9k2/u+7v3o+f77/v/x+//W9P6J4vyt6v30/P/d9v7J8f2a5vx/4Pxw3fvR8/6i6PzG8P3r+v7h9/6R5Px53/yq6f2E4fyg5/y27P2/7/0bvz2NAAAN0ElEQVR4nO1dibaiuhKVJAwKiAgqoAf//y+fQEbI5G1Rsl72Wrdvt4d4kkpVpcaw23l4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh8HvvwX0bfkuTwqZn8FLc+iwBCIMrydP/26CRuIwQAQKC7JyvM7ptIMgBhMAJCALLTO4MPeUVHv8ajs9PUyBFdyrQeAP9ulmPTx2zwixr5qrNdFRkIFoAgs9ne+sUSy8GgXX3OK6GVkGLc3sykCotKPjQA5Vdm/nH0ivUMvHHRDTxmSDUyQG9pnK3gBujKARz/46kR1MqBvSAdr5FRVQX0M3D84ho+hQeePSqLl3URNs8W8IsEmdzmuHUcO0FU5cX4XHLBH0MHpaTBfA4K9ln94KgBoYw1YsQ/ceHUbIGJgdyzu9ppTSAVPr2V3FrRYov3D8BRohd/WE8/g/dV570CQqDg6EPLqAErcY+bACopsaPkDdab9Tq4TrRAEk3XMI0AAS8nT3Z8oFYy8IYWUucEpj2Ectvoyc4UzpQsKSlgJF/uGTopJFhbKE7OI9ML1JQ804/Qn+JLMbNVa0x4PWDjAih99ZjJSTe4r8eIsAoMUtWgAxa8Vaa8Gk7TrDv1Eze2+Oi4u1GtCc6aWEc0PdJ8fsIrIp8kW2tqU3cFBieZ/pANGZ8D189OdmWUEy2e2od6piyp9tC7G5jEbrnu3bSBSsmfkM68NxgY3PlJ9Bwzw6tpl02RiiQQ3LDK5HdNdjh8fGqaXwEWfqNPGVac+3E2fm1iVMkbBLQ+/DpKisz88MFBA2NvTYuC4wtz7O84advoA1P8GkJboyjhtSc0hoVDB2mxt6TFYRbfNekXF/nCUkbCSKQFNCkCF2lhqTvJIQIJTaDhhHBRd9qdqRmhQIttM6WTT3Bz8UzFTpRWGdJo7mA6ERYBWvvaSVtrshq0DuUV8XJBVYc2ATKFPA3MszVM7K8K5QwgcXIYTdl3eqToDPenhfu7OfxNk46VDxzp2UHCvw01NdSlCdhPXYaFt4zeFJkkyhKxyOaJcIraL5lCAQbHfmvAzrXSxfgjiR8+LJPjD4GShDgU4FYgvNEbAjVJqomSTxIgSKVn8I/dypxhAxHIRf9ATtO5NNCTVW6YHHFE+cOTXRtQdyYQZRHdbknTFEWRFkXTJLdDQhSq3JoqXDS1SFpHEqUND0XJwptz0NOlLG7LeHg8aWS3Qnwvo3JpCSR1X56H5UuKjxYYCjaCc9nXPGeVLh6pJMWFef1Y9G2FhnIUGzJwBBlqWFDV9umkP3AU1RBR3hwSouaSuIxmRTnvYqBI1MbNDitk5ypziOP5b2QQCTL9zS2PfcDDngQQviM8rqnO8NSaVjQWNsMoqqquO5/PXVdVUQQB4k8TBbLTP5WXfxWnTHFWcAXN17S5HZeW2D68NcUJLAfM6PheMfWv0PxBCSHGosauzIldqXZgB5CCBJiXHUQyjfP6unLr2fZTtyTEQIYsr4cYF7ayjBmyM35uKEq51XkGJYwGQbXhfHvYy1iiu9TEl0qJS2aM/RM3lnikh/rSLb45ADDf5vka3qVKgnekIisJGUCkhIv+H2UqFYLLBqmRi5SgHMIFXnB8whT4H4HdNy4UHMPZN+N/ovv7LTqr4ioWDwDU1jjmycwB4qkjmw6SGxEnGquYDJaXCqlbJFaMG0pevotEaHGA4DzywhMrQPIUTodYliQSJiLxf5yincLJp7NADVBt5kwRuocAvOOtJPEc7EoRxWmbAMTKBeHhJzGOc8ghT3+0jdD4jWMK8ZzrBCGp8Kw1eQIBhUi7bFE8K5zfsLLt3VoRNZsQRJ3gTve8kFxnPM+hqetaEgDDMgXGcAXO3Ish8LTjast/Hx3PWQ036GaBhQMv4uSh2faFfTe6IAB18czLIIcoHD6/yotnU67bBP24NrpkrkOw3JeO8XUuV5x3ZmC/bMtZLpWMGXQBLgVfJhlOXEHkT3OLrLtO2kGGTYLXZoZkj4WfN4FoncFIFBWy4UfKYjI5uLBJ/JAYdBYwkh5q+CSBMQ5/zizOetliJ2ZgT7QJBaseeTagoSUtv2tTpP0eyilg9VeFspBUKus2RILOIdbnoSJEkYOKKvhRXDgha0FK/4K0RZ0lLRQHecgG8Ekxcq7iHJI6eUi3Bf2mz5smuTRxadF1EByRTtQV9CHBnxcjhRozjZZT/yQaeiWZYF2m9yKYyvyTJzlbzJgn4eVImxghbYoWTvDnQZhSa0fyaxF3vFKQYsY9GUdMffkX1cT/fUn/FUQVGBwBThIQzxaNpk2bF3mOmKYi6TtWKt+3P3F9gMnT4uK4Alvkcm2xFIWWmVKmfBk+bL5vZOD5GUOOdImiXsk0tBAWw6qkjQ4uofv7i/k3WJdCEAaYRbMiJSnmRwGtAjUrRTynb5eqJLYVuMTMmnG4Lk8mGuqJ1HyXwqKScg3Yd7WQgK94CNjTgsZ8zb8KF318mxbYajRvVk22VTxwNKSYKYY/YtKZsyH4IPl6VMdkFROcqd8USj+WsIUQ7mGpAKM8Frb782ngsLQpA8bsA7GT8qKhheJBZOJ9yyl9Hk9NRIHDH1uzwBjzdkwOAq9xGSKT4UA8/O8b4XtyPuilk7/wRYhpKUkhqguef5C21ID4veoe+vVwJ4WJOi/hym+/cJT0KiERQhBC4lDrmx3xeQVVlySsiT1tCtIQQ3DMxWlasUUpUEyjPek9CfAndSq0ll3deX2jj0xMzluE0rDWLLB1IDkSzDJK7UkboNGP6hDopiknQOQIa0pR++Uy9QlkjhlIJmIoY3zUA/xdmw2VAFUAmsoGflI8Fv+WxBBzgdixfznruS72y8KdxkbGFUG70eX3qGHj58XaZFWizd7Pr3GcRU4JBRN6z4rst6Q0s0B6lH4CrjUfSC4cxEI0bNZDEv0dE/RcShR0ouKpOcnqVLmiA7u8D0a/re9jN/5AdJlPBf9gUAHkdr55ECLNxtLo8aLTeayGaMyByMS0m/2K/Z3LqP68f6DkgnAwF3gUa8zp9CBla4ueyzDtL3+XPl3sac9HEXEnu2jm7nOuxG8Ld3teBTa/c5KCG/Cm7aJxDNtIy1GMW2QLITnchd+8ieIc8WZF0FJWh0xEdmybLa65GEFKDrA6ndWi7IpWuPVxC+UXI4QbNyGI+nHvcc8ZdVhwFkDXtMqBmGLklBRqlA59JP7KDbWUHFvhdITgfA2JTUCP/OKtBMasRIl65OUuvIrVWoH5ztzvoukEwwkClOEQPVOW2Iq0KvTHFitnR+JSuCgTq/he4rG93pr5zc0kNMdshiPxGcz9pSTgy7m2oejbUIH8eWmSFOl5fqP3yBc1Xc9TYWQsUYmK80XHOl989WCd2VbCfR9NuaTGy4qq2rgYCdJxVUc6kCukpysQirisZMXxKNt2A3PYyy45f5mVKMjy68lOSkiQFF5PeRYomiaijdbFC5B3kIwEoR/rnSjm8sm71V6s1m5PYSqQloFsDRwAiLpHW17ued/3cRy//szvl7J9VIGxxwq229USUpQGYoyMIoFxlIN9iKRE60MtmaOI4b/8emlvg8QrYoXye5MMKGrjhIaG3ELC5fMOdf4I3u/gnqgw9Cxn+dSmNeXRN9UuYoPr/GKUYxr/PSL7pvaxgTXKLs+CHZ2XWXeOI5j8iWXk5pY+c5Y0gUBUnNzl0P01XbjhwoUB7mDyKuX++Z5ufn19Dodpfs/z19kaX2tSrgOldlQzi2E4gmlJimQr8d4X3E7yKUhuSe2B7lu3ij3OASh+TPyNWQyGZOKUr61x8gIMUzlXJmMAWpmlVAg46OnWQVIb7teidXwcv4dUcSrTHFgju/VyBePFebRsk3lpxCPT1N/EiwY8B2C+OI9m6YlAELHRFbfXLh6qU2pEW8B056/vZIlmbWuQ4Tq3bQLfWalV+JQRhtWTAgR95RnJRH50rmvDquqUNuNcdvHsNk8FSAPbxrr59ahsjCJ6hydgr98wZDpwVtWd+3J2lBaGlbFX4wXGI2TCRAvzewq2hMhuzoUYzjP3uv+oE+CfYEkLsUUVme1JF2lhJyM7oQDUJkGM3KWFhUPJYsQ2FpSL+gJfzGtunOVesmpRmRG6eI7Y2FoDLrzyNFf4W3etbAmWl1fPXsgMI4MiwDa4W++ZsLrU/MiVh+L/Q32i9eSib3a1uPi/oAFxwBQo0pIvdzEQXpgdSvamt9f6G65mVjMms2G3rYEofOXhFzJVMb5Zgn89otoQx+RyLLWMX0CiUp5cDTduqQpZ1kT5DhKsOt0yL2idq0Jh8K+NpUcHd/mO4h61u10P/daA69qlLWL81Ud8/yXTIIrXyVpWOG0Oynnz1Y+zN4QW3CW4knbC3tE8O/Ez5mZ4wr+GG87t0iNHJxDNupVISaRrIsLqz4SejvTB1fmBh0SAODkJQJDzduiiztEd4O4Z1nFU3COhsF++JuH+SwjOMWasmjYafmf6HwXt3gZBe7+XnVihCc5K10O4IXaoRamyrKPlCI69YBjjDtiCZgUoEOpWdDvP2jW50T/oVv8IFDdoDR03hpFpBRRDA6dCFwz7SkYMiEqLEN0pktXLQvNrRTeLx3x7X9J/sQxW1str14HxhcxbRswX8UKIuucbKa/kwpeHvsjoln+6wL6vhrfyDFV5QRa/Hb9u+gd+qRE8x05lDuU4Fqc4PqX/OY5/bNI0/c31ix4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh7/L/gfRoKAr7PoaxEAAAAASUVORK5CYII="
                  }
                />
                <Card.Title>
                  <h3>
                    <a href="https://reactjs.org/">React</a>
                  </h3>
                </Card.Title>
                <Card.Text>
                  A JavaScript library used to build our website&apos;s front
                  end
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card
              className="about-card-style"
              style={{
                backgroundColor: "rgba(255, 235, 144, 1)",
                height: "400px",
              }}
            >
              <Card.Body>
                <Card.Img
                  src={
                    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOgAAADZCAMAAAAdUYxCAAAA8FBMVEX////+/v7TLS3TLiz///3SLi/VLSz//fzKJyf//f//8/XSLjD1t7nALjD//vv9/v//19neh4uzKCnntbS3QUK7Nzb/+vfmoqK/PkDNMCz/0tHz1tnw1dTKMjPBIiTSX16wLi793NzbqanKY2T/6+j55OXrwsC2PDmuKCTWk5PBRUPqiIe2ZGPIKyzQYVzXKy/dc27lo6D2rKulPT/dYGLpeHjdaGnhh4rqnJ3xl5f3ycz/8PbDMjO/IB/MMjWsKCfVgnzekovadHDQlJO/YmTOZGmtODfw19LIP0LyzcnImJSrMC70u8HBTk7cTkv1ubMnvQluAAAUBElEQVR4nO1dC2PbthEGSJAwzYh0nUYOk8iO6yRr1jKc1zRJN8dZu65Zl2z9//9m9wAoPkCJlGTZkXhVlEhHPD7cA4cDoAox0i6RlLfdgy3R3gAdadcIVFci1d5F872TsR67Z6vrsc1XYkmhJeMwiL0toB2duok67yJQq8LVd9l878letdw22ObLVesUQ9i3Ow47CLSrU3JPaAS6WySs7gr+0+OvrodtjU3ezT7cr+t2chlppC+ORu0d6culTc1Ud51GoLtGt205I420Kt227WyL9gToKNFdo1Giu0cj0F2jPQG6N85ob4DujeruHVDR9dUG2V2MJexNdYo+1/eTRfkuXe+djCXsHtXeIHuPnNFIu0a3rVLboj0BetvqNNKmaW9kujdARxpppLtNozMaaaS7TXtlo9KeU95hwvVLpM+/zif5ZJfph3MCevQwDH0/8H0vCCovj1/9PgVDi/IXVHB50d7dqHwyXwAqL/v6CIFKAOoZkPEKFNRpWNFa8Xhg8T4toAQRqGKJ+vAZwO4e+fgfANVKSE0S9UnIu0dGdQFoogzQMLztTt0E+X4YMlBFQEN6+btHYUhAVR3oDiI1QNFGpQHq7bjqRoK97o4CBa+LQCOYRyMCitPrLhICM0BtwIBxBGjwwWDywpI8f6XSZQ3DG++ulyoFpAxUGBv1/RiFHMav33711Vdv3+J7T/ox90I/9HGUitmbAQWJ3r72uDceGNPBj9z40EpalcLrTY7jHligsgQKHYWu5u8Oh9LfjiHOCkkdwqt7fx9a/KcDHCQ/Qw958Lfp9O9Qw+BK6kTFT67Q52QZR0aR9bqoutDZbHY5VUolvWmq9aPjjIBCf4MH96MoUr1r0FpH7w5okMie3j8WiY50/+Y7qp1Cx7654jpLoKIG9OpyqqWEh6m3El9KJvhSSfUL8wkAzYF6Xvzg/lSkVJTZtYerdUlTOtHv3mPZsEC5nj6GR+F75Xq41g1np+zDUK+afvMAJxK/AdT3zahen0wjmUqNjfUhqPLRhxydGFhZgRKVqUrTtF9pxHRy6jHQMAhOH0P8IvoX76gUsKbJQqBhAUC1FEL1XrunsgQaFh4C5RV9z4W/Eien2DZFMCECjaK18x2RTCRINDRAvYqNlhKdnYCNgvKlvSl69CEOGWhYkOpKCYbaryw8+g6BBj4tEgFoEvUtvKDaSMsIbDRgoGHF6xqgvkfOCIDCaCt8YTAsbV6p9sIhQl706JiAZiAVkOhUpUKlMiJed1GqVjJQSgRQnP0ebBRGyRR1lbZFF9Wc4lgBUD/olKjvo0TByhS4TrRTQ5V/Nr6IZAWoHzBQY+SdRSulEaiZmkKfvK6MXE86vujslISRSiOw0W6gno8SlezXtNLlq12nYcyBBgaoTjHn1qMolgZnAEDDwARGJVBpijqALq8ZBZXqOdDQATQwQBVOEqJ8CceLGJiKQWfko31DhwEoOBMUlFxWVLHuq7lEESiqruzR8EIeTnB6sUQD8LoKxplmiPJuPFlE02EyGwbfAkWpYsAA36ZkgWXRdubY1IxCYaCcxkGvK6N20UZpZdnNfpnBi2CGARs11tgh0ZMpqLjUJNESKLQkD59VaIoyi/BBAupXgGJLFaDcTxnViiemcg0CBKCUmMMkYB2oKU0SrhY/pAgdm09TXe9WKqhfUkXG63pzidrIyGMbPZmyuBoSEPL+X378i6V/fEzIZSJLP/rWSjQs59EGgXO6/8sbKvoLvH5+BvqS4NzONmozkD4FDM0KcLZRP2NRIKzi5SFOYZJmoenlL8xA1j+eiVRAVImirkwvjVjXs17XCVSqy2PMec+AJvnXLxTBBNEh0BjKopn5XQFDJKf/PJ5NJjkUnsyOL85BFAmNX0+g4oIy7jP8c/rrcwozAWuaHr49zpGT58D87Ry+AUbKXncpUNkGKhJ1eXVts8LFn19EJI6eQMGHHr6cxNdBUGDx67MjCZE7KJ5xRouBkhL/a0YNF1BB8K/nEQa0Cps//G4S0PdY88NzUGqMGOpAw06gKtJpPdaFCefyKg68ApZ3UO3DFxB64AwEsFpAQa1gyq6VhsXEy9MAH4P+FPnZUUSBuBOorBXleFipsxxCaSSwj4vnkca4PQH3aoHiNJz96ZzabQLtlmgbKPTrZBYUUCzLoPifAShOlhCA1IAGbqASgeYeLFt93HSYvToHEwUbHQSU8uvY8YOL55qXLQkBxZQBRhsIVOM00AVU9wAKQ3gyo0RClmUINMEFBth9P6ASVPeUgrwAyiNQckYY7C0HqkF2GoHiPhi0z0A5pkDVhcHDvOZBhkA5SIcJrglUlquXCtC0DVQA0MBkYQgoD3TDRjuAgkM5fJkXHudwwvzVubASlbBMK4F6TqA0xbw5QMugbgPQKKKYRKYMlNY+npeD6gpVOqOm120BlS5nJMTlVR74jJRVl8KKXs4IvA6oLqXAKDMEQEXCXrcNtB2cQCvRmxzzAjRfI1DJ02wFKBg/AeVNXwwBrxrOSDRt1A0UvG6e4VIWDR+ARlF/r8tAoZ8hp8DOzimI6Q80lW9y3NVEoD4DJWcO0ws4I1JdzwJl/euS6DKgUQTOKA58ztcxUFjeoivrBTRCG2WgrLoGqOwBFEMllCht+OFu58VzybceIpIoDYAfMlDUMQgnejkjF1DwCJczWmOA6RPQlHRX9gMaIVDfY6A+2yg2EfUCirvVbygvxZljdEbSLJkBKKsJvKMzQk+0DCgZOwFV0jG9XM68CtAEM0OJE6jL60Ywj9Kej5EorsVgNeUC2nJG6PbQ65ZJbva6tExB1fXN0oemF5xcUupBy+vO052+Ceol61XdREmiaCIMFK2TEhEtryvaEoXPANTsNldUV3HOaJmNopq+yUnnaGsTgArbRbRR+t5niVLxhtcNm3ldA1Q4gIIEcHqZAwXVNY31AAqtM1BSm4KAihLo+35APZtxwcjIrMgkAo3x0AdtsXQA9ZvLtFKioCpN1W0DjTBkcMyjwhECtoGiGWETqt882gSKBSkxxV63ChSiywVAl6nuehKF3nZJFGz0/ZKgvluiTqDSOqObBupcvSwAunT1gkB1Hag0QKNFQLtWL6uprmwCnbpWL0oedqhuDWi36mqX6kZOoKi6da/rdQFVa9moa3rptlEA+n4gUL8CNC2B+g2gerlEhdiu1+05vdSAVr1uW3Wx8u716ApAxdpARR+gogVUtoH63UC7JZoqjfml+S99KjdQ9/QC0xjmmMpMHuaT20BTTXtm7ekFort5w+a/lupSntXhdWmdqnpL9Ebm0YMOG20GDA4blYNtVAl9Y0CDHgEDPASNMFDJAUNLdZcGDP6CgIH2c5wSdcW6q9ioNwfqsNFETf/z2wOmhw9/+/5ckYKL+ZbEHGj7ODhlGJw2Kpo2apL3CoF2LrzDsJxHtU7FMolKWiRApU2g+LVsZQGfvHhx//79F/D+4sWTQ62M5iYtoKrPPKo6JCpxyxVVV+pGpl7WM/UrxLoENPDiBaqb0o6lngLhW6ITwukGOihgSNtA2UalUV1vfvxmVaByEdBab1O7h02KiOdANFuuWlmiBmhLolFLoguA9l+mkY22gLZtFDcSBWUhETLYDxJtTsq2jQ6Idas2aiTKLas2ULUFoHzqiASICAknrRzcEh0Q1LcCBgaa9gXqUF25jo2a41Vm3z2i7ZG0S3UHxboO1V1so34VqGt6EeuoLiOksabclcmms6H2kmjfEHAu0SZQl0R7A1U9gVo/xJgps2Wq7au60m2j7enldoFW6inNddNA74RESUdJmLjblZrU9R2VqKgDVaSMZQi4RKJyrqylHvOmghJ06HFZCOgEKtoS5ZbTeay7CCgMuzbbVfYoTaS1PpnFBBRPN/LeC4SKyHh0nAcFBvX+QtWNaG6hqSUq/60T/e60wHQRnTk9faISqlnahvFv6A97XczI896L1njCLNKS0p10WNWsXmgKg2qn3zzwoFsdG8FFfPX2949Pnjx+/KROHz9+/G4W0+kuL46v//3uMX5HjN//OM69ovCCLOgCqqM0efbkMdbbqvnJd9dFUfC1jSC/D5W2n4GGXuexORkZzF5/pm/psd9/vILyhCfIr+59NOWhWy+v4iLsAhqG+eT4w9OnT4+fNul4ltNiAAc2mE3ggU+fmDEBURegfiDwLqCwTPvPh1aVpngMOBmDd336gdr+VHsEGjrO44JvW4V+jK1/KovPAgQKFRRBflpjxGYEHGcYcAQyPKRAAqpcA7y+xoMCuEbh4wK0RR9nWYYM1B2v8IqFy7SXE3gwC/JqtVBFntGKPQtCDzcl+fZhnNWuMMZ5npldB76hAF+ggE1p7G3Gh+xwPx1GHUrEMR55oC1Vl0RpS5mweFlWufwQssFnfDLE7pLS04g+QzEX3UDBQKcvc9RPv14tH3XEDXtER51EIYQt4ltJQQFIQHQF39shBiHH8fEOePtwXoZup7qB0g1OvgBjYyV+4dl+Hm/oCm52FLzLaO94mSJdAYOkrX3sRWCOz1O13E0qbm6eBqhLvAloX3OgaDQwGHg2JaiCMj0j1TL3zrgI9dMJlPWTKrWjWL3l5QX2Nm0QeGYTkBkeO/+ueRT3R/MA9T4OqlWCfAu+9QKli/J2KzqnykM4CHQYxqNLxHymiO/ZUsN4Ihr7TeMEX+ckDno4JPN1SDQzJ1l8vw6Uh84zup1lze8ZN+2PtoN6laYat/axj3FcrZaOQpGZhdQgPAKSCRpAweCKgIQFikXNxeXVYx4DliAhDshAUcA4DWCttRPYBihXFJaqWANqvrLjYBYCobngPQfqUF3wuj/npKhxUK0W/R6Obmgs0w/bigRAST7lQ6HtkrkejsMeWsvmbhgVMJ3y20A93mv1aLy8OgUNW2dNtrbFdS50RuaRap2kGhmNblBaHEmhQtbi/LBqsKUnssPeYPklcvc8GtpWaBA9V4NV58BYjeuqqG59UUlAJyx2d5WAuaypidTi54Oyhqrelf1k6UQr7jisXvCxG8F8V5abqPp0M0i2Bs/csQjYuZUPUyNuG5W0tT9BHWncxbVeM6Qhw3mWnJrn8A8UeoaeuefP913LPlZmfZ75rM7RPGptVB099Pi3AdBF41wco4pnjV8KwG+8wDgA4/WoemPBAXvdQwpSIRitAIXQ8/DnHEMK66Djxi8ZBPT7CRmaHPegQvwxpyCh+iMIOZGJMAIzVPMSgY1p4iAnoCo5+uF4crw6nWY80KDs8YN7h5zSbNDh4XenFJEFGMNkk6dQ7tuuCj+s0Zk2QTNPfzhKEgD67PPne/fu/XFvRfrpFLoOQVNYeNdX//31e6Rf+a+Sfr14eE0xNs4FB//7Ccv9wS3+tUqr9mExff78LMHjwZHQeMFP4fpmIOF9Q1iPFsbLgVPNHTSbUWzKRhMHs7MjaIjyZHr+oqscmydaDaaakoYpnWfWQpo01hBKlH70IZ+bPwUppeeyftF4IA5hwvzVkaQDk3Q0ALqBh6BkqrQyh3M3SXz0Hs+5Ya4qSWipnabDK0qjI7qEZ1w6u7zGNGLCLJ708dAjApWcbuDdJhsr2vl3c0DR/wPOKBKUrOJZIE3FUIIajj7EnLNBr10uCmpAiWeXAWF2doSpIWi3Cqgrn7YmmbuzoLBKmQ0DQlnNG1f+15cLPkVgo5hKsnE246yh5TsDMa0XvMzLASiMscloW6K2Sccc7SzvRtfDJu2oNW+ErHyDUYCNfkt5MVgFZ4GJ9ssonz+ZSwwx53bw8gDefdSNzYCV+7CYpNnpEXQeEh2uouPfwwjTVwzU9yoeCKOHoiTGxwEehrLZ2Tm2r/Eo7I3TXCTcYZpakhVuHBugtWCfYmV68b9K0XIkewBAaVs/pd1k89owQEtpytd+cW/LHmpbxR2UN4LNss2E5o3V1nyeAS45I1HbqLhJopsj6OpS6wtWrAbvjxa0FvBDzs56zWV7uU6gkQjzs3ORmGMyNW90I6Skpt0taitZuR2lo0efrij8qYRCEErXI6PZnDvLJxfn9BsMeJ0psb/YcFNASX9Je0iuK1uI0NHR9xdnr1+/Pnv16qwfXXx1TgFn7VcIbtYxrT9geC/g+flAen6YpIJc/tZoA1jxhywGaD65IJrTdbJdoOvphJK8s2si5x5ET+KZDdXzxzs2RGsC5biDbjn2kyoK0ka3zUsjN0hrq4TZAaSQshfQuZdNxfo/FrJFInSsF33XTShWvBL5JZFKrM7ScRoNLkbSxi29nJ9oBYGjItLtCnQ93Z8fIMI7WAAiorfy1fpkl0pko5uwvr609jCV/+w5xVgVTrcU626IcKj4lAmv+cpX5Z/1T2YdrGg22nJX1yA6La7N0QvS3wV+SBo2LYZxQbpN3eXTLc08BJ/eZ4ZoMpxsnhQNW3EkYa8QqEbplMvZEs3KG60u6VTvPld+9qj60yg1RovtenWyU3p1smsdXsge1GqDbdTXKHHzXcgOdifjNtliObs9sK0x6s8bxF6j6NBOWVXtZLjYYghbrs7ecKdqJtxtoD3ZHaaymC2WsDfQqQ168LtL68+jXw7tD9DSG9gZdslfXQ/b+pq8m314QNdHGukLpJoN7DjtC9C9mUdv23i2RXuDdF9wIt228WyLRqC7RrdtOVuiEeiuEadVRP1dCJt1EU62vA32vFMr9VnUs0hlosqdZHI3OZDtrHZJqxvrlCMp2Ch0Q+xO3iB2j1bFkgrlBtm3XnMtA9MWfX+2S3FsuaHsjXVKuhjzJqWzkLgRdrPVPuwV+iw2FnrccRqB7hqtE26MNNJII400jPbG6e7N9LIvOEfaOdofGx2BjjTSSHeEbnvlvx0age4a3bbdbIv2CKr4P0nuzANvu3P4AAAAAElFTkSuQmCC"
                  }
                />
                <Card.Title>
                  <h3>
                    <a href="https://www.npmjs.com/">NPM</a>
                  </h3>
                </Card.Title>
                <Card.Text>
                  Package manager used to build and run our website
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card
              className="about-card-style"
              style={{
                backgroundColor: "rgba(255, 235, 144, 1)",
                height: "400px",
              }}
            >
              <Card.Body>
                <Card.Img
                  src={
                    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATYAAACjCAMAAAA3vsLfAAAA6lBMVEX////iQyn8bSb8oybqUCj8ayb2wLb8pSbiOhn8aibiQSf8oiPhPSH8oib8oiD8oBX8nib8cSb8eibhOx78mCb8hCb8dib8fib+5cznSyj8myb3ZSb8pjD8jybwWif8lCb8iCb86OT+9vPhNRH9uGf8q0L+3r39x4vvWCf8r078nQD+7Nn+1qz/9u3sgW763df0tKjxo5XtinnukoLlUDP/8+b9zZn/9+79w4H91Kf9vXPpcVzxn5D0uK385N/nZE/qdGH3ysHlVDnmWkL51c7zlnrxgWXqSBf8oT39rkr9tV79hgr+rl77uZZ55OBbAAAKYUlEQVR4nO2ca1vbOBCF6xhBYqc2EK4lBZJwacO1pRBaCi0tu93S3f//d9aOY8eSRhcnkm0Zn4+7T5E1mtc6mpHz6lWtWrVq1apVq/zqP1z0tQ/Su+hpHyNX9e46nTvdc/pqdT7dax4jX503Had5p3eMx4HjtNsXegfJVQ9tx7KcwYPWQe7almU1v2odI19dNq1wSh90jtFvBktjtb/pf4Xmpu9hIgRT0jnG4iAcw+k86RwkVz1YznhKA52bQrQ0VaI0YjSY0qW+MSJGK0XpJBGstsa99HEQjeF0qrKXThjVS+l5x5qktNaNJ0fdTxi1rIE2SvvtydJo3nhy1Hk7Dlv7u64xYkaDlHaqQWnvk5NMqaOL0o8xo5WhdDFhNKBU05mxP10arRtPjjqfJoLVPtczxpTRIKUtvYe4fNT74aSmtK2H0qvU0mi1h7kpzWhA6aKOMfrppakGpWlGLaujhdKnQXqMKlCKJ4LlfNJB6RWW0VWg9LGJhU0Lpf2f+BgVoPQjxmhA6Uf1YzwN8LA5luk9hbShiqb0Q32F4ivOaECp6S0FktGA0kfVY/S/tYkx9B3ichLJqA5Kn6ilcRyzKaUY1UHpB5LRgFIt9jA30YmgnlKaUeMpvaIYDSi9UjvGRYdeGqdtdGmcZjSY0k+1UwIY1XWIy0lP0IycplJK+3c0o/pKLbkIYjR4Xyul9KINZLTldAymFGJUdU8OZFSHPcxNFwNwRmo75yCjeg5xOYk69MSUKuycX1hgRlvOtrGU/oRnpJTSS8bSWANTL4MwGA1dlbopfYcZVW8PcxPjZa2U0gcGo2FBVNUYOesba0YKO+dMRo2l9GHADJuj7K4ok1HV9jA3cRJBWee8x2Q0LLWoGSNnAYWJRKoovecsjZmUchhVd7+Fw6ihFysvWfYjmpISSnvw6S1O6Z8qxshZ3ERQRCmXUcsZmHdlqwcUD9NTclR0zs+5S2PilS0+o2o653xGjbz+zGdUDaWLXEbDgqhplPYcfiIEjnf+ntw5WAVNyThK7wWMqqC0z/G6kdp3hlEqeFlbKu63PAoYVXmIy0e8Q088pea8lAoZNe7K1qKQ0fnvt4gZNe7KlkQizD2lR4mlUWMP85LIUEVTGsz3vqZv5QAyilIZRufunG9LLI1Zl0GkEmHOzvmT1NKYdLGy90MmEYK9dB5K4Y4/JYMoFRuqSHN1zmVen5ZRlP71a0FOfy/OrEvZMRZMofTsnxXUkBJqzqxduREa6M9p0fGQ1JG/JzmnxoIczYBaklHb9A6Kjoekbn17XzLddmaN2rZk1DaW3e510QGR0ujQte1NubihWcMmxyhqrNm2bwalW75t28sbcnGbldIdubC9XbJtQyi9DcNmrzWk4rY7W9TkGEXr4ZOYQeno0AsfdumtVNhas4VNilG0+jp8Ets/KjomEhozGmpdarObjVIZRtHKcvQg3peiYyKh4zhsttTrbSZKpRhFe5PncLtnRQdFrIjRUMsyr7eZKH0nEbXW/lL8IAZQmjAavN72ZDCdhVIJRlubSdRMoHTKaBC3fYm4zUCpBKNoZfoYAaWjosMi0pTRMG4SrneGg4KYUdR4k3qM8lN64qcfV8r1bmcOm5jRsc9Nha3slN7gYZNxvZkpFTOK1rGoBZSWvM2MMWpLud7MlAoZRas2IX+r6MBwRTAaal0Yt6yUihhNfG4qbLdFR4YrktFAr1dFcctI6bbg76HG3hL5EG636Mhw9dujwma/EdV6M1IqYhTtU1Gz7WGZKb2mky10vYJ5ZqRUwCjaBJ7B9o+Ljg1Hn6GwBa5XkG7vskRNwCjaoF5spaf0PcBoKIHrzUQpn9FxPRfS8KTo4DB1PYQfWeh6s1DKZ5TwuUZQegoyGkrgejNQyme0tc6Imu0dFh0dpp4ZjIaul3uoz0DpAjfXJvVcSKWl9IyZbEHc+LVeeUp5jAI+dyr/puj4MMRmNBTX9cpTyvsriLEdjOX9Ljo+DB0wGQ21zHO90pTyGG2xtoNJupWT0n7YVuZoj5cospRyGG2BPneqYTmLbqMuP2zcWq8spezQow32dlDmsDHNbiKO65WklM0oUc+FwlbSYylQ/sD1muN65cLGbiszfW6s0pYqz2xRur1hu145Spn/nKznAsn2uej4sLTlCeLGqfVKUcpklK7nkrk2LHE7YetQwOkSu9YrcxWXxSjX54byvLKa3bFGX3z+dsp2vTJtZmbM6XouJv+wpNtBolPBC47peiUoZTEK1nOncv2D0reXX52854LKrvXOzCja5EbN80q7GWA65oLKrPWKKYXtssDn+r/LeaiiddTlJhzD9QophdvKfJ/rDsvd6sN0djDkJBzL9c7GKOJtB75d0hMVQ599zs7AcL0iSkFGW5ztwB0+m3BtN60TjoVjuF5BmxlklFfP9UpbmeRodMsBFdwWBB8pQIwy2ntjld+sweJYOPiGA59SkFFmPTc4TRlwYxcUx8KBrpdLKcQoesv6+55nxrcvsI6ZO8NaVkqBtnJrnRU1/70pZg0W08KBLUBeaZwuhzN9rufflrS2Jq3rA9aZAXC9HEppRpk+1++aZdZg3TB2BsD1cj5SoBll+FzXN86swdr6DYMKuF42pRSjDJ9b8spaFjGqcIDrZVJKMYpWwaiZatZgnboQqPQNB+ZxnmQUrue6vrFmDdYJDCrlelmUkoyiNSDZPN+MyloWHUNnLcr1MiglGQXvLfiHZps1WFsunXBLpOtlUEowCtVzjaqsZdHomU44aluAKcUZhXyu51XBrMG6Ac5ahOsF28w4o2iF8rmVMWuwgEYq4XpBSomWFeVzjaysZdHolrJw+OceCKIUYxTtk5GvllmDddolv2bDW4AQpVhcN4kXW+XMGqyTZzLhsFovQGmaUaqe69nVM2ugRsfEFZvX2LZAU5piFDUIn2t6ZS2Ljg45HznTlKb+J3GNzfNvy39PQZ3IKlza9VKUphhFeD3Xq0RlLYtuMFAx10tSOmUU97kVN2uwCAuXuvhGUpqUSXCfW3mzBqv/JX3WSrlegtIpo1g91+9W36zBOk2ftaY/7UM43qStnK7nusPnl7QX4Lo+BH/aB6c0/s/p+7lVrKxlUaqROv3cYwdiFK1Mt4NqVtayKN1IjWu9GKUxoyjZDlzz26DzK2XhklrvAs3o1OdWow06v6YWLv7IeYdiNPlc40WaNVhJIzV2vYhkNPG5FWqDzq+zpAo3cb0LOKOJz30JlbUsiqtwE9e7QzAa+dwXUlnLovguXFTrRRijk881PPdlmzVQ/cjCTWq9MaXhMX7y8zvmfGCQryILF33uMWkzbzfieq77siprWXQdlcvDWi9KGI3quV7X5EulujW2cONa70LMaOhzXf99bdZ4GlfhQte7O2E0rOd6Jf6BopJo9GXohvd6UcRo6HNfbmUti04DUNdbY0p3GivL7tCAr0HLoMDCBa53N2QU7b30yloGBRZueaVlWe/Qfm3Wsuiou4e2rZ3NYV1Zy6Trgz/vrI26spZZN6u//q3NWnZt/Vf0E9SqVatWrVq1atUyXf8DzCv6cZ9QCMgAAAAASUVORK5CYII="
                  }
                />
                <Card.Title>
                  <h3>
                    <a href="https://gitlab.com/rubenhambardzumyan/cs-373-10am-group-10">
                      GitLab
                    </a>
                  </h3>
                </Card.Title>
                <Card.Text>
                  Houses our project repository and is used for version control
                  and issue tracking
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card
              className="about-card-style"
              style={{
                backgroundColor: "rgba(255, 235, 144, 1)",
                height: "400px",
              }}
            >
              <Card.Body>
                <Card.Img
                  src={
                    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOYAAADbCAMAAABOUB36AAAAV1BMVEX///9B4P1L4f0v3v1x5v3T9v5Y4/3y/P+g7f4p3v3p+/+z8f74/v/8//9l5f2P6/7B8/7d+f/J9f7r+/+K6v6q7/7L9f6y8f556P2b7f7Z+P+L6v6A6f7T1jhTAAAN1UlEQVR4nN1d24LqKgzdFq2inVrH0VHH///OY73WsAIB0qpnPe5tO6wCIeT6718+5s1meRzX9fi432zXCi98Q3zvRtaY0QXGGDvaLeavHpQyqs3oxrADY4v94dVDU8QCkbwz/Z/M6bTmSF6ZjievHqICDj6OF9iyefUoc/FtwzRPUzr6ffVAs7AVsTwT/eAZXUlZtkTLT92jlZxkC7v7TKVhHEfzNKObVw85AYuIJXvjWX7cMfrlPS9ZorNXjzsS+ySapwldvXrkMajSWLZEF68eewRmkObpchKmb8bVq0cvBhi+Lf42i9/ZT2kDXE3xKQu3cYiY+jH2ybLwM7UfsnCdM9Mcn3+wWnqJ2p/XjDsOU4dC6f6oKTxETT0dftixcNashef+d+mb0fffoEc6N0vmh9+eGTXbQcecAGcyea38l9+j7y6InBvY2PPjaskqv/a9Vb8FmSDjvzSv2C1q3lrgLinNkFoz4ybU+NbBq1GTwdbBJ+YemVvkoDzb+Re92PkLMiWSLcZOqALOqvR4pmz9npIRm2/JUwJbZy5Z+6PJ9EC3pswmUI2TL29i2ELPEPNNaX4JH+xz4d4HYzZK97xfOiviJ7f9z6eeUZjaR8KC9o55gYemTFTlXvBH3noMP3JH5XctqRFVmFCiuLN6O8bPABtURY8kd+pYo+RmEJ75eiRRgqKN6a6FpReeuXpkSd4X69ZLNPFG89zp0oy8Ng6zaEfRQkOXZjMUy5McypK3WTSpptgrTM7FJWdvrodkGaW5OMiQtF+DaEGdsWUs24xzc4BLCkE6zXQtaKCjpDu49IvZD3mVWN8YUMg+eEpviQ6oxUuqbqwGn8tRjjuV+jaB/wQBRp6Uvcsk4ehcUDOtcJtTe2ALMy/Bv6rCpvpq0owkf2DJntQUZjZLEYpR2D1u9ok0k0xeKL6mHQGmKb5drCeLY4BpkUjTMWAKzIZzFFncksmk2eKr8ToYTeKqpbJEomogNgX3H5E0T2h8NFPjP6lSG1aDjmgyz3q1Dk2vETj13kkjD4LvQRvTXkIylWielBaeZ/zLzqD6QehoghvzqoWp0fy343jaRHMmtUeHThRwNt4jT/RocmdTsvefGs8DJzCd/Bb3BaBIkwvWTlXf5/RE8YpaGBB/X0eKNKGeNcqwCdFFC99TzbeL2bFG2px9nLSaNKl6lvc297M9y6DpoZkdS2PYQMXuKtKkiSIJW6TqQU7wwY3g5PenbtPF/Dk4XaeLKk16E74hkSa1m5/0qel2thMo0s7HVaXJ2PNN+EkIKoNO18ZQdGnnjz5p+qo0nXFd/2KiY/dLyAjBPkcqqNJkNmeyuZYR3QLQ658uTTwuYXCECxwDLgE1EOvSxDIokebXNjrV5v4XqX6pS5MJwU+gWTVHNjE1CNd5o0vTMVOl0ayasc0wQwJ1SZcmPlEiaU5CZpcQgDqiSxOrezGSdjpLX6tXoJuMLk0ceCQ/N1e5EzliTIm6NCeYpvDpba3h/YBGhiFoynTaplRx8eDL9xCLVuLM9aaTRICx/w0gaQXX6q3X0ov5tMU6XDB2sQHOzaCRZBUZYddexOrl4lC5uiVnLxpACwqENVeR4XXFz2JyOaHcj8ouHF2aNA/oDE+WzD9vogzG4/uABzkbpy5N6Eb02Ujm0RGhD3OoawDnA5FUaX7B5ecJhE2JOLvFIICyD7xIV6UJj03eLR+oG3N52t1/16fdlePxvanShJEqrEa7DXO09e+U0rlKNJCp6zm4VGnCl3ErKZRgYGy5aW/HG8rn7BlDdiKPj0WTZtSaRb7I7mNmf12Cjh3tbBpwjy6v70GTJrRpYH9Y5Xdw27pz1jqrdoHykf0qpSJNXPEGytmpL07HmOXTPdxZtTU6oHFCcg80oVkPal9TH8liQ/aY4561a/eLBgJW9Gg6H519U8XPpRmBreys2g3YHn4frxrNCVyyFkW5sCFXpoDyChvSnv9OIMxEiyYTEIh2JqcUsDXHgLyhCF1plWiumJEDMYuc5y3snl13uxDLoO1QKS4IDx3l4jPRrt1iIw44//D94WDAnAbN6Y4ZOngNswADV28/S4FNLZ/mmluFowKsQqhCBKtv+cO6BXFkOTS/qvl2VrNuALRhYLSJDRqLGMfp7XOGh5oVNuwtw2RRZT90lkhiDn2eTkkAdm/R0RYFPSFZAo9WCm/6nuD5vmgaWKURTabIj+Txz4tyXfqiCQd/AFtM6EZi5ZwsmL4XmqbGziHXmS32CbJCSFb1oQ+arIB3fgk3MAYnhGSZEfo0zY4LLHWuTzHh/kwxAyjPh6DJf17nphEV5YZHKswp7GHRmvECK+FL+sOoQFt4nxVOZk8iyOzRwqXbKy5ksUJCSJog2tOBYpCfkRYAikzEAWeKSLXokWZ7SXbWE/2F6Ch4AITsi68YPabCORo5pRkbA+/oUPIP1WfGn6m/fH8rNufxTWezvUc+SSIqgiJtFG+5Ny8ouloftSH7/bsU7yhp7+gqC9TvEZfSgM9N4XT2TbMbWO/cNqNSWPH7X6cFESoPQSOqN8uBMx6+Sqel6LhxnVtGxLLt5YZixQhnSzzWlVNsTV5VlW0wkXPfrKupDOv5oZkFYnwfVgwgLKVVaHqxHsRa3Sc/3jm9/w4kecp4eurYZ9iC4l1F0z1vSn2oOyiTVLRuvU6xAWl6I5keNy6Usxu2Rnt8hSNZ4qRm7AE7oZ3dCaMwGAvZA4EON+Hoa9WAGbZ5yUPYMqsvoMzwAuj8mcIXV92YPeyYf4p/wCk0gfKDPpIjSfa2Lk0u2qfzvTnPX45/M1wfU5kmp250zD44SKH1VrMvDSZRBZVjbZpMHnl3HFzYJVvxlVYnARgo9uABHCfwZAFjBQqzcl0d0YENqHzqNBnZ//S5+Xg9uwNE3bggN8grYAxVp8lcJeyTWYiteHH64ZgSBVFeruMooCPo02Sy/p7vhb74y6e4xH8g2LKGAeDeQ0WfJj4x6Nf+84kVU3TrbDtrtoF/xFuARp8ms2rpKPxh7sYeb2vX9aO1n8BVRLxOxB5oYlXIkRGhrprX4Gh3zR6ZgfukUA80mbQ/R7+eh9LCjK2byhnh1VjvHtA+1bYHmvg0R4oKEx3WHbpx7UfXh13VyOPh74Gm03znMga0dVLKWN9up+CKPlAeyhXwDow9JtNxbMLN42RylSk+qq0PmlDf4wqrNZE5Yg9BA2KF2JyXPmhS5/sFnCmkOkZNaLH5vm1BsOS5gfdBE8dL8nfCQ1SS6jV7s01udKUQZyfsgyauZeF7ZxNbBaDNxTUluIeieOyeaGL9wGs2/prllK3oAut8b0LztEX3+SUPWuBlOxzNYHhMNVMhCpftcCJIEgW0iM+Ylw2+lwMlo8jDNqu2zBnIW9EHTZhILq7Ptt7nFl8BN+zBlL2Y0LXvzCl1ddvBVPe4CL3pb5khj1zD0GAXsejK5/NNujxyrmSDXatTumlg5VgC+k0HM5JE9QG7IqPOHunQ0ANNxuMRX989vaN8z1UTPYNLKMbrdOKR1fi8/ra/GphncPU+I6Nn/4HmuJPJ5ujP2Oqgv4qmZzBrVlJkn4CmrVyMXdWq2Y+LMNmntEp1mmvOrRdf1pSqGd1RVYdmdq3Fy/HtmmXUaXKleBNKDtMS2ciatF41v/u2dDQKUXnsE22abG+Z+Fr99FX+3Y0cjg8xpE2TO+oSBC2VZf46FfC4vi8hZZpcKEmKBHLeFajSjyxDNyK6NPl2SDb+ZfQ+F9IWUZeim19Flaan1kjCG+n6D3bYQjEqVzGkSdPTeTelARXN5wjvbrRlLmJIs+eC57xOqJJNg28lahRUTdotrUbTXxUw4RbmdJYQxP/DJmLt31aiOfUbH1NaGtLFwdjTnwFPz6UOzXmzC9huUlrgOa0IRE/BRj5cH7FiJsR+/1eDWnjge0aDyhOhtggDk7mucEaMAMPL21LK9Kc2W0eM+u/x17L8S2CZ3JjSVyisV4iEhwPi85bn7nKRrH2zTGu8Tnvjyt/y+xKeia0pMzodo7aNfSO1M2VO7u7wPYATl6ybiR2jYQzd0TmjdXUOzcHFbZHYwCeT5tCNgNPbrdO9GakWcz3qekFq+558mpKSkm/A0jlQoi/mA4khUvUgFlQ9iDQNOjWne4JN8PR1QSwBkYVa5sOQFGcTs0juKd+CcSWrk6zTRewVqT3lW7AZlqpgs71i4Jjp5I8uh2AJSyLFwzGSSA0tVXQfhzSSyYrPE+j2kpbkOwzA0ZYJdmcM6igV6geh4v8KHIt94qULgYayi06UtUZsI8/QmNHuV5FjC39rXAhWwZOb8DCstaPyOPvOPj9cOK1xQ0oV33jENJMsrOY68gbBEbWBbc8n7SY4V4eD2xrX9+uVZ1cq7yZl0NF6rEprXzJLDxtKE074MafvTT1VLkzZ37bSgRMvhitarZceR5XJvCcNADf6D1SKP3h7WQaKgLwH3Jw6YiecLvxtHhMCBV8A4OYvHgNfbepAV/k3F7F3oLGb3X6zmf3VJhT2b8YJzuOXAEdTydyqNj6u7FVI7ypvig9ZsGewwXGhqUwJA3ghkoytphBWzn4bpLieP20qW/irlaGpLD9pV94RZz03KdGB74AoV6VZvruizgJVlMSwuxzf1Ksh9Ie4daM+DOtwi/JOgaEPRqC3tTH7NzcRCLHlJ9SY+kOlK8ICETXG1ossb/H7YfJT2MfdpDUSl8vmYw8QH+bNZjmu67LeLTfN6i0p/geq4alV/ZwzLQAAAABJRU5ErkJggg=="
                  }
                />
                <Card.Title>
                  <h3>
                    <a href="https://react-bootstrap.github.io/">
                      React Bootstrap
                    </a>
                  </h3>
                </Card.Title>
                <Card.Text>
                  Framework that provides components used to build our
                  website&apos;s front end
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        </Row>
        <Row className="container" style={{ marginBottom: "15px" }}>
          <Col>
            <Card
              className="about-card-style"
              style={{
                backgroundColor: "rgba(255, 235, 144, 1)",
                height: "400px",
              }}
            >
              <Card.Body>
                <Card.Img
                  src={
                    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAACoCAMAAABt9SM9AAAA+VBMVEX///8RERH/wwHKysr/rzhWVlbV1dUAAAD/1Zv/kQC1tbX/rCwUFBTh4eH4+Pft7e1ISEjBwcGYmJj/vgAeHh7/sgD/rAAKCgonJyekpKP/qQD/47z/ugBkZGR2dnbl5eVtbW07OzuEhIT/kAD/+/CMjIwsLCwhISG3t7eUlJT/owD/nQCtra2hoaH/iQDY2NheXl7/znNBQUE1NTX/7s//9+n/mQD/2Iz/0WD/1G3/yC//7sr/5a7/4Jn/4af/2YT/zEv/wUP/wXL/8dv/0JH/qj/+37r/zXz/3Kf/vXH/tWX/yTj/0J3/vln/ojP/yYT/rE//y2f/0Y3jROMQAAAHVklEQVR4nO2ae3uiRhTGISyQmHGQiIKJopigYjCxycbddtu9Zbf3dtt8/w/TGWYG8Ro30do+z/v7Q4fhZIB3zjlzGKNpAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPjPccHZ9038XzioMl7v+y7+J9xXDw4Oqi/3fRurofMdRlqyFjr/De407Vsm1sF3Wxux3Xa2NhbHsI2ZYyfydd0+2eo1NuPNAUtamWt9v9bODUui4XSW36bbKfOv605f10nQSZfMfBoai52PY+jiz1otMabZs/iMPGWoZ/Jdk4n0A1frYK2dGUSyVQnyzlhv5e2xxz8bfi8uDdLE9FvaAr1gSefjSLGuff+afztk9JRRtsDbWrXKvrhWB+/W2JVJ15NuMSKW6q3c3qom9UKNP1dHHVvaPK7XJU8JUOVZSSxuRS89YZAtcFFrNptMpLMsEO9WG8a3Wi8UzWuSyE7Ha/WvZdslrsYjZM3FWmP6JKdQYkn2JtZZnYnVZCK95671frVhP9FOiExDtxXZ6RKLpLLd9dhZSsLVQzj+QOtEq8+vZEGswRMG2QYfuGu9VuXDm1Vmrt3W2r6c0ZjI5BpHWtSRFgFvUL27+lIjnWqGnz+3w8U10pErjtxGqk7xUC0PRqkKZCWWI2L4mqTUcRxKnekaUmzvjvs6U6v25rHyoWWyD1N6lEGkalFXi2W2t0Rs5E63hIh7nYpljdol2vX9wNMjR7NMmwSEmNkTW3b72rS9gPgVMSlKrAobnPb6XhT4vm+7bTvOxw7tnYt1dqZp33C1mEiUi1V9u9zQyZxq4JfFYeZFrNd2NVesUCxGs2kfkZXlj2VzT0lULFOvRUKDHQz6ra6fsFHoiZf5pdXv+Am7Eh30gmxQJZbJZoy6RilIDMNwHa2VL8uUrPHoLVGvsxRfr9WaNSbS2ywQlxvyCOLPJ6cyzKoEJpSjObpIWhVTnOro5opaKszSVdmXSY56pgrmIJQCiuXB6rfkmtnuj/lXUSzeq3KWq7ty7JKu1pmdcXg5PGSfXK2mtrZ8iETwdOVUDkh29/EN+6h0eLOtN6RpI9Cj0ZICgepiIVSxTHXlgy5R8iaZe1rTfH6ic1+eE2u6Gt6qsq2yJvq3w8XlcDi80LQah4n0JnOt+yWW6n3D8mU2JpmL3fDPJPOyVEUoU6Fk6qS14F4l35HfwnIqlpU7SEOKpY5ZdcfnYKVYI7nSXPs7LyY+TphaH5lIp1wtJtL76oryIVQLfiSncsw7nEw6kbRaMyVBOe6T8Zx3VcbiW8XyRmLRvqh0V4jl+GKMmOw6vd8fXTI+sdYHFoi1D+yVusnUqp4tWNI8xlJdSNDg4eHa/MDhlRb1krm/SbzbmQco5zWDjOWNxBKBtlIsrZO9QND+ztP758nk8nLIW/dDrhYrH941l74ipkFF0ROJJ0tRWcoSScv1F15uGt7Muhj31BA3QabFZmJF68USZdtgmgN2xI9XE8aPWfunU57kWaPKXeuHeVPTHJ0IRqYMN/4QNyKeEuYp3SVvOT1z5qiVjyFieTOxemvDUK485syVdsHk/Ggy+SwPTuv1Wp2J9H0WiHNbzGV/evOueqcl1JFZ32DzGiyJg5viCqXKMc5I52l5I7HEBsMasRo+1cr2rl9/Pl0dHR2dq6Xv5yFTq3bBN2yYWt/OmsbetE0DUURYvuHK5Y3a6TQhFS2L2zGtm2m7ncm0kVgDnYf3GrF4mZf0N3je53CRafVLfnzMfKv+jaa9rLFInCsfZtwme19m9OJEOb8Zpt7iajTy0umB4zcKp8Y8La8Ra7o1Uck0XiOWFlbYrax91Ofz6znT6nwaby+HXC0m0utmtVr9rWg6m7wtWbF3bypqATyJzE5urFRreAVfYgVRsZAY8IdfI1bghcI8FJc2pMEysSwS6ztO7/fcsa5+L/T8ccrU4uVDjatV3GEez+6qRCIVuZ7aC9QMz8uThk3GcVpK4xtSKcpzO7NDSkmHR68S27KVWCNbeFY6tjsnjW7PF+PSIAq5symx7GIJWuntOr3/yR3rz2LP3ZCpdcpqrFdcrUL5QMPZ9DkQs05D9ULHm7kwTmPc13WdmGnxb9rhbGXRYH87Hbecn3VDlbPcTs+Luu3cosPHS4S8zsxOfurtOL3/fMWD8MtZDou/L1ytGjvZbDarzVfPGN1pP29zqVg6PE4SPG7zLJhS55MXxwVY55CpdfrAyocaV2uPv1B/nVgL7w5b5svV+dHxiyLHh8zdLplYpxfTzdN98VViDfzd/iR2dzUnFVfrTtNecNf6m2+e8n35/f1A/VViRU/6cW1z/pocL/KRlQ98F+KSifS33DzdE9PV8RHcQcncsWNdPBwu4ROLv8OHh4dXb5nFK8a7ZRtb/wrlxc2w5XR9Uln8dRIsh+7ln0MAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAsIJ/ALctmJ/VrgxjAAAAAElFTkSuQmCC"
                  }
                />
                <Card.Title>
                  <h3>
                    <a href="https://aws.amazon.com/amplify/">AWS Amplify</a>
                  </h3>
                </Card.Title>
                <Card.Text>
                  Cloud service used to build and deploy our website
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card
              className="about-card-style"
              style={{
                backgroundColor: "rgba(255, 235, 144, 1)",
                height: "400px",
              }}
            >
              <Card.Body>
                <Card.Img
                  src={
                    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATcAAACiCAMAAAATIHpEAAAAzFBMVEX////9bDX+azP///3/aS/9bDb///j///v+ajH88+fttpr4ZSvse1D7ZizuuqD/+vHqbTrmf1Tyzrr23Mvuq4/sdUfriWLxZi7hajnyZi377OD++O/wy7buxa7quqL+ZSf34M/poYPt0r/tnX7pZzD77uPfhmLgjWnppYjpazf86NncazzrsJXmkG3tn3/mek7df1ndqpLaYCjmvKjZd07vdUXmglr/9ODyjmPacEP85c/vqIL1xabyu5r61rvcgl3dlHbxh1j5cz3jYShWlo6yAAANaUlEQVR4nO2dC3+iOBeHaaAQjBEVFQG1qOgoFe3uTN2Z3Xfntt//O70JIDdFAaeDjfx/uzMtI5Q8PUnOOblxXK1atWrVqk581S9Qq1atlCTTUbvjSavVmoy7qmNKVb/QTctrwySju3H7tqYj/ESFka7ZTXfTXdTwTshv983ZamcjDCEA4OEg8jWEWLc7vZlZ8VvepJTZvE2YRcCSAoTd63amVP2aNyZj09QJs0dC6PEENf8aQdfcLKp+1RuSM10jkGVpcXjE6tZTp+rXvREZUxtfhBZVWKxtjapfuWqR/sDcFKHmk7M3995F8N1+QWoeOdTvVv3mlUre6rAwNSqob+WqX74aUZdN7aPixhYI9dWqi1CRpIZWzth8Aa11lzGEMi9vbD44fX6HbrC8R+CUi1sEHBrcXSNn7ODJyKCY4M64r6Sm0b+maUuBux/9KmwEXPOOwMm7X4Xt4UHo3E3soLxchQ3QBF30LVzeSa8qzdHpbFE+aPprp9PWo0QdQNP76Bkaenm/DWhb1RRFU51r0UU0qbpIv0OqVh4bjIKrceg1PwLbqrA8v0nyFV0pePVyljxNPy3ddug3wx3zfQM/x+Wx6X72iGJ7mUpjFP4LWlVcrDdX94rGDS4PgTzFxomd0HKBPmM7bjCbV7ggaHx4CsXGca3I4OBOYZrbBp3hckFA81s3H5v8h6jqMaatqov2ljLWV6SOgO2HVOY3am3yn5YV65lBm+XUyLR8pxDam/KypZWUcFPjbSXqVVy2N5RhX5WpRF53qnxzRfLXx0/iOF7pwZrdAH9V0tzoiDOAQJh6TzG/fSDgTJN3E30M2lRcujeTXLp1A1hvN9vI9ofqFQ8cZyUDD3ZbuFbZzhRqc1URxdHrwE99KEsCzuykfgskTGXSFRFLZt0AGlgUyGLVR24I7q9B+mmwI1ZbwDdSyYAe2i2Ph/M8Vbv7Xdf75q/XE12MZlVavrcRX65XAGhnefc7z9SzNfuouW1s5v3HE78DzKQrIvZLmBtArt/a+9hIwIEBDKYXHqU+YZ/FiqqWiOiB3vNRWAG2s0YLdKu64r2ZGsWrKdCCTG6IzYlcmROZdtyornhvJemo/7soaAeztZxhK5iI8/z57A1L9maMmEWd3kc4tPxbrWFgdqPh6KzVgjV7ed/CzRtsBhN5LWJt1Nj41rMay1WekqYyl76cFAwWYCeYOe5bG89Jqy8O19XP3kQzm4xxK5hCgvsg2rSe/UqqfH9ZcOKFRhIzN84gLQvNP8LfgpbqgE1++aDQwb8LtJeMWRunFPF6ibebwuZ8mRJHbtG+0CfDJmtzHkw7LzRilTgI38OeVH1uEEOS3EuuDLBNxto3p0BQjxLYeOp/eCNZm4sTW4FmeKPS7KhAMgTMk9ZG/Q/6d/fyI4BmMWZvs9zuG9T+5jwrC7CJ1P/gEiFWNjedtcn5ubmhbePZ4xRgU75/8hw5Oc+ItTduz5RG5x3WQI8AzUXOAxe0bfI36n/wnLnPE96yx22ca7GCh42j4AJr8/wPnibGc4Ub7HHLZ28+Nk76p+1jI/6Hl+EQ3XxRGnvc8rVvW55TrPG2s0Y27RxGf/ozacTvOdeIsMeNpkMuFl37X2+w1pEg6OPW0OJaQ793JNaWs1Nhrz+1cvlvOqZTxYFGzKwx3FL/g7Rt4ofcqZRgCglDWhQZBPz3r1FvYOtfvTupteXNCNA4iy0p7bzcAEZfviw3XaPl+XFK7kpK7+2zFtdfSNTGiq5tx47pBUsUnLksstoSDpgbYHCFnNjG0T2t56+DQmliYVtdAd9Im1z53gQ2jusVXNqL2ZutmmsieQqbVXSInzn3jeMM7XKxI2xe+zYuOj0T2OxNgRMv5zOS1qasCk+MgB3mugWOm17qGJLYnH3xbQvYG84iGl3gkMAmjduZW5plPwGxuIuNfL61CrF5UyvdMsuR2BuV8XTWg4tbWyljI4JuhaV7O50bNKbY+KAbdVxUbiIwGlVdxDfRmWn4MWtTNrZHrfhCcjBkspqemSICtIOlSKNmud1/HhntTamycnCRtamDkvuaPfhjp2yKzq05h82Za6X6A1/QZbKSUs1ODc4cKqmxsq+gxuDQQiDem+N71NwD3bO2RW9dYsPGmGjqjWGDA8me8hHY/RUNKj9cR43BEZm4jmZikUoqf6LLl+m6hLJ72ND7GG7daFV1ksGW17bJdLn86IqF9/RBNmsDWSklNiYAttclmC9T/tql0Mwu2g2kxBdT6oEDYn5bFZrHeiTYYW0c60jxCYSPe3faa0xGs6/tHz+vwAY0ljuFQPGaCiHGiPynnVoWmR8bajC3bCEtnhNTUQMI/i8v6Er+o9nWNTtyncTWZG805qQsO8fcpNwCa6vqAv0u5ZgZnlck4GA0Lk3JHxn9ZeBSI9Wsa5JnGmGN7ViTX2Jxd4etxCSGE4I2myMxZzW7tLjvMrbhHYQJx3I61+wG9wDwnt2dy7JFF3NMy45d0Y3N9CnzsXyW+JID83S77TZt2lgPrTJluKUG/qDu3klslaUyA80A7bp3a2meaOnNRrsIuUeAhq27bdl8BUYj9zxyeeIHAFF7c+dV9CBCT240EbwUeT1SaruJebjrvqtqILHr2ghmHebpb6iK7PnM39asRhaTPJoPdQQTnonnqj14JxUP56O6gqYVGJCitty+jTChdxDGCNl9t6XeeV+QrYCdaKjjzdYddJrNZmfvbjdj1VDqipmpFBpeEkWRweUItWrVqlWrVq1atWrVqlWrVq1atWrVugXlyNUWT+eeuiPjKbeeKxYDJZO0wVtLGblbXlSS/8KLCcUKnTqsKPmt9znvSUlMfOxj0g0ed8Rzs13T13KTnl8lqatBszMfH20GaDSW/XZ/Px1Fp+V+DJ7S9B/3MfzspBmfgCT2dkd7b406eyt1SfwefWyx7NzifLneExSeqARsNxIm5Ox1TK+j9iRxXexpmNyCBYy+LA4XXf8ZEAr0nqdwSxCpKeBpdO8GC7uUBSt94Sm9mlJeC+gw31zVn25x9VsPPwzm2/l83kcgsTzPWQvQXtLrEDVi4+z8FEF9t+1N9xoOC8ep9BHbzwD82NKnhRYiNmFs5wHDBrCTYqTqD2CdGmKV2xDuA76qdpPnRPVwsLpMaejxfU15V4BLg04T3GjQNqL2x9LgekSbHMlZfU+NjnZR+sBmup8XnB9unuKHI249DEB66xW5DcLtWG6V21O4Km+KYwa3sIXXwAqmMP7mG4wmWQ87zS00OLr8N81N3AnNn+mNGim3wzmVt8oNh9wcTYgamhkKN0Sh18PPEzvUSAdy+piJ09wOq8H5OQaPaW6Whv91hX7yPHbK7WCEt89NHArRDnZjHJ47r7zGrksDj1tCYZFPcRNeP0N/9wFSw3+0hRS3BtacMUrthkG4/dTo4fbcu+DWj/GZ4HBbNtLj7SIfai6gVpZXepJb829bGHjnzAja1+cUN2kvNCVHS232Q/qFzy70f3Hvgts+LFQri9sYgcx1Gye59ZUt1ru04xRcatIJboZNkEmd+A/gKDdhQFoHz+Bun5sRb8cy7U3ZQaC71kkn/jQ30bCFvUQruHXEbeJV0Q1O7pJEufFbweuAbp/bKt5TZnLjnD4GUO+0Tkxry7A3+uTuTCe9Zpob6WXaJjVFnPBtKTfS+wr0XK3b5WZ5X4gT4piF/v8ZbtxirmEooPXqKADL4mas4Y8f0HaOuC3WmDp35CckTsf27I36RZMb5vbgroimO13QY8v0znAjJtfr6wJEuwWXVBY3eoYWQCvuiNsI+Y3/FieOEvC5GTbsm7fLDfjxKUa7+L6mZ7mRa909AsJL6nImN9JBgvbimNtcsI0TN/rcuBW9erPcHvcu1aqbCJoucCMINjpAqXWkWdx44qZ5sUiCG8+ZhI/n+QSgDgq+XbSFoULavtvk5p3aeuT/X+RGt28XUomMTHvjzOcvtDVM2dtMx0FP5Ap2rNIfMG7I86xbtbfTu8Zk+m++aKnkNUwdOJHNjTO9TiTFjcT5/0w8DRK2e+BG/u5/fV/cJihMUmTZG3FX27m5Bd8muCl9AJ58CSC+ja/PjXywgR4+/3xX3MYoPBVB9lqh48iKcHvNxy28N8lN1cFrJ9BPGDsdO2zuzCF8eEDviZulh3vadXUc5XmkPzwz9GoRDQMSNxW0tw1pC6RgRIJEB92Qb9RNtNA746YMoeYnKeQOJFX2UCRV11v+19IW4lQGuxg3cSe05ehWIcqnR9zoRmnvihvpy+C64ciL0Q76AbYvS4O6O1uYsurqcJ3KKBXjlsjryevYB2NuyRiB2+T2lLE1p/gBCVizbfLn0IpdH9uCgOz2WhcEO30AQBenj4o5x62BcdSHkkgVhS9CIA6Cfc+VpnB7/QLx2lroP+v0P0rjnY4xJnFoMoJ3XBvR6/ry6EZSiVNH0UvLp3jkKX1Cy/CbDfovZq8TFP0CzS9PHw5fd7XsvHyFklQjc2RctMaTyez4LLXFqLHZjB3peJWRY6XHqc1ZgrppRdYnqWp8fHpmRM+T1einGtbxD3p3+j3vzx/9pJvjlueFjj7zC0vBn/zy5rHVqlWrVi0m9H9BDhGO+pWPFwAAAABJRU5ErkJggg=="
                  }
                />
                <Card.Title>
                  <h3>
                    <a href="https://documenter.getpostman.com/view/17588807/UUy37RGZ">
                      PostMan
                    </a>
                  </h3>
                </Card.Title>
                <Card.Text>
                  Collaboration tool used to document our API
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card
              className="about-card-style"
              style={{
                backgroundColor: "rgba(255, 235, 144, 1)",
                height: "400px",
              }}
            >
              <Card.Body>
                <Card.Img
                  src={
                    "https://www.docker.com/sites/default/files/social/docker_facebook_share.png"
                  }
                />
                <Card.Title>
                  <h3>
                    <a href="https://www.docker.com/">Docker</a>
                  </h3>
                </Card.Title>
                <Card.Text>
                  Used to allow for standardized building of our website for all
                  users
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card
              className="about-card-style"
              style={{
                backgroundColor: "rgba(255, 235, 144, 1)",
                height: "400px",
              }}
            >
              <Card.Body>
                <Card.Img
                  src={
                    "https://cdn-images-1.medium.com/max/1000/1*6M0FxnC6CD9L6xGwROl5jQ.png"
                  }
                />
                <Card.Title>
                  <h3>
                    <a href="https://www.crummy.com/software/BeautifulSoup/">
                      Beautiful Soup
                    </a>
                  </h3>
                </Card.Title>
                <Card.Text>
                  Library used to scrape data sources (such as the Olympic
                  fandom and Wikipedia tables) for data.
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        </Row>
        <Row className="container">
          <Col>
            <Card
              className="about-card-style"
              style={{
                backgroundColor: "rgba(255, 235, 144, 1)",
                height: "400px",
              }}
            >
              <Card.Body>
                <Card.Img
                  src={
                    "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/Pandas_logo.svg/450px-Pandas_logo.svg.png"
                  }
                />
                <Card.Title>
                  <h3>
                    <a href="https://pandas.pydata.org/">Pandas</a>
                  </h3>
                </Card.Title>
                <Card.Text>
                  Library used to store scraped data and then populate the
                  database.
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card
              className="about-card-style"
              style={{
                backgroundColor: "rgba(255, 235, 144, 1)",
                height: "400px",
              }}
            >
              <Card.Body>
                <Card.Img
                  src={
                    "https://www.postgresql.org/media/img/about/press/elephant.png"
                  }
                />
                <Card.Title>
                  <h3>
                    <a href="https://www.postgresql.org/">
                      PostgreSQL Database
                    </a>
                  </h3>
                </Card.Title>
                <Card.Text>
                  Relational Database used to store all of our data.
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card
              className="about-card-style"
              style={{
                backgroundColor: "rgba(255, 235, 144, 1)",
                height: "400px",
              }}
            >
              <Card.Body>
                <Card.Img
                  src={
                    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARwAAACxCAMAAAAh3/JWAAAA/1BMVEX///8AAAA3c6X/0kL/zz43dan/00P/10f/zj03cKD/1UU3d6z/2Ur/zDv/20w3c6b29vYnJyeFhYWqqqo2bJk0NDTX19fz8/Pe3t5LS0v/+u3s7Ozl5eXZ2dm+vr6wsLA7OzuQkJCZmZnLy8uNjY15eXnn8PYdHR1wcHBGRkZhYWFXV1e4uLguLi6hoaFlZWX/6qT/9toSEhJmlcBJeqP/8M6Ip8P/4nYhISFylLT/3mf/z0u2yt3/44MjbKP/ySlFg7X/8MLT4Ov/12y8zuB7osaivNRfjLMiZ5tAfbC3zN//5pbf6PD/11f/6a0eX5D/4GD/34n/1mD/+eb/3HgyMHtnAAAQW0lEQVR4nO2dC1ubyP7HZ6q11epKoOEihDuGWwZLW+ue2mjdaj2nt393z/t/Lf+Z4RIgkGiMkpPwffZZYwKU+fCb320mLQCdOnXq1KlTp4fo+OrLzen1W6rr05vzq+O272hV9OX68tWr8fgV1XOs8Xj8/PL6C9P2jbWu49NXKZZMz6kOD8fj6+9t3127unlVVcbmkOA5Fdq+wRZ1PW5k84xo/G5zjSdhMx6Pa+yGwnl2+GxTXfMVhXJ5fnV+2cDm2e74a9t32ZKuKRtqGm9LbJ7lbHZ3b7+0fZvtiBAZJ2P/Pi6yeTZhs7t73fJdtiRqLqlPuXxeazdY79q9ybZE4SThiPn38wY2mwxnfENffhk3sdlkOK9e/Qe/umqaU5sOZ3z59rLZbjYcThalGthsOpwZc2p39/Vmw8ntJk+Md3c7y5mGU8NmU+EcT+t8XGWzqdOqRle3VTabBUeoMZhc/7mtsiFwPn9K9O1z2zf/qPp+ejmeqSoaCufkBdYe1s7eSdsjeDQxpw394roYnrJJ4Ozvv6Da2/r7qO1RPI6+X766D5sSnBe58fxYSzrH/16ETT6tUjh7W3+1PZDH0P3s5vXrApy9CZudne2fbY9k+bpZwN+U4KRsdrZ21m5iHS9mNxM4eymcra3ttQtZpwuyyeDkdoO10/ZglizmckE2KZwim62LN20PZ7m6Gi/I5vWfBE6JzdbLb20PZ7k6XZTNrxsAvu3k/oZoe3vNovnbBdm8/nUFwF87ZTbbW20PZ6liFmbzFYCjF0U2BM7Lf9oe0DJ1PL1X4E5sXv/6jovynQqb7Yv3bQ9omZr443uyIavFH3ZKc2rt4HwZLzSnXmOHA95vTQJVwublesE5H89hQ5pctyUwv369uyHbAo/2dqps1gxOWlhNsTm8vX339c86nd5c0R2TRz+m2awjnKm1u93br+dzdm59LrDZytisIZwpNofP5mxM+ufnv7br2KwtnAKbd4nVvDn59OFfNfqxh0vMvTo2awYHO+SpOfWMbM05Ovm/P1Lt/7G/vz/ph+5VCqoCm4P1glOz/+b2HL//JkeTAsr5vJjBZs3gXI2nYjhZrHv/Rw2b/bls1gzO8bjK5hZX2/8syGbN4DBTud8t9safFmRzcLBWhSe4PKzkxe+mDGe/6nCa2XxsezjL1fVhpWb4E4Cfi7K5+G/bw1muzsdlNrenAHybF6iKhXiBzcHFmq1cHU92pu9mcD4syObgYt0Wrt6W6/AqnHux+d32YJatL+NSj6IMZ8rfzGJzsG4rM1hfD0v9myKce7JZO8OhO9oKfb8inHuy+bhuHofourjfbwrOXdkcHKzfpCL687YCZ//+bC4+ricbYjs1cGbnfttlNBe/13FOJfry7jZTBqfMZqdoLrStVdDBx5P1qqmquro5TXSO4byostk6OWrWP+trNDXCcCqF+DruaFtQH15UmxRbJ2+m1fZttqMUzlScKnviNWtP3FUJnFKc2pmOUxsMp66ZXs5vNhdOMxuMZdOnVSObix+f/r5Yx5boXfWh2W62yZeH3n/cZDh7jXaT5DufLzYcTu0iTNYH3XTLqV1oOEiP2GA4JztNizAXyfcV3/+vTKuQ09QlX/JN40aKg4+k/D76m3Qp/gc2ZAsjBvSXfdHfWw0NnJcHL08+//xIDOdi+kuvyrLv48HyhvFwWddC0Exe/L3VwIb0tWiaU2M4DnSWdSPLEt+D+nKuZPVgfqm/thvYZK2/KTYahHC0nBtZongIrcFgMPcvxRSU6UMYQVaSvw9R0CQ8Ohhkn/zc225YECdoDipfO1OMM3K2/rCRPIJcGJ8ZEKJ5x3mwp0tEQRDQn5Kn93tx8rhFmMjNDz862bvY3q5hg6fV70pXlE/P1pc6sIeL8fBNxUMIw3lHcrBelEcGhy2e8ebbj+2L1Mck3ob0i39Pb1CS/eRsb2nDWo6Su7LxnGA1ec7fGOomB/umaRo26wb6kPxqJB9qsPBLrqM3n3+efPv0X6zf305+fn7T0C+WpVWEw9IxqWAwIj/N2QdTOmeT3wU8HfP5qEJ4h8nZqLMVhMPBXmRokpKa0Ox8cEAOKWVFGJeWvX4YHDKzpIXPfhxhlxOTUTkcQ/3G0JpxsDCswmEKPN0HwTFXDY5qEzB9yVABh71GnLgUXm46nulV4eBpaU1ePgQOt2JwLMKi50jYGUpnHIAxjtJItSyN5etPqIED1Dz7WS84GnSdPCJHmiKDEMrYhLD36Nc6nzo4E60XnLCfgondMOSIuQgaKf4Qecuoievz4cwJdzO0YnCSzEvyOUsAvAiy6SHbrOX0IetOn7E5cHBw0Z0z4k8tlOVuimX3YY+8GrDVfA48PpyVyXMUOORVxsXZPyPhclFFpt/vQc9RB2zjORU4vDQofsrWZMh310rB0aDsOPieyAB5lETxxJ1GjedU4Dhl/7tGcEIIoI8thjVkjpQOnieLScoSNJ5TgeNBsfgpgWNXzlA4xw98p5IaMKErDeNR4BqTC+RwBGsgKqna+uchZDiAkm55SYIMYZb5mUrceE4Zjjw5iWoajhpkiUJpqqpnk5o+b40SODp5YUw+hHFbnVNnFI/Se3BNlD8jKZwDB+Kwbzq+pGOqpSfLViDwOkmfDK9atJH63TORST/I8+scjmDkSGGvLdPJO0yI0ybvCj0TNjYFEzgFzYJDiiUavegfNJmr2ODSYpXHFpS3jXM46WsYqY1lzBOI2o0qQcQNGaAwWsLEYYzZtRWUJElPbC4uYSzDobVJMgURLDb5CLTsekOYe/8CHBmbnNvyUgQL8Vj7wYgaNwMTp4mHyw2azqBwaFuUkRU8yN4UnMn6QUjgJFci5f7k0GgChxhVZnwTOIOzea2TJ5CaPH0cf3E9ZalJnxTfK9d4Z2WHHMGzGXAED3ppkCJwRvmhUjHi83m44rJLW0U33Z4IGzsgDoBx7XTdU2RA0QWVVYYj1kyr2pUnAqefH2qTP9afGn4GB//UV+EfeOpLPuSom0C2GeW3y3JNJ1TynBqHfAc4VuLLI7F8UArHySdjy7JjDzrx0LI03xFBPlCxsX6owAnLDQoCp6ZaTVywPjGyLFIHpelL4PQU2mevvchTizzDGPscM+qbA+ofFCACWfSbTphfeE6NS+RcmmUWK4MoywS8go0UV35mtWqfSkxsB9SKTUImxP+TFQdoqt54wj3hWHRtoze1sGBl2Wcho07hJOAWHdEyJXE44A5DAFVVBIbPk9jKM4zedPx8OMWiVSMIfI2nWWClVaPpKZ28x0HheEqy/tVc+z6dWEfAT3WoepoJQs2LcfYHQxD5YsPx94HDEKMMqJcXSxlyKjUpK/J0nMsshmJrDJhPJxHyZAgagzgEsa1jD4lzHsVsmvP3gdOfxK5aOFmelXllLssvFfp2Q4v/KdU3cNYhxcglxfkg6xDwTftk5sPJfTkpGbKifVCEI04W5eW44HVotMpfVVKoVmRC+px0nJmhSXYjNnV0phf1iirBiZrgFBa6aIzPkmWCJN1FRZvbvXsOZflSoEkzC54bugVDDhpKT7om2rjDiIWF+rIIh5w2zCzBgcP86mFh/rCTKCXQiTVq3XYC26Axw4FOoYpEDftRuJlxtl/gkYw1CVEWdbFnzhktmXAKHGe2oxcMRC9452RDy1nbfod3qQ0buMyWZIFJ0+SGNJBPmoYjJMo1TzVMRhQmnw2SWOTaecaXkqPUAmQpA47wSDkJtPsDJTXZBhOmSY9mtVqCSjRmeLaMMzZBCem9CGhUU/ohqTDMUfWpTvI62KMDNrNfY1NOLM6lVqXBolI2YeEtarVG/uujjfwOUh3y/HAUYhkLp6eIBioV1aw/RbAXsUQROaOaCpEtFkM98AMpTqOzRaeZHxJDwrHLyUoF2cjWoCWUGSAxp1hyydX9MH8H26G++ELYMhSYCRx8+/gGdZOE1sFyHhjTvFWMEepmZumI1v0xlhXhVCfA3sRT7WDkJ22IEDav7G2UJJU4k4j0FLDzdGnibnHBKlTG7UvEiSAtIoCo58kaw6xC7bcCsiMaQfo8jSQxRyPWQF4GHav2ZarBKnRD5wqqpBLqs9muFBKmRaYuYt1T1sSvq1Munp+KeCspfJt0R3FPVaCDRlmBMJq7b3uuCkT0qQ+rXeQVlc0mLTvScfeQmbTzkPzwR1uBU47O7tzLN/Zrn1R9mqLGfT2OcLaTLJyr3ADef0FWhJoFFSQBzQxUEDsjSprDVQQQXFMCgQdUCXCmZxHLQaxkgj7+M0UZsvoQJ6QysJGu8EN31MdT0VmBBSwg6zK2nViQrWDESsihK8Ihvrv7+0xkAAxHYXo8axPL6WH7UDxiOTanSrLcB5wIoOggDIfHpgEZzQCqB/QBCETgWEpvEIVAF0CggBXZr6M6ZOm8j0tKdKYiR4ppm0WB0f1t5wxxrgNk6tYxHA9fwYoJHIclqFnEAoHW564SJnA0MIiAJyZweGqugQxYpcZPtSMW0e9mjEgpYZMickTuES2wTBI61FpYl9iQbdIemh6ZMFRi5OAPyOB9ByEwQqBnmibQpNBVQI8DugYCA+i2GYJYBXoIoKaYzVupnlCBCfJKGGfK3oi024HWv38hkTY+RHJ+thteBcSQaCmffSjzfPK5hvADkHmRsSwgigzgGSDyioA/FiywIsloFMi04RT3DMeDjIpdEH5XcXGEf9w/WJu11KCuSLxnpXTDP6kmsMGzyYwSbRhU7tC671xjuBldPdaf8eEq1OZUoa4oELIx7NMvVsV5f9cq7SXCBqbf99L63XFaK9oQSHfC4XTZxcYTuXkgxxYlcelvirfAmpvdCIep/qasxCaLGpFlyn76jYjYg76NKwgZKDIQWH0EfQP7T7kHZ/towSKZjcDjMTKpxxB51yJvYfwWAyyRT0xStmSgRvQ1Y9EjkTMAjABkeWDhQ8ECE/hRpcEI2d5kY6Ri2R6MAxKQHdrLJFUYUjXSbeY0hhGqiZDoDySN6TkoHgBX9ogz1zh+ZAlDNwJOqI2AMVI9UtTKHj8CaePIG9BKw8F+ztNAJKHAR74NkIpWI1qlYhwfhWypC+7gJyqx2BIGHIRGqGko2WKDTeysX92jFvBA0ck04kLeMxExCAlndBZgVSAO8eeh6ACQLN+pZ7mHSQp4hFkiDYQcsAwgugCaxsOr36VKiSJTVF1I1xNIdPeNINRdsl0ZG4/vR4ZhBpDW8RFpz5cjvWsC3k3giDqZlBiOhTNfAkeBhNTAAQwpKXGNNUzh4KKC4iJwuCIcHqxc02fg+KEwQDiap1/4hj6UPFaCNZIqX3eQfZaVQcACx2WMyCCThh/akSP4rgxQxOJaoec4xA1bHudZskS+2CX3UUC2iYm4DnV9wY5kM5A1SeQDe/Gvtz2aBJWNQj5ETr8OSCIdjpBQ82DJWwJDHGsWiugPupxA/jdwpt7OYxZDT03+Y8hHK5PnVMQjd2Qj20CI4zXbMRF2NZmn1rnF75pfiT1/SxDDq5rhuDaHTMNwdT0IHKTiMugh19SMlYrPSxAjK8q8dbhOnTp16tSpU6dOnTp16tSpU6dOnTp16tSpU6dOnTp16tSpU6dOnTptkP4fBvgcyW/zWqEAAAAASUVORK5CYII="
                  }
                />
                <Card.Title>
                  <h3>
                    <a href="https://flask.palletsprojects.com/en/2.0.x/">
                      Flask
                    </a>
                  </h3>
                </Card.Title>
                <Card.Text>Web Framework used for out backend.</Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card
              className="about-card-style"
              style={{
                backgroundColor: "rgba(255, 235, 144, 1)",
                height: "400px",
              }}
            >
              <Card.Body>
                <Card.Img
                  src={
                    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAU4AAACXCAMAAABNy0IIAAABSlBMVEX////IGjfMGzheAACrEyzHACbq6urIBC6Ih4fBABvHf4fJETL8/Pzf1daHi6HGACF/AACQk6WrACCWAA+skZPVtbjp5OTLvL2VmKiOjY27TFtaAACsrbNVAACrqqunqLCEABOhfYDCbXd5OkKjABKlpKW4PE2PVFqurq+VlJSio67S09lmAAClqLmcnquedHmASk/fy868vLyyACnAAADk2NrHx8jb29vd3ubZwMJ1AACtr77p6vC7vcalDymAf3/TrLG2KD2uABK+XWm4ABHIlZulACNsGCRsAAClABq3AB/FeICUECauAArCxdJ+gpufQk2LWl91KTN/Ch5qEyFLAABlABHApKe0foS4QFCmGzGuaXBuMDmnUl+fk5rKnqOFIzF+TVSxio6VfoSkNEOkbnSlWmSVIzSDMjy+rrCnR1OQRU6VYmiKbG9t6fVeAAASw0lEQVR4nO1c+1/ayBYPiQnSDFJuBUUeipYWxBdEHiqorWhdtHarLm7b7brX3b13e9f7//96z5mZvCCBqPT6IN/PdguZmWTyzZlzvucMqSD48OHDhw8fPnz48OHDh4/h4mRiYmK5cd+zeCIgEx+Azg8fTu57Ik8DUWQTCa3f90yeAuqczYmJ6H1P5SlgS2dz4oPvPu+OZZNOf7XfHb51DhVFw3dO3PdUngSiunEW73smTwINZp4ftu57Ik8DTcrmtB+HhgI0zigh9z2Np4JlXyENEZgULd/3JJ4Oor7eHCKwNueH9GGBTIB1+mFoWGgCnX6dc1hoTESjflluaFgG4/RF0rBQB+P0RdLQMB2NTngUSY2Tra2iH7P6oQjG2fTW9QSdbDTqOwZ3ECTIm8GdRKenwZQ92/Ioogn0eCtxEmQTEV35znN6BGgsOwOsbdrbGYo6ndPRkTdPQvd8neGRnOa0QefIe88TNy6j01GPCdFJ1KeTgyY+zosd2PEWieomnaOulcBFuiQ+YHNeddKKTqfH/k8WLPGpOxhnQ/AeWorTK9NI6cqoGydNfMiEzWNyS6Nr2Futc5mSOd0cdTZZ4kPs0UinU9jyGFvQOE/q9VEn00h86lu9gGXeAGY9CHOyAvj+k334OIn2rw43wewGJ0YnKyv+LrzARFLfxIdMA1OD1nADOvnbSYAtV5GkA7ziyiDxswVLfeRzS8FbdXgRzLM/V/WVxYGMjwSmPWTl9YEreXFxceTlJqIY9VId3gLj6+cQitDu/1LRc3W4sQLm1+cs0Do7xFk9WmBK7sWsTtzNr14sNhf7G++TwknU9XU1jxKd2d+ikxU3ZtFwFxdHJQ412AtWzq+rbUW9FifBOy46nKIBqxyxOCrGafyo3eGG68CmV+09CybYa+JNxibwebdZPhaYr1ysNHqwfIN9nQau6GI3dDZHxTyNF4J0K52w7ApNe965EKghLuqY7cVo6KRlO4H26tuNNh0bTiSOKJ3I4UQPs9NeK8OIVF82Z0cjYS8aDE7Xu3Ej31kH4yySbjR1NlPf9S4eDnRzdKoaoez0+OM44kwZSc2mAKNinFjOZCvdUWe7VYYru/vP9ncr8GkDgPK9CHTyB0JK0ArNYTzO7DPVEIhigcMplS7ox/VxfTJd0ue0/3cQDO7LzjoGK8O9laDYm5AcDAbl0JuY8DYir8eZcbIHEpsJRbA1KEfaext4DvAa8FdpPWRgvdJ7qbchGzT9+D4bt37kfgcx1qX16WY3/t3g/rraCdDZrZX2QmqAQZVfjQUCIbCK5myKrueYJuuN2B55U9JHXQfN46FeOqc0MWCB+IYfV35k5ws+c5/9v1gXdeZGN30vWOkxz3mZ3i8A/wpQOhVgswo3NhPhpOjcqKEjPnqJDaBtDnRm7XSqOp2V5+IgOpVD8dHQWV/p2rgohZARbfv4eJuTBnSicRJBGQsyprWx7e0x3iq/YnwuadqYhgfgr/fxnutkX4yNjelkwsf/8OOVTXa8n3UeQhftcdApbC0u2jZ5ZmBlqZunHz+eJpNnnM46sFkUyBguOlF9IUnn51I6eTZGCQ0yPtNJ6A8HxMPkTw4xIyt1OsfQDI8hIB5La/rxSrLT6Yz1p1NiYx8FnVgZtmj58Hu4tS+UDzIpdTRKJ2ohIBptU3y58I22xi/OOd3BedqbELpw5V03T11pw/A5pLxtb5j8R186EVMvHgmdgr32mw1aIkm808Hl3ExhHIpF0H8dTJkjL5IdZr4bvDulM+Z2oZ/h1PKFhGfcsDU8KTrJirUyvAYLOmS4vrgE61BeTKXAvWIsETXbOmZ0G3FlAJ0HwMgnAc+o7tkanhSdQhHoNLQ8NUeTkSu401YulVKEXQz4oZJ9aFzCtRuIMGvrT+cUWLd8KazOiUx7meihk2w8m58/uoSnGg6H6VIx6FQuj+aPdrvdSWl/fn7+WczMDcIM1C4qu5e7xnqL7e/H+Oiwgbjt2113ZGeZeRYhX2ye07j+azNF0VxNpyVgE0TSZ2BB7RHSU2kMy9za+tP5BdZ6SxFKSXxgu9aWLjrJfhvyBDUot/ZK65HIe3odRmd8ryXTFut4sq9FsD8cnufrCscBQkfYGJJluTWDJG18guHQbZ8OY32wG0xfMb6t98qSmwGrG02BYAEu9QuGa1H++ivjMxcO4ycilN4GHKlaQ2ML0FvuTyd5y+2LrvZX1iY7nWFN1iVqUMNniwcpnXu7LT1XWDeXScXsHwi2LumxP4NczMbavDH4RlBm9PwkhHxmjbxDhVi6anwL3ZVOkJWziw2BJt8fmWZWg9rXLDXLBrBZ4xdv9SogamyBSBg/96Xzm8yNMktXu3XONjrDLdWSRljpDGiycdx8HhstPd2gDaGf8eDfKhtMmWYnCn5pq3R4gN/Immo8NaBzQf8m3p1OrAynBIK7FVPJY34bsHrefKkIKRqHhN9wLmMOYyXNMNu+dH6FE7RwxdEHENx3oZPQ7Ek8mJs7+8xIMulE1g42D491Qug1QzRCPk8mpc5nalx4/dcH29ucK1GDhIQ+ARGGb8/R0fhc1w4wFwFsf4bJLBzwLwev7l5pOTHrRVmdT0pp6NVkGeKQQJApx9B6fYDsXA6iU3lruFhU7UaSibDSeYSrILh5PZldSHeo47HQqb44z16wCUb4an+FWcfL7FQl/G0t3QHCxDY8s9fn784pO+I/lhZW19LbzIKfd66vqRjBq308f9fp4AWOJwUccQqnFbffSXcmEwtGqdlZHn3+KUG6YzCqyl+ryIYrnZM/iLqx9aPzQjaa2Gq3pPUWOuMtdB1X9HPlVNLsi/03Knq/SeZlUAyLb/jy3JCoLmGGH36J09/Moq1dbOL9vFygj+B0TM88BIKBVOTFrXC7n8q7GeqcTMTvHelsO2AwKuOl4650Xnij8w+wonYMq6cbV4cBuzCy0InhP2jU6k5BhllCke4vLXR+tj2ZuHSsD6B0qn9wojEgBC/Y528/GHQy1cZlHmQZYle6dmsULXQCoaeSdHasFzkisJLj+Hzt8VgwZ8eFTxedyuXurv4l/p4+GYogXXmWmZt0kpe2MBVOiqJqFUoMvxjeeipkV29T6YDuB5BO45n9bUlOaAOnM4z9uQ8CsxiQS3gGQTarFGz/vDz5UUovHWv6nStL+Mnp4U22A3rW2EVneF2O6C7yylINZYiYiaZJ51TbUgoFSOn0KY0MNjoXVP0yKIjENzMmto0wZ6PztQudAjpMllRMyY512ltBrw4DdgHsUykrsbAEj5swPeRwOSrxWJbTReeSZX1+Frn4YeBij8OkE+WYNQPVkyI3On+jp1VN6LrHK50XSwGaq1Hl4bj6boHGLE18EGQdUgg9zSJ/0piIl3s31i1vOLADV1DudIaxiArih+MMT9Y2kjmTzmu1J6FncKaTbNqfEkOQrn5vdJIklxnKe7ErV7s9mjTxocASiHmjwip14j9zpy1qPUMpUZxmdzqxmKS+XtWxdmbraNL50a1K7EInzk7cnrPh+Y/0xN7o5CWEClUeoeH8fhrDul4DoXSaQlbBKKr+SxE26GqXe8zzE64vPlN3On+wFzkrNAIYtNnpdPTQLnTi7MTPWTtYBPdIJ50KaAlQHubBu4FVhxmQzqBlueHzV7+UISpQN9rqKik9kwPm5FzpxGKSbbIddBFGymrxnQcWhY6o7DOLcfGdWDx1pN8zncIC3peGojNS6j3LLVBMpcxfHSCdgYhphCifg9l8Tbig5tnF57MIjfx8kbjSiVuREWvJ+GqTu2QKk86Lwy55OxNpU0Jd6Fzb5r7IQGyPh0uvdNKkQPy36JxC3xyEZ+UMlE5WckGUkC85l88LwjktbQZCzwwPE/9ENz31Yrwrnagm7W43Tle7HkhNOuNpzeZS9iMBcX1XcKKTXvSC6reQ+aRKoWDoU6/u7EMnSfLsvteR3QpVoNPce2N0BuSZErCm7Lepkyvn80WhlGZ8BtvzsYqiVHb3WO2HmzIplWLPaYJU4mB0wocrKn8qpRJ/EEqpdI73EImVSnFSiWeBTvUoXoH7pUFKf2TKfISGXVKpXFA645UK4XQGLyvQX0lTF6Q/fbKPBRH4plQqmF6o83zEa/oAKhWF06nu8QZA9tDYsB0CLCLJQifk6m1NC1Hx3YpX8/k8ES44n4GgHAmFIvz3C5E/2cDSekRmrXodluVR7yMRehY1EtF/HHK5zouQcJ5n++vsKmooBAKnlKYKNdieOTp6RRtaFfwViN5lPcbpDATpL0muknQ7VdaOdncv59syT4rhpLI+osTpDMih1jyn02gQMBjR/ewhbZ1YRJJOp8Z2fFnWLrZiggJ0AuWT6TP7jw+gy/tVPvAvNdALdeabZQTPApTfzK7qf35Uzc7QuJrc5gXXoMqvTv42+9M1vsC+0/TpVKKTFYOQvDIRjzv4f5gjIgadfI2z2ogl8iwcWzZo7girSGJ0iqKWPjYqIOJ7DBhonrAWNpaSZ5qVTG3zmz5wyU40Z+jTpIVOnsPR/F/v8MecWbyi9rEGmZhZIcSr678CCeg3zUu9tMhH3knHAbNdbP+NJ3luGdFLp2in8xvqAwdJfRtYRRK9m4OxsZnVtHSs0cr1wSZVcQToLOPfV53OGJ+qtn28tGr6G2nMCfOTL8wvvJAWP7Qc+npmfmGVpNV05zPLdALac3xaxNofOVg4YF+oniPX6c6xxnPXg81Jeo5NyxTwGb7W2GfUAOHnlgY2dW3gTqoNpOGm90EkpWwueEGSlsCfXCfTkiQlpVUeDmvAZw0/zJvLaPsv6zYA9u9G8lrIJo1vaV1PmV3TF+aXpO42Sgvp5OHc3OZh8ppegFhOQSn4i/XXf0lSep1MLm3CgCUpG++eTJqWSNeS5ohw2tJA8XPwJtWPRnNlesX5zQq7SLK1hKemNiyXyNNoJFRaorH7Av7809Gzr16ncQPEv11dXU1Oec75yNTkVfbqInzLy4Gn7d2jdQO+uI8v9Dq9o1HtNk5X8Gh0uqQ9T0pLx7xerwYjw1Fr9wksO3iufuC/0UFfXnF4a7JLJPUFaM8MESrJbKmyke3gPhdT9Y//tWvcAWh7vY2iTudiynjHoAZ/4vW40iWS3KEogpKBaFSrTdUQxd9PJRoAgl9r1WrtDvdy/yBvrfspg9DU2ex6w4rvY3iiQslkalQsZTLwHyKf//WfS9LS4V85hFcLf3AofZqZwZpYyLPb3TLonO1iksKTcVKZhOaZp5RyRnO///e/BUpn7kG8BHAL7Mu0gm/bpu6PpsFmr2mmvLnOGtikIpQzBpO5bpRvf0f3CcITBKO2NRgNY603ha53rDwGdqbhkdNa9xnYWXK5wp1/iHIvIJtM/q/fYHUZ5tn7JhXIzvxgwwKnCcYJ/8u7TArt0/t8HhBImuKnG21gbq24sAnLGMxzkGGBz8xUhSr4SrdnWCzkCo/yDVf9Z6A3G1Vvppou/5x7OZV3sTmzCwpOAh7T3Y7BOguPX38OAZjs9Dcs5jPLEH/cCasVCoVHGo2GjDLLxd1BfaaSy2T6aYA88PlYxdJQQVgu7ooi9ZkokfqRrgCdmWFP7VGCV4ZdQGCpg0jKZXL906dyoZB43LnmkEBS+T5iqQo+UxEwBxpwFjDPxymWhg2jMuwA5jOruczANLIK5vloU/ehAqORS1MeAzoBNgfHbTDPhC+WBKrTXcQS85llTzWOWqKQ8MUSglWGe0Goz1Q8VuByYJ6+WBJo+M6XHarANeoz80Cnl1WsgHnm4o49lZrz8SeKIi1lZroFk4KVtxqw6U0ClROJxPh4b1JQTOzs7IyPUJwq6pXhLiqUGi0XeRPoSoJivDt7r+6MI3YG1QaeDPQ6e94hkax6rrUXEpxPe0BSGJvA56jIfMM4847Fdm/xujae0FG2Ijeuo/Cdb+OhoIpbaexPxmH/wptxlg02E+Mu+M638VBQzRiW2bMLBPBWGR5M586IRPdaTjfMfK0bGa+V4aqx2M1Vb+N2J/G97+OhQF/lDopIKeS8VYaJzmKXj6wZoWhkciasdOBCd9KGZeDTk/fk5jm+09W7wIXSCGX0BPPyjKOSIUCnJ+GpjLOV3X0WkttBjNZmEnH9Fx+rhULBi2QsjCfGc/mqw2lq+ZxrCXDkgJVhD5KxttPtNX04AvcpB2fcENBHJu+5G3LA5yDPV8al/n+ZzaNHPDFwG51g7Parnd6QH1gZzgCdI6Mr7wolMWCfkpaNRkoJ3QnlRP9t9AIs9RGqD98VA8RScWd0ym9DQbHvPuUoFYeHA6y1uznHMhinL5JuhDhk5C6pO8YhXyTdEJlEYtz5h8q5UdpXGxawYFQo5x2wMzqF9iGiPO6+beGLpBuD0F0LRzpHZttimMDKkh2JAi2/j/txaIio+Y7Thw8fPnz48OHDhw8fDw3/A/h60hrOysItAAAAAElFTkSuQmCC"
                  }
                />
                <Card.Title>
                  <h3>
                    <a href="https://flask.palletsprojects.com/en/2.0.x/">
                      SQL Alchemy
                    </a>
                  </h3>
                </Card.Title>
                <Card.Text>Used to map database relations in Python.</Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card
              className="about-card-style"
              style={{
                backgroundColor: "rgba(255, 235, 144, 1)",
                height: "400px",
              }}
            >
              <Card.Body>
                <Card.Img
                  src={
                    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAS0AAACnCAMAAABzYfrWAAABR1BMVEX9/f3////+/v5MYSh5njympqZMYyd6oT6srKyenp6kpKRqlR+wuaQ8VAzt7e1PT09ISEhBQUHi69ZIXSZ0mTlYcyzi4uJ2nDaMrFzk6N/39/f/mwD/lABKSkpSUlLNzc3CwsLZ2dm3t7c0NDTx8+5aWloAACIbJzpmZmY8PDyCgoLGxsbo6Ojd3d0AABu7u7t3d3cAEywWIzc8Q1FgYGCPj4+XtG4AAABFTFj/jgD/9ulDWRu5zJ9ucnkAABkAAB8lLj4AAA3/yI7/48I3RyEyQCL/q0f/sFX/vXf/1qv/69OzyJcrKyvN08Zvf1YvSwCirJVzg1tmjR+Fk3COrmJfckFWbi5mhTJYXmhkaXJBUyT/wXwtPhYiNQBFUTZob15xfWMVLAB7gnOssKf/oir/tmP/0qLU38Shs4mOoXGtxI5thEldcUDddeRLAAASwElEQVR4nO2d62ObRrrGZ5AlA4lBJA52sAUCWReEZF0sUjtpYifeza3pNj3dpml7eva0p23a7P7/n88MMFzEdRBuIkdvnFiWAGt+ep533hkGAuAmigf40G9grWJDiyY2tGhiQ4smqqHFgE8kNrRoYkOLJja0aGJDiyY2tGhiQ4smKqH1yeAqDQgIstY2UEwsfaTg43zopvwFUY6UbLF1juN5vl6v8zzHcXVDGzHXHxg9KkU3EKj6ciBmbRlcc0/SshLaPBcj5QOra8q15kXJapKgqnBwvAWusR9pWCntVFmFBaYx15YXBSydz9aVry9WqAoXwxT9AstfhfdMPtaKtIARE5ZHLw6R065p9ioKaxRjwhqWpqPQrLYRl5dSibwK6yEhygmL7LwSrTG3RApVpCHdKbLGshHZ1Stx4wpOLGXGYOdVaGlcGIShC0nq09h6SH/c6C/L9Ywz9EoVRIVBDas+kdM2A3pYX1XgKiQJCL+8hXilGLJcrEArDMsYZ20JNJavElcB6yBW/9i5cc/ltVJXGM5e5WnpIVha3g7CJITLzV0Zn9WqLF1WDxrbezfu3XV4XW1nnA9rFMBiU00YCi2UvQDOKAoK5/fQNySn+0Ksvt15UGs0tre29jx9Fe/2sr7K0lL8tvNGUnKPx9h3I29YBkr9eK6CnViyQg0s34MPGjX0B9EivD6sE42g6Uruxm6MAlxBlYYndlhNqGZWhyGsak64tHx9pVcAq0Zey4OkNSle94/YlDESz7FjGl4ZdRH2YKMWpRXo68NoS/FhFVaWiyuZFk79NLxS2wNvfuXpKkqLJn+lfhClq1O/h2NpYCFJptKqZtQNb+7Ukmn5+rqKyG60QKRVqDcMh5WBq/ioO0Nb6bQwr3/ehX99deoPlzVKWBAYWdM7nFFs1F2SFuK1D0qasbwTA2nRz9+PnKweQhaZxFh11J1PK1UfK/3arGiTBtL6EIeFdjYsfSQoqD7Fp9Mig+5i46IVtJVbgKZ3iOWcCIi0jBKwoMBao8jhFbkdriy4Aur6ALRKO9GvtcpIC9FJINgO6YtfYcaQwomM87caX2Y111hFWikhBNmfZ3PbUIm2UNkPac1IT8sfIeoV0oKMFoirnSeu1WnhmcKjm3T1aiknktllysI0N4JRNyeX9WIBWs64Gx59+frrnZ3/IvNfK0ZGq7wekZ9UCysy6layW7CCttDvUe7e/GZnBw8nd1G96uurSP6np8WmG5HJrb8yjyyTQ/NWtrjK0tre2z/68ptvd9xJCkTLqe9vwaDPy5lnpaXlz7Gzo6VXrOHpxcXJQ1zet23bsali225XINu2jH887fdPHrbTD+6XETniSossWtvbu7t33uwEUxQ1TAvzWrlnTGtOsDZkuY6/OO6hmM0f2xDyj45Z/Nzk0ezcwfaw99kYyrPZrNc7733GpuIidW9OoqfW1vb21u53b2q3b9cisevaE1zJOFEJn5dearLy2cXQsNr2vH+uQ7k3/x4/iX7qOUPJfv8CMifzma3pFtfnUmkpJHVli4uS1jYi1cCkGrU0WtU7MXqqdclPjOXuY/XmQwhP5xdYUyf9/gyjkc/nNtSOZy4mJqMzJV7ktVLdYiKt7UZtSVMxWmV+VzYtLXpeOm36YT4/hZCb9XSklOMLbvYWPcfiH9u9WX49689RsNVpC9GKqSrdidVoawlWam16Mj9BEjme1dEuj07Hx1hkD+cXAMrH/Yv8GR4iLk7IwEVPK0dbVTsxehKfNdqxQaJi2EOb1x1asD9HmrLPh7CHVAX6M+ROlOr75yd2GmUvALuKFZNppcG6MifK4fxuyQnaM2a980ePHj9GNQL6aTifAfj2fALf9lion/csvMkQdZmz84tsP1pe8Wtk0KLUVj6tap0YnD1ErBKTtPaoP7fbbcO+cGhZvXNd6fdGkOs9hPys7+4z4k9ns/5xep+IYkwEnPFZVk+rUicGMwSxotSLt/OZ7jw4dZyozGf18Tl6pPXm8O3sob+dwM36j7JO1yrsCgXqR+HEwIeTlN4fzBxJQZyxnAffzx6ySFYI2/H4ohcuN7jZuZVBi4yssiZRP24n+qcBU0+1IloXzgOr1+/j70bv5PsezlCnM3veCzNmZ+eZXePEo6X/lbQqdKK/QCRjkuZ0PmNxuYTyUg93l8KsPz/GD+zZ3C3s2Yc6PqyGKtbMwbe1Qqf4MTjRPyGWMbXcftyfnZzOHp9yMzdLvZ335/i7dt53NAbtxz20xUVv/ih9WO3s4NHKGCpegbYqc6LfIWa2kp2jEfNsCODpqdMRGDM8BEJ7X8z7TlYXHs7xuLt3klOiWh+EVmVO9CdLs1ceKVpbw04l+5KDAIUhW+iThKq2BK3U+AicSKZRKp8szaaVMSP4MTsxY7L0KsLrE/mr7RMbKU5clRbgCxmxYNx/8eT58yeXGVt4U1ylTmXk0mo08JfzAAWeJazYiWStVsaiB/Db/WKsXj49Q3F4eJaOy6/lMyYhSmvLgdWo3UbxA4rbb77b3cUzzY4T84RVSFtkjUj6/DCET85+LMTrxycvLi8vfzw8fJG6yRWNEzEkTOmHv+H4bxw/ofj7//zrf7/JXDNIOdNchBa8PCvIy4mDs5eprxWZg0iNFFo/uIz+juKnn37++ee//euXX355bWny6Aj34iufUKSlBe8jjz27zD9Ddvn0gLl/eJC+QZH5LTptbW3t7n53xzmH8ebb//vy7kg+Cv1CVz+VZflitJAbDw8PD55nAQOXzw/Ozl4gIT5P3YYYMXOpDS0tfNLHCefsKyRq8s66Zh4x6sd8WkrRM/lIXgcHZ4fPfn+Z4Mn7L39/dnh2ePYMvfY8w4iF5uVTo9g6CAYEkLzsvlqEG0AGPikzW6G4fHp4cHCAoPz6DKXzly/v43h5+eLJb7+iJ9ErT3FfyBzmSyt7TE2vrRAt75oZUosCJiye1c8nkuq0yCLTl78dYmAITCTcZ565dcOLrKx1FecTY9oKlQskawUEk3vDwrQ0Yo4CtJDlfn/q6CgS6JmnvxN/XqZ3nhY5Vz0pt8qm4IokEKwuIoYs9euSaPnTW5lX1YWBXT55+mtIWb8+fZJBKBTEh3nLKVdzYpJmoo9X0RYgbTAoljADnK9wXL68z+Rv7oS/JClv/mFVJ8ZpgMTnSlSngT/ol8dThVB4/VZqlF4BvpoVI81QSl96QRPBYresKXnStlWzfEw0VTkxfFlPJfMQiaH7y5rzBz1JVbbTlsK0lk0XOuiqTgzExV+VupR2PYhSMyjucbKuikJxI9mJmSgK/24/glUQbO4l1GVYRa5Sz7+8IMmGymscXz9Io7X3/nMc3m9cUo3zE35+SWGltBU6o4h6Rr1iXiOL9g4ISRnraGfnAYpaGq0btxTNCUXWNIGJ7g8VRYGCpo1gdllfkJYSvhaHtWShEmKMIoytpet8Sq7+hke1WCzR0jqtZrPZ0XjJXBpXMczCVIHW6Rjl1oPHGiZEl2+x1QROheHD8nyhm0UkaiuXli6p3W63qdfFpk4+LlIKDsQF1KdTCzLe8ytpK3JHgysLvuBdgcrSanGKMII8pjWyh0NOwSuxh0MN2l21O9RtW4NKfTi0hTRchWlBoeB9tsoH1y5/A7hitHisJkxLaLbUVnMK62ZLReZciKoqtSXJgFOpKbVEyguzkrKMEr/VVqWs6qOic77lnbhYdAXsxElHVOyWJA9aQzAc8OOButC0ZnMiS2Jda0qGL66lqq44LVxIXJm8eF4rLqyytLpiUzIdWvi2rFNRGg9ElcM1EcpbDKZltJq6MpwayXfboLwWQ2lfCS+ew2XcakO1YnlLGI0gpqVM1U5LlWRLajWlxQjTAohWm225PQDde0mh5RSS1QLjOa7eHtG9v9JZvo6bgPPWsKnKXEsao6H8QG0NI7QAz1lVONEJRtAmLL5RbhXBs20d125UH+WyPzxaDXIiOn6vEY+WOJwYxqiOiAzULsS0ONuCC3GAaSnYiVazqQkts16JE/0AQiXh3CRppdkSMmSBR43btf/UardDBf0yLVNtSc0O0o+kT0V12lRNeSotOFViES1RbCNaiiQOhi2TskTOo1UscqYBS49kfUToYztCcevdu3ef/+GMA//4488//91IptUdoOhqxqI7ltXmgl10ZWUgSioHoNHtLqzFog21bqvZNYJLYhmnhCWuvEpaV3HrBZcWhnQXM3r3+Rc4HFBffPH+/b1/7u9vb+/eTqKFB4PAFTNeSwbcSn6kK/gBEIDDBAJ5zIQvIMb7AMU/3bF+tODdN//+872LyGH0/t7+/r4zJYNjayuZlq4OBovBUHZQQHd4Q27FiL67iQmPsFE1T2QE292uxXbFcaY1P3JaX+Hk9J+vEaQtH9KeA8UBlEJLQnlLbC0USG4p503SeG/XfYBodVGR703gwAnqKOst0zmBs67acub8bt/xCMUijVaLkweqqUGoaxpSC8RzOAIEmqYrGr5iUEA/A5S61IG7jQ4dWqhbGKNNrfQ3tAa0andiM8l5tOqQExEtTuqYqCOUu2bHFHXQMbtT0xxCTTU70kBBY0bxFbDxNpxLS5TkyatOPdWN15MWqrdEcQjHHZG1W00wNAdWFw0UW6o47KpNAT2eNDsGp6oLW+tIhi2agkdLX4jT9PasA63b1LS6LVNdjCHfaim62LQmrMYMxKnSUgdwKLaErjgdybqsoO9QqxsQOdCl1RKHLTFjAvx60hKHBhpYC0NRlSSpM4HG0BQDWspQEpuDCXRoQZ0TpS6hpYqindGc60kL5S007EEOE9lJe4IKeXUS1haoL1B9P3FotU3JDrSlqmgE/qlpy8nyLZZDThxxXLslcnAaoqXrgoW1hmlNxQXUJJ/WoIvy3SeWt9TBtKWaum6KbVaSxk112g45UZBMWxabnNJVu9xQFK2BKpEsP7LxVM4nReuVJJmSyTHMFKUtk4ODZlPtml3FlLpwanYEw2xKqIJwZpvbZqtjm69Gk05nwpuvxrJpDtLf0DWkpchOoGod6Bw3dqbOBUGWAX5yhL5DuW7jS5UUbaJDzdbQC4rg/UUbjNdzVF2SVvCu3CEOaST+CT/2niZjHvc1xn3d2Tj9DV1HWmQRIMOQNYHkXnhk2UiwVtCdcQgtSclqzPWkReaOCQAPgTsJ4ZMBwU0FyaxW9unE60oLEBIMUVn41Cqz9AQT2nSd57fK0WKCRTMgEJl3CQbwnyZO9VeF+ZunvqG1pYVvtXUncaYZ+ldfMIwvI19vTEhFwUUsTLAQ7to50bkp2R3//lEJecsXiJ+zEnwXN6IPbS1pfRWjtbe3vY1I4ZuS+bdEijkRBNk9EBfpGcMJitgQRJNZ6hv6qGkdPdjB12T6tPb2bmzdw1eJ1SKR4EQQ1w0IJX8QPIp1iOtKC0DlNeLlzgYiUjfufXELKC6gTFrhBgc5KS1ASG/eLmtJC729o9e1nTvbe3t7++/fOZcbHtUaebRCOvETt5eU/Eo0pUNcY225Yxfl9Z29958fuWOWIqtG8Mglqp3gQfDF+Fb0xQfWm5YTiNcR9P5jtoK0ou0lzc8zor/XejqR+IWQAgW1FXSIZMUM43d7oasLUjrEtdWWaxQ/UztKK6atBPWEbZjVIa4tLSIAXypFnZhwnDwfhnZaXyeCUH3p/FvEibHVWD6PiA39QWQwQlxnbZH6m3z6RZ247KvYFymsQvIFkT3WkVbciUhbjZ3lu8Dm94mpQV6O7HJtnIiHQ6hezesTycxL2HUZHWLk40hf7LYGtKJOdOpVwRkPNTKcuDToW3KiP04kbBIUt4a0Epzo1Fyovm/sJGoLDbu9/4IsfJBcH0bltMZODDuEdF+Y180HDq9GmBZi9Y6BTNiJqTZM6BATUtga0QJxQ/nw0PD65gMv33u0MKvg2gV/t7wOMTqgXmsnxrCRWtNZSIryva+tvRv772Bk+iGyX6YRl/isqxMDd8SqSOCMt2/i/hHRwqzCC7gjTivaIQZf60gr7EOG+ND/5vxFvFD95enKH1cGexPsFB3i2joxeLRkREDKCaQvcLOGWfkmXN4414ex+mqtnRiYKhBDkJ3wYAhAJuyooKRdSvA5HeLaO9FvYdAhhhTk/QP9xAMirWUC21F0iOvqxCASfOhRCNVjvrYyDRgzYhzOejoxbI5YFbn8IggZkIkaMO7EzA5xXbUV0FjqEMPyAb4FvZ38h0sJPNYhAhA6zLVz4rIR/deWG51vRXLMeIcIrocT4xk+lKJCPWC8KC00Qlx7J4azVLxDjHox1MSg1EgeIS4d8vo58SOJDS2a2NCiiQ0tmtjQookNLZrY0KKJDS2a2NCiiQ0tmtjQookNLZrY0KKJSmh9KrGhRRMbWjSxoUUTG1o0saFFExtaNLGhRRMbWjSxoUUTG1o0saFFExtaNLGhRRP/D2RhWSFeafanAAAAAElFTkSuQmCC"
                  }
                />
                <Card.Title>
                  <h3>
                    <a href="https://aws.amazon.com/elasticbeanstalk/">
                      Amazon Elastic Beanstalk
                    </a>
                  </h3>
                </Card.Title>
                <Card.Text>Used to deploy our backend.</Card.Text>
              </Card.Body>
            </Card>
          </Col>
        </Row>
        <Row className="container">
          <Col>
            <Card
              className="about-card-style"
              style={{
                backgroundColor: "rgba(255, 235, 144, 1)",
                height: "400px",
              }}
            >
              <Card.Body>
                <Card.Img
                  src={
                    "https://miro.medium.com/max/300/1*veOyRtKTPeoqC_VlWNUc5Q.png"
                  }
                  height="50%"
                />
                <Card.Title>
                  <h3>
                    <a href="https://jestjs.io/">Jest Unit</a>
                  </h3>
                </Card.Title>
                <Card.Text>
                  A delightful JavaScript (Typescript) Testing Framework
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card
              className="about-card-style"
              style={{
                backgroundColor: "rgba(255, 235, 144, 1)",
                height: "400px",
              }}
            >
              <Card.Body>
                <Card.Img
                  src={
                    "https://www.npmjs.com/npm-avatar/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdmF0YXJVUkwiOiJodHRwczovL3MuZ3JhdmF0YXIuY29tL2F2YXRhci9iODU3NTM1NDE2NDQ4MDE2NGY3NGI5NGRiNDEzMzcxND9zaXplPTQ5NiZkZWZhdWx0PXJldHJvIn0.dH1HDQAhGHYj1VjYBfMa3dX2inomB2y0w0iiQlHlJ5s"
                  }
                  height="50%"
                />
                <Card.Title>
                  <h3>
                    <a href="https://enzymejs.github.io/enzyme/">Enzyme</a>
                  </h3>
                </Card.Title>
                <Card.Text>
                  JavaScript Testing utility for React that makes it easier to
                  test React Components
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card
              className="about-card-style"
              style={{
                backgroundColor: "rgba(255, 235, 144, 1)",
                height: "400px",
              }}
            >
              <Card.Body>
                <Card.Img
                  src={
                    "https://www.radview.com/wp-content/uploads/2021/02/selenium_logo_square_green.png"
                  }
                  height="50%"
                />
                <Card.Title>
                  <h3>
                    <a href="https://www.selenium.dev/documentation/webdriver/">
                      Selenium WebDriver
                    </a>
                  </h3>
                </Card.Title>
                <Card.Text>
                  Automating web applications for testing purposes (acceptance
                  tests)
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </CardGroup>
    </div>
  );
}
export default About;
