import React, { useState } from "react";
import "./About.css";
import MUIDataTable from "mui-datatables";
import { useQuery } from "react-query";
import Multiselect from "multiselect-react-dropdown";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Dropdown from "react-bootstrap/Dropdown";
import Row from "react-bootstrap/Row";
import Button from "react-bootstrap/Button";
import "./Events.css";

// used in MUIDataTable; defines the columns that will be shown on the table
const eventCol = [
  { label: "Event ID", name: "event_id", options: { display: false } },
  { label: "Name", name: "event_name" },
  { label: "Sport", name: "sport" },
  { label: "Season", name: "season" },
  { label: "Most Recent Olympics", name: "most_recent_olympics" },
  { label: "Sport Type", name: "sport_type" },
];

// used in MUIDataTable; defines the countries table features
const options = {
  selectableRowsHideCheckboxes: true,
  onRowClick: (rowData: any) => {
    window.location.assign("/events/" + rowData[0]);
  },
  print: false,
  download: false,
  pagination: false,
  filter: false,
  search: false,
  viewColumns: false,
  sort: false,
};

function Events() {
  // Obtain parameters from the page URL
  const queryParams = new URLSearchParams(window.location.search);

  // Obtain active filters and sort
  const filteredName = queryParams.getAll("event_name").map((item: any) => ({
    name: item.replace(/U/g, "U-Z").replace(/-U-Z/g, "-U"),
  }));
  const filteredSport = queryParams
    .getAll("sport")
    .map((item: any) => ({ name: item }));
  const filteredSeason = queryParams
    .getAll("season")
    .map((item: any) => ({ name: item }));
  const filteredRecent = queryParams
    .getAll("most_recent_olympics")
    .map((item: any) => ({ name: item }));
  const filteredType = queryParams
    .getAll("sport_type")
    .map((item: any) => ({ name: item }));
  const activeStr = queryParams.get("sort") + " " + queryParams.get("order");

  // obtain current pages and pagination
  const pParams = queryParams.get("p");
  const curPageNum = parseInt(pParams != null && pParams != "" ? pParams : "1");
  const [page, setPage] = useState(curPageNum);
  const [totalPages, setTotalPages] = useState(1);
  const [disablePrev, setDisablePrev] = useState(curPageNum == 1);
  const [disableNext, setDisableNext] = useState(false);

  const fetchItems = async () => {
    const res = await fetch(
      `https://api.goingforgold.me/api/events${window.location.search}`
    );
    const events = await res.json();
    // Set up paginations
    setTotalPages(events.pages);
    if (page == events.pages) {
      if (page == 1) {
        setDisableNext(true);
        setDisablePrev(true);
      } else if (page > 1) {
        setDisableNext(true);
        setDisablePrev(false);
      }
    } else if (page < events.pages) {
      if (page == 1) {
        setDisableNext(false);
        setDisablePrev(true);
      } else if (page > 1) {
        setDisableNext(false);
        setDisablePrev(false);
      }
    }
    if (window.location.search != "?p=1")
      window.scrollTo({
        top: document.documentElement.scrollHeight,
        behavior: "smooth",
      });
    return events.body;
  };
  const { data, status } = useQuery(["events", page], fetchItems);

  //filter options for dropdown buttons
  const sport_options = [
    { name: "Alpine skiing", id: 1 },
    { name: "Archery", id: 2 },
    { name: "Athletics", id: 3 },
    { name: "Badminton", id: 4 },
    { name: "Basketball", id: 5 },
    { name: "Biathlon", id: 6 },
    { name: "Bobsleigh", id: 7 },
    { name: "Boxing", id: 8 },
    { name: "Canoeing", id: 9 },
    { name: "Cross-country skiing", id: 10 },
    { name: "Curling", id: 11 },
    { name: "Cycling", id: 12 },
    { name: "Diving", id: 13 },
    { name: "Equestrian", id: 14 },
    { name: "Fencing", id: 15 },
    { name: "Field hockey", id: 16 },
    { name: "Figure skating", id: 17 },
    { name: "Football", id: 18 },
    { name: "Freestyle skiing", id: 19 },
    { name: "Golf", id: 20 },
    { name: "Gymnastics", id: 21 },
    { name: "Handball", id: 22 },
    { name: "Ice hockey", id: 23 },
    { name: "Judo", id: 24 },
    { name: "Luge", id: 25 },
    { name: "Modern pentathlon", id: 26 },
    { name: "Nordic combined", id: 27 },
    { name: "Rowing", id: 28 },
    { name: "Rugby sevens", id: 29 },
    { name: "Sailing", id: 30 },
    { name: "Shooting", id: 31 },
    { name: "Short track speed skating", id: 32 },
    { name: "Skeleton", id: 33 },
    { name: "Ski jumping", id: 34 },
    { name: "Snowboarding", id: 35 },
    { name: "Speed skating", id: 36 },
    { name: "Swimming", id: 37 },
    { name: "Synchronized swimming", id: 38 },
    { name: "Table tennis", id: 39 },
    { name: "Taekwondo", id: 40 },
    { name: "Tennis", id: 41 },
    { name: "Triathlon", id: 42 },
    { name: "Volleyball", id: 43 },
    { name: "Water polo", id: 44 },
    { name: "Weightlifting", id: 45 },
    { name: "Wrestling", id: 46 },
  ];
  const season_options = [
    { name: "Summer", id: 1 },
    { name: "Winter", id: 2 },
  ];
  const sport_type_options = [
    { name: "Individual", id: 1 },
    { name: "Team", id: 2 },
  ];
  const name_options = [
    { name: "Show all", id: 7 },
    { name: "A-F", id: 1 },
    { name: "F-K", id: 2 },
    { name: "K-P", id: 3 },
    { name: "P-U", id: 4 },
    { name: "U-Z", id: 5 },
  ];
  const olympics_options = [
    { name: "Show all", id: 7 },
    { name: "2018 Winter Olympics", id: 1 },
    { name: "2021 Tokyo Olympics", id: 2 },
  ];

  //handle filters
  function handleSelectSport(selectedList: any, selectedItem: any) {
    queryParams.append("sport", selectedItem.name);
    queryParams.set("p", "1");
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/events?${toStr}`);
  }
  function handleRemoveSport(selectedList: any, selectedItem: any) {
    const myCurSports_Arr = queryParams.getAll("sport");
    const new_Arr = myCurSports_Arr.filter(
      (item: any) => item != selectedItem.name
    );
    queryParams.delete("sport");
    new_Arr.forEach((item: any) => queryParams.append("sport", item));
    queryParams.set("p", "1");
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/events?${toStr}`);
  }
  function handleSelectSeason(selectedList: any, selectedItem: any) {
    queryParams.append("season", selectedItem.name);
    queryParams.set("p", "1");
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/events?${toStr}`);
  }
  function handleRemoveSeason(selectedList: any, selectedItem: any) {
    const myCurSports_Arr = queryParams.getAll("season");
    const new_Arr = myCurSports_Arr.filter(
      (item: any) => item != selectedItem.name
    );
    queryParams.delete("season");
    new_Arr.forEach((item: any) => queryParams.append("season", item));
    queryParams.set("p", "1");
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/events?${toStr}`);
  }
  function handleSelectSportType(selectedList: any, selectedItem: any) {
    queryParams.append("sport_type", selectedItem.name);
    queryParams.set("p", "1");
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/events?${toStr}`);
  }
  function handleRemoveSportType(selectedList: any, selectedItem: any) {
    const myCurSports_Arr = queryParams.getAll("sport_type");
    const new_Arr = myCurSports_Arr.filter(
      (item: any) => item != selectedItem.name
    );
    queryParams.delete("sport_type");
    new_Arr.forEach((item: any) => queryParams.append("sport_type", item));
    queryParams.set("p", "1");
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/events?${toStr}`);
  }
  function handleSelectName(selectedList: any, selectedItem: any) {
    if (selectedItem.name === "Show all") {
      queryParams.delete("event_name");
      queryParams.set("p", "1");
      const toStr = queryParams.toString().replace(/\+/g, "%20");
      window.location.assign(`/events?${toStr}`);
    } else {
      queryParams.set("event_name", selectedItem.name.replace(/-Z/g, ""));
      queryParams.set("p", "1");
      const toStr = queryParams.toString().replace(/\+/g, "%20");
      window.location.assign(`/events?${toStr}`);
    }
  }
  function handleSelectOlympics(selectedList: any, selectedItem: any) {
    queryParams.append("most_recent_olympics", selectedItem.name);
    queryParams.set("p", "1");
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/events?${toStr}`);
  }
  function handleRemoveOlympics(selectedList: any, selectedItem: any) {
    const myCurSports_Arr = queryParams.getAll("most_recent_olympics");
    const new_Arr = myCurSports_Arr.filter(
      (item: any) => item != selectedItem.name
    );
    queryParams.delete("most_recent_olympics");
    new_Arr.forEach((item: any) =>
      queryParams.append("most_recent_olympics", item)
    );
    queryParams.set("p", "1");
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/events?${toStr}`);
  }

  //handle pagination
  const handleFirst = () => {
    queryParams.set("p", "1");
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/events?${toStr}`);
  };
  const handleLast = () => {
    const lastPage = "" + totalPages;
    queryParams.set("p", lastPage);
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/events?${toStr}`);
  };
  const handlePrev = () => {
    const prev = "" + (page - 1);
    queryParams.set("p", prev);
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/events?${toStr}`);
  };
  const handleNext = () => {
    const next = "" + (page + 1);
    queryParams.set("p", next);
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/events?${toStr}`);
  };

  //handle sort
  function handleEventsSort(selectedItem: any) {
    queryParams.set("sort", selectedItem.split(" ")[0]);
    queryParams.set("order", selectedItem.split(" ")[1]);
    queryParams.set("p", "1");
    const toStr = queryParams.toString().replace(/\+/g, "%20");
    window.location.assign(`/events?${toStr}`);
  }

  return (
    <div>
      <link
        href="https://fonts.googleapis.com/css?family=Lobster"
        rel="stylesheet"
        type="text/css"
      />
      <div className="vid-container">
        <img
          className="country-img"
          src="https://images.unsplash.com/photo-1518290581883-8a26c3735cd2?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=3869&q=80"
          alt="stadium"
        />
        <div className="vid-header">
          <h1>Events</h1>
        </div>
        <div className="vid-body-1">
          <h5 className="vid-body-text">
            Here is a list of all the Olympic Events from 1896 to 2016.
          </h5>
        </div>
        <div className="vid-body-2">
          <h5 className="vid-body-text">
            This list can be filtered by name, sport, season, sport type, and
            most recent olympic game.
          </h5>
        </div>
      </div>
      <div className="filter-btns">
        <Container>
          <Row>
            <Col>
              <Button
                className="pagination-btn"
                onClick={() => {
                  window.location.assign(`/eventsSearch?p=1`);
                }}
                variant="outline-dark"
              >
                Search
              </Button>
            </Col>
            <Col>
              <Button
                className="pagination-btn"
                onClick={() => {
                  window.location.assign(`/events?p=1`);
                }}
                variant="outline-dark"
              >
                View All
              </Button>
            </Col>
          </Row>
          <Row>
            <h5 className="filter-header">Filters</h5>
          </Row>
          {/* Filter Buttons */}
          <Row md={5}>
            <Col>
              <Multiselect
                options={name_options} // Options to display in the dropdown
                onSelect={handleSelectName} // Function will trigger on select event
                displayValue="name" // Property name to display in the dropdown options
                placeholder="Name"
                singleSelect
                selectedValues={filteredName}
              />
            </Col>
            <Col>
              <Multiselect
                options={sport_options} // Options to display in the dropdown
                onSelect={handleSelectSport} // Function will trigger on select event
                onRemove={handleRemoveSport} // Function will trigger on remove event
                displayValue="name" // Property name to display in the dropdown options
                placeholder="Sport"
                selectedValues={filteredSport}
              />
            </Col>
            <Col>
              <Multiselect
                options={season_options} // Options to display in the dropdown
                onSelect={handleSelectSeason} // Function will trigger on select event
                onRemove={handleRemoveSeason} // Function will trigger on remove event
                displayValue="name" // Property name to display in the dropdown options
                placeholder="Season"
                selectedValues={filteredSeason}
              />
            </Col>
            <Col>
              <Multiselect
                options={olympics_options} // Options to display in the dropdown
                onSelect={handleSelectOlympics} // Function will trigger on select event
                onRemove={handleRemoveOlympics} // Function will trigger on remove event
                displayValue="name" // Property name to display in the dropdown options
                placeholder="Most Recent Olympics"
                selectedValues={filteredRecent}
              />
            </Col>
            <Col>
              <Multiselect
                options={sport_type_options} // Options to display in the dropdown
                onSelect={handleSelectSportType} // Function will trigger on select event
                onRemove={handleRemoveSportType} // Function will trigger on remove event
                displayValue="name" // Property name to display in the dropdown options
                placeholder="Sport Type"
                selectedValues={filteredType}
              />
            </Col>
          </Row>
          <Row>
            <h5 className="filter-header">Sorts</h5>
          </Row>
          {/* Sort Buttons */}
          <Row>
            <Col>
              <Dropdown>
                <Dropdown.Toggle style={{ width: "100%" }}>
                  Name
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item
                    onClick={() => handleEventsSort("name asc")}
                    active={activeStr === "name asc"}
                  >
                    Ascending
                  </Dropdown.Item>
                  <Dropdown.Item
                    onClick={() => handleEventsSort("name desc")}
                    active={activeStr === "name desc"}
                  >
                    Descending
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </Col>
            <Col>
              <Dropdown>
                <Dropdown.Toggle style={{ width: "100%" }}>
                  Sport
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item
                    onClick={() => handleEventsSort("sport asc")}
                    active={activeStr === "sport asc"}
                  >
                    Ascending
                  </Dropdown.Item>
                  <Dropdown.Item
                    onClick={() => handleEventsSort("sport desc")}
                    active={activeStr === "sport desc"}
                  >
                    Descending
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </Col>
            <Col>
              <Dropdown>
                <Dropdown.Toggle style={{ width: "100%" }}>
                  Season
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item
                    onClick={() => handleEventsSort("season asc")}
                    active={activeStr === "season asc"}
                  >
                    Ascending
                  </Dropdown.Item>
                  <Dropdown.Item
                    onClick={() => handleEventsSort("season desc")}
                    active={activeStr === "season desc"}
                  >
                    Descending
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </Col>
            <Col>
              <Dropdown>
                <Dropdown.Toggle style={{ width: "100%" }}>
                  Sport Type
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item
                    onClick={() => handleEventsSort("sport-type asc")}
                    active={activeStr === "sport-type asc"}
                  >
                    Ascending
                  </Dropdown.Item>
                  <Dropdown.Item
                    onClick={() => handleEventsSort("sport-type desc")}
                    active={activeStr === "sport-type desc"}
                  >
                    Descending
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </Col>
          </Row>
        </Container>
      </div>
      <div>
        {status === "error" && (
          <h3 className="waiting">Error Fetching data!</h3>
        )}
        {status === "loading" && <h3 className="waiting">Fetching data...</h3>}
        <MUIDataTable
          title={""}
          data={data?.map((event: any) => {
            return [
              event?.event_id,
              event?.event_name,
              event?.sport,
              event?.season,
              event?.most_recent_olympics,
              event?.sport_type,
            ];
          })}
          columns={eventCol}
          options={options}
        />
      </div>
      {/* Paginations */}
      <div className="disp-footer">
        <div className="disp-instance">
          <h6>Showing {data?.length} Instances</h6>
          <h6>
            Page {page} of {totalPages}
          </h6>
        </div>
        <div className="disp-pagination">
          <Button
            className="pagination-btn"
            onClick={handleFirst}
            disabled={disablePrev}
            variant="outline-dark"
          >
            First
          </Button>
          <Button
            className="pagination-btn"
            onClick={handlePrev}
            disabled={disablePrev}
            variant="outline-dark"
          >
            Prev
          </Button>
          <Button
            className="pagination-btn"
            onClick={handlePrev}
            variant="outline-dark"
            disabled={disablePrev}
          >
            {page - 1}
          </Button>
          <Button className="pagination-btn" variant="outline-dark" active>
            {page}
          </Button>
          <Button
            className="pagination-btn"
            onClick={handleNext}
            variant="outline-dark"
            disabled={disableNext}
          >
            {page + 1}
          </Button>
          <Button
            className="pagination-btn"
            onClick={handleNext}
            disabled={disableNext}
            variant="outline-dark"
          >
            Next
          </Button>
          <Button
            className="pagination-btn"
            onClick={handleLast}
            disabled={disableNext}
            variant="outline-dark"
          >
            Last
          </Button>
        </div>
      </div>
    </div>
  );
}
export default Events;
