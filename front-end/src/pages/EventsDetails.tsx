import React, { useState, useEffect } from "react";
import Card from "react-bootstrap/Card";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import ListGroup from "react-bootstrap/ListGroup";
import Button from "react-bootstrap/Button";
import "./EventsDetails.css";

type EventDetailsType = {
  bronze_athlete_id: string; // "3441"
  bronze_athletes: string; // "Veronika Vítková"
  bronze_country: string; // "Czech Republic"
  bronze_country_id: number; // 32
  event_id: number;
  event_name: string; // "Women's sprint"
  gold_athlete_id: string; // "812"
  gold_athletes: string; // "Laura Dahlmeier"
  gold_country: string; // "Germany"
  gold_country_id: number;
  most_recent_olympics: string; // "2018 Winter Olympics"
  season: string; // "Winter"
  silver_athlete_id: string; // "2552"
  silver_athletes: string; // "Marte Olsbu"
  silver_country: string; // "Norway"
  silver_country_id: number;
  sport: string; // "Biathlon"
  sport_type: string;
};

function EventsDetails(props: any) {
  useEffect(() => {
    fetchItem();
  }, []);

  const [event, setEvent] = useState<EventDetailsType | undefined>(undefined);
  const [googles, setGoogle] = useState([]);

  // obtain the appropriate event instance specified in the URL from our API
  const fetchItem = async () => {
    const data = await fetch(
      `https://api.goingforgold.me/api/events/${props.match.params.id}`
    );
    const event = await data.json();
    setEvent(event.events);

    // Perform a google image search of the Event and Sport using Google Search API
    // key will be disabled at the end of Fall 2021 Semester
    const myCx = "e5160c5bced8d6de8";
    const myKey = "AIzaSyAfWv5xRzILr3h4cA_DS8PRfjs0lua-HBc";
    if (event && event.events) {
      const searchStr = event.events.event_name.replace(/\s/g, "-");
      const sportStr = event.events.sport.replace(/\s/g, "-");
      const data = await fetch(
        `https://www.googleapis.com/customsearch/v1?q=olympics-${searchStr}-${sportStr}&num=1&start=1&searchType=image&cx=${myCx}&key=${myKey}`
      );
      const googles = await data.json();
      setGoogle(googles.items);
    }
  };

  return (
    <div className="event-background">
      <Container>
        <Card className="event-card" style={{ backgroundColor: "#D6AF36" }}>
          <Card.Body>
            <Container>
              <Button
                className="btn-mine"
                onClick={() => window.history.back()}
                variant="outline-light"
              >
                &lt;
              </Button>
            </Container>
          </Card.Body>
          <Card.Body>
            <h1 className="event-name">
              {event && event.event_name.toUpperCase()}
            </h1>
          </Card.Body>
          <Card.Body>
            {/* Render the image generated from the custom search of the event and sport */}
            {googles.map((google: any) => (
              <div key={google.title}>
                <img
                  width="70%"
                  height="auto"
                  src={google.link}
                  alt={google.title}
                />
              </div>
            ))}
          </Card.Body>
          <ListGroup variant="flush">
            <ListGroup.Item>
              <Card.Title>Background Information</Card.Title>
              <Card.Text className="event-card-text">
                <Container>
                  <Row>
                    <Col className="event-info">
                      <div>
                        <h3>Season</h3>
                        <h5>{event && event.season}</h5>
                      </div>
                    </Col>
                    <Col className="event-info">
                      <div>
                        <h3>Sport</h3>
                        <h5>{event && event.sport}</h5>
                        <img
                          src={`https://olympics.com/images/static/sports/pictograms/${
                            event && event.sport.replace(/\s/g, "-")
                          }.svg`}
                          alt="sport icon"
                          height="50px"
                        />
                      </div>
                    </Col>
                    <Col className="event-info">
                      <div>
                        <h3>Sport Type</h3>
                        <h5>{event && event.sport_type}</h5>
                      </div>
                    </Col>
                  </Row>
                </Container>
              </Card.Text>
            </ListGroup.Item>
            <ListGroup.Item>
              <Card.Title>Olympic Medalists</Card.Title>
              {/* pictogram of the sport related to the event */}
              <img
                src="https://www.pngall.com/wp-content/uploads/2017/05/Olympic-Rings-Free-Download-PNG.png"
                className="olympic-logo"
                alt="olympic logo"
              />
              <Card.Text className="event-card-text">
                <Container>
                  <Row>
                    <Col>
                      <div className="gold-medal">
                        <div className="medal-text">G</div>
                      </div>
                      {/* Renders the athletes related to this event */}
                      <div className="athlete-btn">
                        <Button
                          href={`/athletes/${event && event.gold_athlete_id}`}
                          variant="outline-dark"
                        >
                          {event && event.gold_athletes}
                        </Button>
                      </div>
                      {/* Renders the athletes's countries related to this event */}
                      <div className="country-btn">
                        <Button
                          href={`/countries/${event && event.gold_country_id}`}
                          variant="outline-dark"
                        >
                          {event && event.gold_country}
                        </Button>
                      </div>
                      {/* Renders the country map from Google MAP API that correspond to the gold winner of the event */}
                      <div className="iframe-container">
                        <iframe
                          className="responsive-iframe"
                          frameBorder="0"
                          src={`https://www.google.com/maps/embed/v1/place?key=AIzaSyAfWv5xRzILr3h4cA_DS8PRfjs0lua-HBc&q=${
                            event && event.gold_country
                          }`}
                        ></iframe>
                      </div>
                    </Col>
                    <Col>
                      <div className="silver-medal">
                        <div className="medal-text">S</div>
                      </div>
                      <div className="athlete-btn">
                        <Button
                          href={`/athletes/${event && event.silver_athlete_id}`}
                          variant="outline-dark"
                        >
                          {event && event.silver_athletes}
                        </Button>
                      </div>
                      <div className="country-btn">
                        <Button
                          href={`/countries/${
                            event && event.silver_country_id
                          }`}
                          variant="outline-dark"
                        >
                          {event && event.silver_country}
                        </Button>
                      </div>
                      <div className="iframe-container">
                        <iframe
                          className="responsive-iframe"
                          frameBorder="0"
                          src={`https://www.google.com/maps/embed/v1/place?key=AIzaSyAfWv5xRzILr3h4cA_DS8PRfjs0lua-HBc&q=${
                            event && event.silver_country
                          }`}
                        ></iframe>
                      </div>
                    </Col>
                    <Col>
                      <div className="bronze-medal">
                        <div className="medal-text">B</div>
                      </div>
                      <div className="athlete-btn">
                        <Button
                          href={`/athletes/${event && event.bronze_athlete_id}`}
                          variant="outline-dark"
                        >
                          {event && event.bronze_athletes}
                        </Button>
                      </div>
                      <div className="country-btn">
                        <Button
                          href={`/countries/${
                            event && event.bronze_country_id
                          }`}
                          variant="outline-dark"
                        >
                          {event && event.bronze_country}
                        </Button>
                      </div>
                      <div className="iframe-container">
                        <iframe
                          className="responsive-iframe"
                          frameBorder="0"
                          src={`https://www.google.com/maps/embed/v1/place?key=AIzaSyAfWv5xRzILr3h4cA_DS8PRfjs0lua-HBc&q=${
                            event && event.bronze_country
                          }`}
                        ></iframe>
                      </div>
                    </Col>
                  </Row>
                </Container>
              </Card.Text>
            </ListGroup.Item>
          </ListGroup>
        </Card>
      </Container>
    </div>
  );
}
export default EventsDetails;
