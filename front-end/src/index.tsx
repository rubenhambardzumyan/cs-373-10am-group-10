import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import Navbar from "./components/Navbar";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import About from "./pages/About";
import Events from "./pages/Events";
import Countries from "./pages/Countries";
import Athletes from "./pages/Athletes";
import CountriesDetails from "./pages/CountriesDetails";
import EventsDetails from "./pages/EventsDetails";
import AthletesDetails from "./pages/AthletesDetails";
import Search from "./pages/Search";
import { QueryClient, QueryClientProvider } from "react-query";
import AthletesCompare from "./pages/AthletesCompare";
import CountriesCompare from "./pages/CountriesCompare";
import CountriesSearch from "./pages/CountriesSearch";
import EventsSearch from "./pages/EventsSearch";
import Visualizations from "./pages/Visualizations";
import ProviderVisualization from "./pages/ProviderVisualization";

const queryClient = new QueryClient();

ReactDOM.render(
  <QueryClientProvider client={queryClient}>
    <BrowserRouter>
      <div className="App-header">
        <Navbar />
        <Switch>
          <Route exact path="/about" component={About} />
          <Route exact path="/" component={App} />
          <Route exact path="/events" component={Events} />
          <Route exact path="/countries" component={Countries} />
          <Route exact path="/athletes" component={Athletes} />
          <Route path="/events/:id" component={EventsDetails} />
          <Route exact path="/countries/compare" component={CountriesCompare} />
          <Route path="/countries/:id" component={CountriesDetails} />
          <Route exact path="/athletes/compare" component={AthletesCompare} />
          <Route path="/athletes/:id" component={AthletesDetails} />
          <Route path="/search" component={Search} />
          <Route path="/countriesSearch" component={CountriesSearch} />
          <Route path="/eventsSearch" component={EventsSearch} />
          <Route path="/visualizations" component={Visualizations} />
          <Route
            path="/ProviderVisualization"
            component={ProviderVisualization}
          />
        </Switch>
      </div>
    </BrowserRouter>
  </QueryClientProvider>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
