// Caitlin's repo: https://gitlab.com/caitlinlien/cs373-sustainability/-/blob/phase3/frontend/babel.config.js
// Jefferson's repo: https://gitlab.com/forbesye/fitsbits/-/blob/master/front-end/babel.config.js

module.exports = {
  presets: [
    [
      "@babel/preset-env",
      {
        targets: {
          node: "current",
        },
      },
    ],
    "@babel/preset-react",
    "@babel/preset-typescript",
  ],
  plugins: ["@babel/plugin-transform-react-jsx"],
};
