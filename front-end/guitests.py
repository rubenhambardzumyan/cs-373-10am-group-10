# Jefferson's repo: https://gitlab.com/forbesye/fitsbits/-/blob/master/front-end/guitests.py
import os
from sys import platform

if __name__ == "__main__":
    # Use chromedriver based on OS
    if platform == "win32":
        PATH = "./front-end/gui_tests/chromedriver.exe"
    elif platform == "linux":
        PATH = "./front-end/gui_tests/chromedriver"
    else:
        print("Unsupported OS")
        exit(-1)

    # Run all of the gui tests
    os.system("python3 ./front-end/gui_tests/splashTests.py " + PATH)
    os.system("python3 ./front-end/gui_tests/navbarTests.py " + PATH)
    os.system("python3 ./front-end/gui_tests/athletesTests.py " + PATH)
    os.system("python3 ./front-end/gui_tests/countriesTests.py " + PATH)
    os.system("python3 ./front-end/gui_tests/eventsTests.py " + PATH)
