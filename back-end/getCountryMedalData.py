import requests
import pandas
from bs4 import BeautifulSoup
from sqlalchemy import create_engine
import io
from dotenv import load_dotenv
import os

load_dotenv()
from sqlalchemy import create_engine

AWS_DB_KEY = os.getenv("AWS_DB_KEY")
response = requests.get(
    url="https://en.wikipedia.org/wiki/All-time_Olympic_Games_medal_table"
)
print(response.status_code)
table_class = "wikitable sortale jquery-tablesorter"

soup = BeautifulSoup(response.text, "html.parser")
AthleteTable = soup.find("table", {"class": "wikitable"})
df = pandas.read_html(str(AthleteTable))
# convert list to dataframe
df = pandas.DataFrame(df[0])
df.columns = df.columns.droplevel(0)
df.columns.values[11] = "country_num_olympics"
df["country_name"] = df[df.columns[0]].str.split(
    "(",
    expand=True,
)[0]

df = df.iloc[:, 11:17]
df.rename(
    columns={
        "Unnamed: 12_level_1": "country_num_gold",
        "Unnamed: 13_level_1": "country_num_silver",
        "Unnamed: 14_level_1": "country_num_bronze",
        "Total": "country_medals_total",
    },
    inplace=True,
)
df["country_medals_total"] = df["country_medals_total"].str.split(pat="[", expand=True)[
    0
]
df["country_medals_total"] = df["country_medals_total"].astype(int)
print(df)
id = 1
country_id = []
map = []
population = []
gini = []
capital = []
language = []
flag = []
region = []
for country_name in df["country_name"]:
    name = country_name.split("(")[0]
    country_url = "https://restcountries.com/v3/name/" + name
    country_response = requests.get(url=country_url)
    if country_response.status_code != 200:
        map.append("")
        population.append(0)
        gini.append("")
        capital.append("")
        language.append("")
        region.append("")
        flag.append("")
    else:
        current_df = pandas.read_json(country_response.content)
        if "maps" in current_df:
            map.append(current_df["maps"][0]["googleMaps"])
        else:
            map.append("")
        if "population" in current_df:
            population.append(current_df["population"][0])
        else:
            population.append(0)
        if "gini" in current_df:
            gini_dict = current_df["gini"][0]
            print(gini_dict)
            gini_added = ""
            if type(gini_dict) is dict:
                for key in gini_dict:
                    gini_added += str(key) + ": " + str(gini_dict.get(key))
            gini.append(gini_added)
        else:
            gini.append("")
        if "capital" in current_df:
            capital.append(current_df["capital"][0][0])
        else:
            capital.append("")
        if "languages" in current_df:
            languages_dict = current_df["languages"][0]
            languages_added = ""
            comma = False
            for key in languages_dict:
                if comma:
                    languages_added += ", "
                languages_added += str(languages_dict.get(key))
                comma = True
            language.append(languages_added)

        else:
            language.append("")
        if "flags" in current_df:
            flag.append(current_df["flags"][0][0])
        else:
            flag.append("")
        if "region" in current_df:
            region.append(current_df["region"][0])
        else:
            region.append("")
    country_id.append(id)
    id += 1
df["country_map"] = map
df["country_population"] = population
df["country_gini"] = gini
df["country_capital_city"] = capital
df["country_language"] = language
df["country_flag"] = flag
df["country_region"] = region
df["country_id"] = country_id
# print(df)

# df.to_csv(r"countries.csv", index=False)
# Country_id_to_name = df[['country_id', 'Country Name']].copy()
# Country_id_to_name.to_csv(r"country_keys.csv", index = False)

engine = create_engine(AWS_DB_KEY)
df.head(0).to_sql(
    "countries_updated", engine, if_exists="replace", index=False
)  # drops old table and creates new empty table

conn = engine.raw_connection()
cur = conn.cursor()
output = io.StringIO()
df.to_csv(output, sep="\t", header=False, index=False)
output.seek(0)
contents = output.getvalue()
cur.copy_from(output, "countries_updated", null="")  # null values become ''
conn.commit()
