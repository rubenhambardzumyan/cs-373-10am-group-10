import requests
import pandas
from bs4 import BeautifulSoup
from sqlalchemy import create_engine
import psycopg2
import io
import sql
import csv
from io import StringIO
import json
import html5lib
import numpy as np

from sqlalchemy import create_engine
import os
from dotenv import load_dotenv

load_dotenv()

AWS_DB_KEY = os.getenv("AWS_DB_KEY")
source_url = "https://olympics.fandom.com/wiki/?curid="
api_url = "https://olympics.fandom.com/api.php?action=query&format=json&list=categorymembers&cmtitle=Category:Athletes&formatversion=2&cmlimit=500&cmcontinue="

country_id_to_name = pandas.read_csv(r"country_keys.csv")
names = list(country_id_to_name["Country Name"])
country_names = []
for name in names:
    new_name = name.replace("\xa0", "")
    new_name = new_name.lower()
    country_names.append(new_name)
# print(country_names)

event_id_to_name = pandas.read_csv(r"event_keys.csv")
events = list(event_id_to_name["Event"])
events = [x.lower() for x in events]


all_page_ids = []

response = requests.get(url=api_url).json()
while "continue" in response:
    cm_continue = response["continue"]["cmcontinue"]
    print(cm_continue)
    for item in response["query"]["categorymembers"]:
        all_page_ids.append((item["pageid"], item["title"]))
    response = requests.get(url=api_url + cm_continue).json()

athleteNames = []
athleteImages = []
athleteCountry = []
athleteSport = []
athleteGames = []
athleteFullName = []
athleteDOB = []
athleteHeight = []
athleteBirthplace = []
athleteGold = []
athleteSilver = []
athleteBronze = []
athleteResults = []
countryIndexes = []
number = 0
for page_id in all_page_ids:
    number += 1
    # if number >= 40:
    #     break
    athlete_url = source_url + str(page_id[0])
    print(athlete_url)
    athlete_page = requests.get(url=athlete_url)
    soup = BeautifulSoup(athlete_page.text, "html.parser")
    athleteName = page_id[1]
    athlete_image = soup.find("img")["src"]
    country = None
    sport = None
    olympic_games = None
    full_name = None
    dob = None
    Birthplace = None
    height = None
    gold = None
    silver = None
    bronze = None
    country_index = 154

    athlete_medals = soup.findAll(
        "td",
        {
            "class": "pi-horizontal-group-item pi-data-value pi-font pi-border-color pi-item-spacing"
        },
    )
    index = 0
    for instance in athlete_medals:
        if index == 3:
            gold = instance.get_text()
        elif index == 4:
            silver = instance.get_text()
        elif index == 5:
            bronze = instance.get_text()
        index += 1
    athlete_results = soup.find(
        "table", {"class": "article-table article-table-selected"}
    )
    if athlete_results:
        df_of_results = pandas.read_html(str(athlete_results))[0].to_dict("list")
        event_ids = []
        for key in df_of_results["Event"]:
            event_name = str(key).lower()
            found = event_name in events
            event_index = 477
            if found:
                event_index = events.index(event_name) + 1
            if not found:
                for s in reversed(event_name.split()):
                    for name in events:
                        if s.lower() in name:
                            event_index = events.index(name) + 1
                            found = True
                            break
                    if found:
                        break
            event_ids.append(event_index)
        df_of_results["event_id"] = event_ids
        # print(df_of_results)
        athleteResults.append(df_of_results)
        # print(athleteResults)
    else:
        athleteResults.append(None)
    athlete_info = soup.findAll(
        "div", {"class": "pi-item pi-data pi-item-spacing pi-border-color"}
    )
    for instance in athlete_info:
        label = instance.find(
            "h3", {"class": "pi-data-label pi-secondary-font"}
        ).get_text()
        value = instance.find("div", {"class": "pi-data-value pi-font"}).get_text()
        if label == "Country":
            country = value
            if str(value).lower() in country_names:
                country_index = country_names.index(str(value).lower()) + 1
            elif value == "USA":
                country_index = 145
            elif "Russia" in value:
                country_index = 109
        elif label == "Sport":
            sport = value
        elif label == "Olympic Games":
            olympic_games = value
        elif label == "Date of birth":
            dob = value
        elif label == "Birthplace":
            Birthplace = value
        elif label == "Height":
            height = value
        elif label == "Full name":
            full_name = value
    athleteNames.append(athleteName)
    athleteImages.append(athlete_image)
    athleteCountry.append(country)
    athleteSport.append(sport)
    athleteGames.append(olympic_games)
    athleteFullName.append(full_name)
    athleteDOB.append(dob)
    athleteHeight.append(height)
    athleteBirthplace.append(Birthplace)
    athleteGold.append(gold)
    athleteSilver.append(silver)
    athleteBronze.append(bronze)
    countryIndexes.append(country_index)
# athlete_information = [athleteName, full_name,  athlete_image, country, sport, olympic_games, dob, Birthplace, height]
# print(athlete_information)

dict = {
    "name": athleteNames,
    "images": athleteImages,
    "country": athleteCountry,
    "sport": athleteSport,
    "games": athleteGames,
    "full name": athleteFullName,
    "dob": athleteDOB,
    "height": athleteHeight,
    "birthplace": athleteBirthplace,
    "gold": athleteGold,
    "silver": athleteSilver,
    "bronze": athleteBronze,
    "results": athleteResults,
    "country_id": countryIndexes,
}

# print("name", len(athleteNames))
# print("img", len(athleteImages))
# print("country", len(athleteCountry))
# print("sport", len(athleteSport))
# print("full_name", len(athleteFullName))
# print("dob", len(athleteDOB))
# print("height", len(athleteHeight))
# print("birthplace", len(athleteBirthplace))
# print("gold", len(athleteGold))
# print("silver", len(athleteSilver))
# print("bronze", len(athleteBronze))
# print(countryIndexes)
# print("results", len(athleteResults))

# print(dict)
df = pandas.DataFrame(dict)
df["athlete_id"] = np.arange(len(df)) + 1
# print(df)
df.to_csv(r"athletes.csv", index=False)
athlete_id_to_name = df[["athlete_id", "name"]].copy()
athlete_id_to_name.to_csv(r"athlete_keys.csv", index=False)

engine = create_engine(AWS_DB_KEY)
df.head(0).to_sql(
    "athletes", engine, if_exists="replace", index=False
)  # drops old table and creates new empty table

conn = engine.raw_connection()
cur = conn.cursor()
output = io.StringIO()
df.to_csv(output, sep="\t", header=False, index=False)
output.seek(0)
contents = output.getvalue()
cur.copy_from(output, "athletes", null="")  # null values become ''
conn.commit()
