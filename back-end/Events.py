from models import Events

from sqlalchemy import and_, or_
from query_helpers import *


def sort_events(all_events, queries):
    sort = query_helper("sort", queries)

    if sort == None:
        return all_events

    sort_attr = None
    sort = sort[0]
    if sort == "name":
        sort_attr = Events.event_name
    elif sort == "sport":
        sort_attr = Events.sport
    elif sort == "season":
        sort_attr = Events.season
    elif sort == "most-recent-olympics":
        sort_attr = Events.most_recent_olympics
    elif sort == "sport-type":
        sort_attr = Events.sport_type
    else:
        return all_events

    order = query_helper("order", queries)
    order = order[0] if order != None else "asc"
    if order == "desc":
        return all_events.order_by(sort_attr.desc())
    else:
        return all_events.order_by(sort_attr)


def filter_events(all_events, queries):
    gold_country = query_helper("gold_country", queries)
    silver_country = query_helper("silver_country", queries)
    bronze_country = query_helper("bronze_country", queries)
    season = query_helper("season", queries)
    sport = query_helper("sport", queries)
    sport_type = query_helper("sport_type", queries)
    name = query_helper("event_name", queries)
    most_recent_olympics = query_helper("most_recent_olympics", queries)

    if gold_country != None:
        all_events = all_events.filter(Events.gold_country.in_(gold_country))
    if silver_country != None:
        all_events = all_events.filter(Events.silver_country.in_(silver_country))
    if bronze_country != None:
        all_events = all_events.filter(Events.bronze_country.in_(bronze_country))
    if season != None:
        all_events = all_events.filter(Events.season.in_(season))
    if sport != None:
        all_events = all_events.filter(Events.sport.in_(sport))
    if sport_type != None:
        all_events = all_events.filter(Events.sport_type.in_(sport_type))
    if name != None:
        name = range_helper2("A", "ZZZZZZZZZZ", name)
        all_events = all_events.filter(
            and_(
                Events.event_name >= name[0],
                Events.event_name <= name[1],
            )
        )
    if most_recent_olympics != None:
        all_events = all_events.filter(
            Events.most_recent_olympics.in_(most_recent_olympics)
        )

    return all_events


def search_events(all_events, queries):
    q = query_helper("q", queries)
    if q is not None:
        search_terms = q[0].split()
        search_results = set()
        for word in search_terms:
            term_formatting = "%{}%".format(word)
            search_results.add(Events.event_name.ilike(term_formatting))
            search_results.add(Events.sport.ilike(term_formatting))
            search_results.add(Events.season.ilike(term_formatting))
            search_results.add(Events.most_recent_olympics.ilike(term_formatting))
            search_results.add(Events.sport_type.ilike(term_formatting))
            search_results.add(Events.bronze_athletes.ilike(term_formatting))
            search_results.add(Events.bronze_country.ilike(term_formatting))
            search_results.add(Events.gold_athletes.ilike(term_formatting))
            search_results.add(Events.gold_country.ilike(term_formatting))
            search_results.add(Events.silver_athletes.ilike(term_formatting))
            search_results.add(Events.silver_country.ilike(term_formatting))

        all_events = all_events.filter(or_(*tuple(search_results)))
    return all_events
