from flask import Flask, request, jsonify
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from dotenv import load_dotenv
import os
import math
import re

from models import (
    Athletes,
    Countries_updated,
    Events,
    db,
    app,
    country_schema,
    countries_schema,
    athlete_schema,
    athletes_schema,
    event_schema,
    events_schema,
)
from Athletes import *
from Countries import *
from Events import *

load_dotenv()

app = Flask(__name__)
CORS(app)

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv("AWS_DB_KEY")
db = SQLAlchemy(app)
ma = Marshmallow(app)


@app.route("/")
def hello_world():
    return '<img src="https://i.kym-cdn.com/photos/images/original/001/211/814/a1c.jpg" alt="cowboy" />'


@app.route("/api/countries", methods=["GET"])
def get_countries():
    queries = request.args.to_dict(flat=False)
    all_countries = db.session.query(Countries_updated)

    # Filtering, searching, sorting
    all_countries = filter_countries(all_countries, queries)
    all_countries = search_countries(all_countries, queries)
    all_countries = sort_countries(all_countries, queries)

    # Pagination
    p = query_helper("p", queries)
    if p is None:
        p = 1
    else:
        p = int(p[0])

    result = None
    number_of_pages = None
    if p == -1:
        result = countries_schema.dump(all_countries)
        number_of_pages = 1
    else:
        pages = all_countries.paginate(page=p, per_page=10)
        result = countries_schema.dump(pages.items)
        number_of_pages = math.ceil(all_countries.count() / 10)

    return {"body": result, "pages": number_of_pages}


@app.route("/api/countries/<int:index>", methods=["GET"])
def get_country(index):
    country = Countries_updated.query.get(index)
    return jsonify(
        {
            "country": country_schema.dump(country),
            "athletes": athletes_schema.dump(country.athletes),
            "events": events_schema.dump(country.events),
        }
    )


@app.route("/api/athletes", methods=["GET"])
def get_athletes():
    queries = request.args.to_dict(flat=False)
    all_athletes = db.session.query(Athletes)

    # Filtering, searching, sorting
    all_athletes = filter_athletes(all_athletes, queries)
    all_athletes = search_athletes(all_athletes, queries)
    all_athletes = sort_athletes(all_athletes, queries)

    # Pagination
    p = query_helper("p", queries)
    if p is None:
        p = 1
    else:
        p = int(p[0])

    result = None
    if p == -1:
        result = athletes_schema.dump(all_athletes)
    else:
        pages = all_athletes.paginate(page=p, per_page=12)
        result = athletes_schema.dump(pages.items)

    for athletes_data in result:
        if ".png" in athletes_data["athlete_image"]:
            athletes_data["athlete_image"] = athletes_data["athlete_image"][
                0 : athletes_data["athlete_image"].rindex(".png") + 4
            ]
        elif ".jpg" in athletes_data["athlete_image"]:
            athletes_data["athlete_image"] = athletes_data["athlete_image"][
                0 : athletes_data["athlete_image"].rindex(".jpg") + 4
            ]
        if athletes_data["athlete_games"] != None:
            games = athletes_data["athlete_games"]
            match = re.findall("[\D]+[ 0-9]+", athletes_data["athlete_games"])
            games_string = ""
            for game in match:
                games_string += game + ", "
            games_string = games_string[: len(games_string) - 2]
            athletes_data["athlete_games"] = games_string

    number_of_pages = None
    if p == -1:
        number_of_pages = 1
    else:
        number_of_pages = math.ceil(all_athletes.count() / 12)

    return {"body": result, "pages": number_of_pages}


@app.route("/api/athletes/<int:index>", methods=["GET"])
def get_athlete_index(index):
    athlete = Athletes.query.get(index)
    athletes_data = athlete_schema.dump(athlete)
    if ".png" in athletes_data["athlete_image"]:
        athletes_data["athlete_image"] = athletes_data["athlete_image"][
            0 : athletes_data["athlete_image"].rindex(".png") + 4
        ]
    elif ".jpg" in athletes_data["athlete_image"]:
        athletes_data["athlete_image"] = athletes_data["athlete_image"][
            0 : athletes_data["athlete_image"].rindex(".jpg") + 4
        ]
    if athletes_data["athlete_games"] != None:
        games = athletes_data["athlete_games"]
        match = re.findall("[\D]+[ 0-9]+", athletes_data["athlete_games"])
        games_string = ""
        for game in match:
            games_string += game + ", "
        games_string = games_string[: len(games_string) - 2]
        athletes_data["athlete_games"] = games_string

    return jsonify(
        {
            "athletes": athletes_data,
            "country": country_schema.dump(athlete.countries),
        }
    )


@app.route("/api/events", methods=["GET"])
def get_events():
    queries = request.args.to_dict(flat=False)
    all_events = db.session.query(Events)

    # Filtering, searching, sorting
    all_events = filter_events(all_events, queries)
    all_events = search_events(all_events, queries)
    all_events = sort_events(all_events, queries)

    # Pagination
    p = query_helper("p", queries)
    if p is None:
        p = 1
    else:
        p = int(p[0])

    result = None
    number_of_pages = None
    if p == -1:
        result = events_schema.dump(all_events)
        number_of_pages = 1
    else:
        pages = all_events.paginate(page=p, per_page=10)
        result = events_schema.dump(pages.items)
        number_of_pages = math.ceil(all_events.count() / 10)

    return {"body": result, "pages": number_of_pages}


@app.route("/api/events/<int:index>", methods=["GET"])
def get_event(index):
    event = Events.query.get(index)
    return jsonify(
        {
            "events": event_schema.dump(event),
            "athletes": athlete_schema.dump(event.athletes),
            "country": country_schema.dump(event.countries),
        }
    )


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
