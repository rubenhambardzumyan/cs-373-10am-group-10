def query_helper(name, queries):
    try:
        return queries[name]
    except KeyError:
        return None


def range_helper(min_range, max_range, range_query):
    range_query = range_query[0]
    if len(range_query.split("-")) < 2:
        min_range = int(range_query)
    else:
        min_range, max_range = range_query.split("-")
    return [min_range, max_range]


# For comparing strings
def range_helper2(min_range, max_range, range_query):
    range_query = range_query[0]
    if len(range_query.split("-")) < 2:
        min_range = str(range_query)
    else:
        min_range, max_range = range_query.split("-")
    return [min_range, max_range]
