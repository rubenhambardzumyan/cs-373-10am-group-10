from models import Athletes

from sqlalchemy import and_, or_, cast, Integer
from query_helpers import *


def sort_athletes(all_athletes, queries):
    sort = query_helper("sort", queries)

    if sort == None:
        return all_athletes

    sort_attr = None
    sort = sort[0]
    if sort == "name":
        sort_attr = Athletes.athlete_name
    elif sort == "gold-medals":
        sort_attr = cast(Athletes.num_gold_medals, Integer)
    elif sort == "silver-medals":
        sort_attr = cast(Athletes.num_silver_medals, Integer)
    elif sort == "bronze-medals":
        sort_attr = cast(Athletes.num_bronze_medals, Integer)
    elif sort == "sport":
        sort_attr = Athletes.athlete_sport
    elif sort == "country":
        sort_attr = Athletes.country_name
    else:
        return all_athletes

    order = query_helper("order", queries)
    order = order[0] if order != None else "asc"
    if order == "desc":
        return all_athletes.order_by(sort_attr.desc())
    else:
        return all_athletes.order_by(sort_attr)


def filter_athletes(all_athletes, queries):
    country = query_helper("country_name", queries)
    sport = query_helper("athlete_sport", queries)
    bronze_medals = query_helper("num_bronze_medals", queries)
    silver_medals = query_helper("num_silver_medals", queries)
    gold_medals = query_helper("num_gold_medals", queries)
    name = query_helper("athlete_name", queries)
    if country != None:
        all_athletes = all_athletes.filter(Athletes.country_name.in_(country))
    if sport != None:
        if sport == ["Speed skating"]:
            sport = ["Speed Skating"]
        all_athletes = all_athletes.filter(Athletes.athlete_sport.in_(sport))
    if bronze_medals != None:
        all_athletes = all_athletes.filter(
            Athletes.num_bronze_medals.in_(bronze_medals)
        )
    if silver_medals != None:
        all_athletes = all_athletes.filter(
            Athletes.num_silver_medals.in_(silver_medals)
        )
    if gold_medals != None:
        all_athletes = all_athletes.filter(Athletes.num_gold_medals.in_(gold_medals))
    if name != None:
        name = range_helper2("A", "ZZZZZZZZZZ", name)
        all_athletes = all_athletes.filter(
            and_(
                Athletes.athlete_name >= name[0],
                Athletes.athlete_name <= name[1],
            )
        )

    return all_athletes


def search_athletes(all_athletes, queries):
    q = query_helper("q", queries)
    if q is not None:
        search_terms = q[0].split()
        search_results = set()
        for word in search_terms:
            term_formatting = "%{}%".format(word)
            search_results.add(Athletes.country_name.ilike(term_formatting))
            search_results.add(Athletes.athlete_birthplace.ilike(term_formatting))
            search_results.add(Athletes.athlete_name.ilike(term_formatting))
            search_results.add(Athletes.athlete_sport.ilike(term_formatting))
            search_results.add(Athletes.athlete_DOB.ilike(term_formatting))
            search_results.add(Athletes.num_bronze_medals.ilike(term_formatting))
            search_results.add(Athletes.num_silver_medals.ilike(term_formatting))
            search_results.add(Athletes.num_gold_medals.ilike(term_formatting))
            search_results.add(Athletes.athlete_games.ilike(term_formatting))
            search_results.add(Athletes.athlete_height.ilike(term_formatting))

        all_athletes = all_athletes.filter(or_(*tuple(search_results)))
    return all_athletes
