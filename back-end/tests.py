from unittest import main, TestCase
from app import app


class Tests(TestCase):
    # testing /api/countries
    def test_countries_1(self):
        test_client = app.test_client()
        r = test_client.get("/api/countries")
        self.assertEqual(r.status_code, 200)

    def test_countries_2(self):
        test_client = app.test_client()
        r = test_client.get("/api/countries")
        data = r.json
        self.assertEqual(len(data["body"]), 10)

    def test_countries_3(self):
        test_client = app.test_client()
        r = test_client.get("/api/countries")
        data = r.json
        self.assertEqual(len(data["body"][0]), 14)

    # testing /api/countries/<int:index>
    def test_countries_index_1(self):
        test_client = app.test_client()
        r = test_client.get("/api/countries/4")
        self.assertEqual(r.status_code, 200)

    def test_countries_index_2(self):
        test_client = app.test_client()
        r = test_client.get("/api/countries/4")
        data = r.json
        self.assertEqual(len(data), 3)
        self.assertEqual(len(data["athletes"][0]), 14)
        self.assertEqual(len(data["country"]), 14)
        self.assertEqual(len(data["events"][0]), 18)

    # testing /api/athletes
    def test_athletes_1(self):
        test_client = app.test_client()
        r = test_client.get("/api/athletes")
        self.assertEqual(r.status_code, 200)

    def test_athletes_2(self):
        test_client = app.test_client()
        r = test_client.get("/api/athletes")
        data = r.json
        self.assertEqual(len(data["body"]), 12)

    def test_athletes_3(self):
        test_client = app.test_client()
        r = test_client.get("/api/athletes")
        data = r.json
        self.assertEqual(len(data["body"][0]), 14)

    # testing /api/athletes/<int:index>
    def test_athletes_index_1(self):
        test_client = app.test_client()
        r = test_client.get("/api/athletes/1")
        self.assertEqual(r.status_code, 200)

    def test_athletes_index_2(self):
        test_client = app.test_client()
        r = test_client.get("/api/athletes/1")
        data = r.json
        self.assertEqual(len(data), 2)
        self.assertEqual(len(data["athletes"]), 14)
        self.assertEqual(len(data["country"]), 14)

    # testing /api/events
    def test_events_1(self):
        test_client = app.test_client()
        r = test_client.get("/api/events")
        self.assertEqual(r.status_code, 200)

    def test_events_2(self):
        test_client = app.test_client()
        r = test_client.get("/api/events")
        data = r.json
        self.assertEqual(len(data["body"]), 10)

    def test_events_3(self):
        test_client = app.test_client()
        r = test_client.get("/api/events")
        data = r.json
        self.assertEqual(len(data["body"][0]), 18)

    # testing /api/events/<int:index>
    def test_events_index_1(self):
        test_client = app.test_client()
        r = test_client.get("/api/events/1")
        self.assertEqual(r.status_code, 200)

    def test_events_index_2(self):
        test_client = app.test_client()
        r = test_client.get("/api/events/1")
        data = r.json
        self.assertEqual(len(data), 3)
        self.assertEqual(len(data["athletes"]), 14)
        self.assertEqual(len(data["country"]), 14)
        self.assertEqual(len(data["events"]), 18)

    # searching by country capital
    def test_search_countries(self):
        test_client = app.test_client()
        r = test_client.get("/api/countries?q=kabul")
        data = r.json
        self.assertEqual(len(data["body"]), 1)
        self.assertEqual(data["pages"], 1)

    # searching by country region
    def test_search_countries_2(self):
        test_client = app.test_client()
        r = test_client.get("/api/countries?q=oceania")
        data = r.json
        self.assertEqual(len(data["body"]), 5)
        self.assertEqual(data["pages"], 1)

    # searching by athelte game (Antwerp in 1920)
    def test_search_athletes_1(self):
        test_client = app.test_client()
        r = test_client.get("/api/athletes?q=antwerp&p=2")
        data = r.json
        self.assertEqual(len(data["body"]), 6)
        self.assertEqual(data["pages"], 2)

    # searching by population and country name
    def test_search_countries_3(self):
        test_client = app.test_client()
        r = test_client.get("/api/countries?q=25687041 australia")
        data = r.json
        self.assertEqual(len(data["body"]), 1)
        self.assertEqual(data["pages"], 1)

    # Searching by athlete name
    def test_search_athletes_2(self):
        test_client = app.test_client()
        r = test_client.get("/api/athletes?q=dressel&p=1")
        data = r.json
        self.assertEqual(len(data["body"]), 1)
        self.assertEqual(data["pages"], 1)

    # Searching by event sport substring
    def test_search_events_1(self):
        test_client = app.test_client()
        r = test_client.get("/api/events?q=swim")
        data = r.json
        self.assertEqual(data["pages"], 4)

    def test_search_events_2(self):
        test_client = app.test_client()
        r = test_client.get("/api/events?q=united states of america")
        data = r.json
        self.assertEqual(data["pages"], 16)

    # filtering by country region
    def test_filter_countries(self):
        test_client = app.test_client()
        r = test_client.get("/api/countries?country_region=Oceania")
        data = r.json
        self.assertEqual(len(data["body"]), 5)
        self.assertEqual(data["pages"], 1)

    # filtering by country population range
    def test_filter_countries_2(self):
        test_client = app.test_client()
        r = test_client.get("/api/countries?country_population=10000-50000")
        data = r.json
        self.assertEqual(len(data["body"]), 3)
        self.assertEqual(data["pages"], 1)

    # filtering by athlete sport
    def test_filter_athletes(self):
        test_client = app.test_client()
        r = test_client.get("/api/athletes?athlete_sport=Speed skating")
        data = r.json
        self.assertEqual(len(data["body"]), 12)
        self.assertEqual(data["pages"], 8)

    # filtering by athlete country
    def test_filter_athletes_2(self):
        test_client = app.test_client()
        r = test_client.get("/api/athletes?athletes?country_name=USA")
        data = r.json
        self.assertEqual(len(data["body"]), 12)
        self.assertEqual(data["pages"], 292)

    # filtering by events sport
    def test_filter_events(self):
        test_client = app.test_client()
        r = test_client.get("/api/events?sport=Speed skating")
        data = r.json
        self.assertEqual(len(data["body"]), 10)
        self.assertEqual(data["pages"], 2)

    # filtering by events sport type
    def test_filter_events_2(self):
        test_client = app.test_client()
        r = test_client.get("/api/events?sport_type=Individual")
        data = r.json
        self.assertEqual(len(data["body"]), 10)
        self.assertEqual(data["pages"], 38)

    # sorting by events name
    def test_sort_events_1(self):
        test_client = app.test_client()
        r = test_client.get("/api/events?sort=name")
        data = r.json
        self.assertEqual(data["body"][0]["event_name"], "49er")
        r = test_client.get("/api/events?sort=name&order=desc")
        data = r.json
        self.assertEqual(data["body"][0]["event_name"], "Women's vault")

    # sorting by invalid parameter for events
    def test_sort_events_2(self):
        test_client = app.test_client()
        r = test_client.get("/api/events?sort=invalid-parameter")
        data = r.json
        self.assertEqual(data["body"][0]["event_id"], 1)

    # sorting by athletes name
    def test_sort_athletes_1(self):
        test_client = app.test_client()
        r = test_client.get("/api/athletes?sort=name")
        data = r.json
        self.assertEqual(data["body"][0]["athlete_name"], "Aaron Blunck")
        r = test_client.get("/api/athletes?sort=name&order=desc")
        data = r.json
        self.assertEqual(data["body"][0]["athlete_name"], "Zulkifli Abbas")

    # sorting by invalid parameter for athletes
    def test_sort_athletes_2(self):
        test_client = app.test_client()
        r = test_client.get("/api/athletes?sort=invalid-parameter")
        data = r.json
        self.assertEqual(data["body"][0]["athlete_id"], 1)

    # sorting by countries name
    def test_sort_countries_1(self):
        test_client = app.test_client()
        r = test_client.get("/api/countries?sort=name")
        data = r.json
        self.assertEqual(data["body"][0]["country_name"], "Afghanistan\u00a0")
        r = test_client.get("/api/countries?sort=name&order=desc")
        data = r.json
        self.assertEqual(data["body"][0]["country_name"], "Zimbabwe\u00a0")

    # sorting by invalid parameter for countries
    def test_sort_countries_2(self):
        test_client = app.test_client()
        r = test_client.get("/api/countries?sort=invalid-parameter")
        data = r.json
        self.assertEqual(data["body"][0]["country_id"], 1)


if __name__ == "__main__":
    main()
