from models import Countries_updated

from sqlalchemy import and_, or_, cast, Integer
from query_helpers import *


def sort_countries(all_countries, queries):
    sort = query_helper("sort", queries)

    if sort == None:
        return all_countries

    sort_attr = None
    sort = sort[0]
    if sort == "name":
        sort_attr = Countries_updated.country_name
    elif sort == "capital":
        sort_attr = Countries_updated.country_capital_city
    elif sort == "population":
        sort_attr = Countries_updated.country_population
    elif sort == "region":
        sort_attr = Countries_updated.country_region
    elif sort == "total-medals":
        sort_attr = cast(Countries_updated.country_medals_total, Integer)
    else:
        return all_countries

    order = query_helper("order", queries)
    order = order[0] if order != None else "asc"
    if order == "desc":
        return all_countries.order_by(sort_attr.desc())
    else:
        return all_countries.order_by(sort_attr)


def filter_countries(all_countries, queries):
    region = query_helper("country_region", queries)
    population_range = query_helper("country_population", queries)
    bronze_medals = query_helper("country_num_bronze", queries)
    silver_medals = query_helper("country_num_silver", queries)
    gold_medals = query_helper("country_num_gold", queries)
    num_olympics = query_helper("country_num_olympics", queries)
    total_medals = query_helper("country_medals_total", queries)
    name = query_helper("country_name", queries)
    capital_city = query_helper("country_capital_city", queries)

    if region != None:
        all_countries = all_countries.filter(
            Countries_updated.country_region.in_(region)
        )
    if population_range != None:
        population_range = range_helper(0, 1402112000, population_range)
        all_countries = all_countries.filter(
            and_(
                Countries_updated.country_population >= population_range[0],
                Countries_updated.country_population <= population_range[1],
            )
        )
    if bronze_medals != None:
        bronze_medals = range_helper(0, 841, bronze_medals)
        all_countries = all_countries.filter(
            and_(
                Countries_updated.country_num_bronze >= bronze_medals[0],
                Countries_updated.country_num_bronze <= bronze_medals[1],
            )
        )
    if silver_medals != None:
        silver_medals = range_helper(0, 959, silver_medals)
        all_countries = all_countries.filter(
            and_(
                Countries_updated.country_num_silver >= silver_medals[0],
                Countries_updated.country_num_silver <= silver_medals[1],
            )
        )
    if gold_medals != None:
        gold_medals = range_helper(0, 1180, gold_medals)
        all_countries = all_countries.filter(
            and_(
                Countries_updated.country_num_gold >= gold_medals[0],
                Countries_updated.country_num_gold <= gold_medals[1],
            )
        )
    if num_olympics != None:
        num_olympics = range_helper(0, 55, num_olympics)
        all_countries = all_countries.filter(
            and_(
                Countries_updated.country_num_olympics >= num_olympics[0],
                Countries_updated.country_num_olympics <= num_olympics[1],
            )
        )
    if total_medals != None:
        total_medals = range_helper(0, 18876, total_medals)
        all_countries = all_countries.filter(
            and_(
                Countries_updated.country_medals_total >= total_medals[0],
                Countries_updated.country_medals_total <= total_medals[1],
            )
        )
    if name != None:
        name = range_helper2("A", "ZZZZZZZZZZ", name)
        all_countries = all_countries.filter(
            and_(
                Countries_updated.country_name >= name[0],
                Countries_updated.country_name <= name[1],
            )
        )
    if capital_city != None:
        capital_city = range_helper2("A", "ZZZZZZZZZZ", capital_city)
        all_countries = all_countries.filter(
            and_(
                Countries_updated.country_capital_city >= capital_city[0],
                Countries_updated.country_capital_city <= capital_city[1],
            )
        )
    return all_countries


def search_countries(all_countries, queries):
    q = query_helper("q", queries)
    if q is not None:
        search_terms = q[0].split()
        search_results = set()
        for word in search_terms:
            term_formatting = "%{}%".format(word)
            is_number = any(char.isdigit() for char in word)
            search_results.add(Countries_updated.country_region.ilike(term_formatting))
            search_results.add(
                Countries_updated.country_capital_city.ilike(term_formatting)
            )
            search_results.add(Countries_updated.country_name.ilike(term_formatting))
            if is_number:
                word_int = [int(word)]
                search_results.add(Countries_updated.country_population.in_(word_int))
                search_results.add(Countries_updated.country_medals_total.in_(word_int))
        all_countries = all_countries.filter(or_(*tuple(search_results)))
    return all_countries
