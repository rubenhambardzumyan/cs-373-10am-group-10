import requests
import pandas
from sqlalchemy import create_engine
import io
import numpy as np
from bs4 import BeautifulSoup
import os
from dotenv import load_dotenv
import re

load_dotenv()
AWS_DB_KEY = os.getenv("AWS_DB_KEY")

country_id_to_name = pandas.read_csv(r"country_keys.csv")
names = list(country_id_to_name["Country Name"])
country_names = []
for name in names:
    new_name = name.replace("\xa0", "")
    new_name = new_name.lower()
    country_names.append(new_name)

athlete_id_to_name = pandas.read_csv(r"athlete_keys.csv")
athletes = list(athlete_id_to_name["name"])
response = requests.get(
    url="https://en.wikipedia.org/wiki/Chronological_summary_of_the_2018_Winter_Olympics"
)

soup = BeautifulSoup(response.text, "html.parser")
EventsTable = soup.findAll("table", {"class": "wikitable", "style": "width:100%;"})
df = pandas.read_html(str(EventsTable))
first = True
for day in EventsTable:
    df = pandas.read_html(str(day))
    df = pandas.DataFrame(df[0])
    if first:
        all_winter_data = df
    else:
        frames = [all_winter_data, df]
        all_winter_data = pandas.concat(frames)
    first = False
all_winter_data.columns = [
    "Bronze medalists",
    "Bronze Country",
    "1",
    "Event",
    "Gold medalists",
    "Gold Countries",
    "2",
    "3",
    "Silver medalists",
    "Silver country",
    "Sport",
]
all_winter_data = all_winter_data.drop(columns=["1", "2", "3"])
all_winter_data["Bronze Country"] = all_winter_data["Bronze Country"].str.split(
    pat="[", expand=True
)[0]
all_winter_data["Silver country"] = all_winter_data["Silver country"].str.split(
    pat="[", expand=True
)[0]
all_winter_data["Gold Countries"] = all_winter_data["Gold Countries"].str.split(
    pat="[", expand=True
)[0]
all_winter_data["most_recent_olympics"] = "2018 Winter Olympics"
all_winter_data["Season"] = "Winter"

response = requests.get(
    url="https://en.wikipedia.org/wiki/Chronological_summary_of_the_2016_Summer_Olympics"
)


soup = BeautifulSoup(response.text, "html.parser")
EventsTable = soup.findAll("table", {"class": "wikitable", "style": "width:100%;"})
df = pandas.read_html(str(EventsTable))
first = True

for day in EventsTable:
    df = pandas.read_html(str(day))
    df = pandas.DataFrame(df[0])
    if first:
        all_summer_data = df
    else:
        frames = [all_summer_data, df]
        all_summer_data = pandas.concat(frames)
    first = False

all_summer_data.columns = [
    "1",
    "Bronze medalists",
    "Bronze Country",
    "Event",
    "Gold medalists",
    "Gold Countries",
    "2",
    "3",
    "Silver medalists",
    "Silver country",
    "Sport",
]
all_summer_data = all_summer_data.drop(columns=["1", "2", "3"])
all_summer_data["Season"] = "Summer"
all_summer_data["most_recent_olympics"] = "2021 Tokyo Olympics"
# all_summer_data.to_csv(r"events2.csv", index = False)

frames = [all_winter_data, all_summer_data]
all_winter_data = pandas.concat(frames)

bronze_country_id = []
silver_country_id = []
gold_country_id = []
for country in all_winter_data["Bronze Country"]:
    if country.lower() in country_names:
        bronze_country_id.append(country_names.index(country.lower()) + 1)
    else:
        bronze_country_id.append(154)
for country in all_winter_data["Silver country"]:
    if country.lower() in country_names:
        silver_country_id.append(country_names.index(country.lower()) + 1)
    else:
        silver_country_id.append(154)
for country in all_winter_data["Gold Countries"]:
    if country.lower() in country_names:
        gold_country_id.append(country_names.index(country.lower()) + 1)
    else:
        gold_country_id.append(154)

bronze_athlete_ids = []
silver_athlete_ids = []
gold_athlete_ids = []
new_bronze_medalists = []
new_silver_medalists = []
new_gold_medalists = []
sport_type = []
for athlete in all_winter_data["Bronze medalists"]:
    new_bronze_athletes = ""
    current_bronze_ids = ""
    athlete = athlete.replace("*", "")
    if athlete in athletes:
        bronze_athlete_ids.append(athletes.index(athlete) + 1)
        sport_type.append("Individual")
    else:
        bronze_athletes = re.findall("[A-Z][^A-Z]*", athlete)
        if len(bronze_athletes) > 3:
            sport_type.append("Team")
        else:
            sport_type.append("Individual")
        first = True
        for i in range(0, len(bronze_athletes) - 2):
            new_string = bronze_athletes[i] + bronze_athletes[i + 1]
            found = False
            for name in athletes:
                if new_string in name:
                    index = athletes.index(name)
                    found = True
                    break
            if found:
                if not first:
                    new_bronze_athletes += ", "
                    current_bronze_ids += ", "
                first = False
                new_bronze_athletes += new_string
                current_bronze_ids += str(index)
        bronze_athlete_ids.append(current_bronze_ids)
    if len(new_bronze_athletes) != 0:
        new_bronze_medalists.append(new_bronze_athletes)
    else:
        new_bronze_medalists.append(athlete)
for athlete in all_winter_data["Silver medalists"]:
    new_bronze_athletes = ""
    current_bronze_ids = ""
    athlete = athlete.replace("*", "")
    if athlete in athletes:
        silver_athlete_ids.append(athletes.index(athlete) + 1)
    else:
        bronze_athletes = re.findall("[A-Z][^A-Z]*", athlete)
        first = True
        for i in range(0, len(bronze_athletes) - 2):
            new_string = bronze_athletes[i] + bronze_athletes[i + 1]
            found = False
            for name in athletes:
                if new_string in name:
                    index = athletes.index(name)
                    found = True
                    break
            if found:
                if not first:
                    new_bronze_athletes += ", "
                    current_bronze_ids += ", "
                first = False
                new_bronze_athletes += new_string
                current_bronze_ids += str(index)
        silver_athlete_ids.append(current_bronze_ids)
    if len(new_bronze_athletes) != 0:
        new_silver_medalists.append(new_bronze_athletes)
    else:
        new_silver_medalists.append(athlete)
for athlete in all_winter_data["Gold medalists"]:
    new_bronze_athletes = ""
    current_bronze_ids = ""
    athlete = athlete.replace("*", "")
    if athlete in athletes:
        gold_athlete_ids.append(athletes.index(athlete) + 1)
    else:
        bronze_athletes = re.findall("[A-Z][^A-Z]*", athlete)
        first = True
        for i in range(0, len(bronze_athletes) - 2):
            new_string = bronze_athletes[i] + bronze_athletes[i + 1]
            found = False
            for name in athletes:
                if new_string in name:
                    index = athletes.index(name)
                    found = True
                    break
            if found:
                if not first:
                    new_bronze_athletes += ", "
                    current_bronze_ids += ", "
                first = False
                new_bronze_athletes += new_string
                current_bronze_ids += str(index)
        gold_athlete_ids.append(current_bronze_ids)
    if len(new_bronze_athletes) != 0:
        new_gold_medalists.append(new_bronze_athletes)
    else:
        new_gold_medalists.append(athlete)
all_winter_data["sport_type"] = sport_type
all_winter_data["Bronze medalists"] = new_bronze_medalists
all_winter_data["Silver medalists"] = new_silver_medalists
all_winter_data["Gold medalists"] = new_gold_medalists
all_winter_data["bronze_country_id"] = bronze_country_id
all_winter_data["silver_country_id"] = silver_country_id
all_winter_data["gold_country_id"] = gold_country_id
all_winter_data["bronze_athlete_id"] = bronze_athlete_ids
all_winter_data["silver_athlete_id"] = silver_athlete_ids
all_winter_data["gold_athlete_id"] = gold_athlete_ids
all_winter_data["event_id"] = np.arange(len(all_winter_data)) + 1
all_winter_data["Event"] = all_winter_data["Event"].str.replace("0 ", "0")
all_winter_data["Event"] = all_winter_data["Event"].str.replace(" × ", "×")
print(all_winter_data.columns)
all_winter_data.columns = [
    "bronze_athletes",
    "bronze_country",
    "event_name",
    "gold_athletes",
    "gold_country",
    "silver_athletes",
    "silver_country",
    "sport",
    "most_recent_olympics",
    "season",
    "sport_type",
    "bronze_country_id",
    "silver_country_id",
    "gold_country_id",
    "bronze_athlete_id",
    "silver_athlete_id",
    "gold_athlete_id",
    "event_id",
]
# all_winter_data.to_csv(r"events.csv", index = False)
# Event_id_to_name = all_winter_data[['event_id', 'Event']].copy()
# Event_id_to_name.to_csv(r"event_keys.csv", index = False)

engine = create_engine(AWS_DB_KEY)
all_winter_data.head(0).to_sql(
    "events", engine, if_exists="replace", index=False
)  # drops old table and creates new empty table

conn = engine.raw_connection()
cur = conn.cursor()
output = io.StringIO()
all_winter_data.to_csv(output, sep="\t", header=False, index=False)
output.seek(0)
contents = output.getvalue()
cur.copy_from(output, "events", null="")  # null values become ''
conn.commit()
