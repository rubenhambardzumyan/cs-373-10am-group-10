from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from dotenv import load_dotenv
import os

load_dotenv()

app = Flask(__name__)
CORS(app)

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv("AWS_DB_KEY")
db = SQLAlchemy(app)
ma = Marshmallow(app)

# Athletes model
class Athletes(db.Model):
    athlete_id = db.Column(db.Integer(), primary_key=True)
    athlete_name = db.Column(db.String())
    country_name = db.Column(db.String())
    athlete_sport = db.Column(db.String())
    athlete_games = db.Column(db.String())
    athlete_DOB = db.Column(db.String())
    athlete_image = db.Column(db.String())
    athlete_height = db.Column(db.String())
    athlete_birthplace = db.Column(db.String())
    num_gold_medals = db.Column(db.Integer())
    num_silver_medals = db.Column(db.Integer())
    num_bronze_medals = db.Column(db.Integer())
    event_results = db.Column(db.Integer())

    # Associations
    countries = db.relationship("Countries_updated", backref=db.backref("athletes"))
    country_id = db.Column(db.Integer(), db.ForeignKey("countries_updated.country_id"))


# Countries model
class Countries_updated(db.Model):
    country_id = db.Column(db.Integer(), primary_key=True)
    country_name = db.Column(db.String())
    country_population = db.Column(db.String())
    country_gini = db.Column(db.String())
    country_region = db.Column(db.String())
    country_capital_city = db.Column(db.String())
    country_medals_total = db.Column(db.Integer())
    country_num_olympics = db.Column(db.Integer())
    country_num_gold = db.Column(db.Integer())
    country_num_silver = db.Column(db.Integer())
    country_num_bronze = db.Column(db.Integer())
    country_map = db.Column(db.String())
    country_language = db.Column(db.String())
    country_flag = db.Column(db.Text())


# Events model
class Events(db.Model):
    event_name = db.Column(db.String())
    bronze_athletes = db.Column(db.String())
    bronze_country = db.Column(db.String())
    gold_athletes = db.Column(db.String())
    gold_country = db.Column(db.String())
    silver_athletes = db.Column(db.String())
    silver_country = db.Column(db.String())
    sport = db.Column(db.String())
    most_recent_olympics = db.Column(db.String())
    bronze_country_id = db.Column(db.Integer())
    silver_country_id = db.Column(db.Integer())
    season = db.Column(db.String())
    event_id = db.Column(db.Integer(), primary_key=True)
    bronze_athlete_id = db.Column(db.Integer())
    silver_athlete_id = db.Column(db.Integer())
    sport_type = db.Column(db.String())

    # Associations
    gold_country_id = db.Column(
        db.Integer(), db.ForeignKey("countries_updated.country_id")
    )
    countries = db.relationship("Countries_updated", backref=db.backref("events"))
    gold_athlete_id = db.Column(db.Integer(), db.ForeignKey("athletes.athlete_id"))
    athletes = db.relationship("Athletes", backref=db.backref("events"))


class AthletesSchema(ma.SQLAlchemySchema):
    class Meta:
        fields = (
            "athlete_id",
            "athlete_name",
            "country_name",
            "athlete_sport",
            "athlete_games",
            "athlete_DOB",
            "country_id",
            "athlete_image",
            "athlete_height",
            "athlete_birthplace",
            "num_gold_medals",
            "num_silver_medals",
            "num_bronze_medals",
            "event_results",
        )
        include_relationships = True
        load_instance = True


class CountriesSchema(ma.SQLAlchemySchema):
    class Meta:
        fields = (
            "country_id",
            "country_name",
            "country_population",
            "country_gini",
            "country_region",
            "country_capital_city",
            "country_medals_total",
            "country_num_olympics",
            "country_num_gold",
            "country_num_silver",
            "country_num_bronze",
            "country_map",
            "country_language",
            "country_flag",
        )
        include_relationships = True
        load_instance = True


class EventsSchema(ma.SQLAlchemySchema):
    class Meta:
        fields = (
            "event_name",
            "bronze_athletes",
            "bronze_country",
            "gold_athletes",
            "gold_country",
            "silver_athletes",
            "silver_country",
            "sport",
            "most_recent_olympics",
            "bronze_country_id",
            "silver_country_id",
            "gold_country_id",
            "season",
            "event_id",
            "bronze_athlete_id",
            "silver_athlete_id",
            "gold_athlete_id",
            "sport_type",
        )
        include_relationships = True
        load_instance = True


# Schemas created for each model
country_schema = CountriesSchema()
countries_schema = CountriesSchema(many=True)
athlete_schema = AthletesSchema()
athletes_schema = AthletesSchema(many=True)
event_schema = EventsSchema()
events_schema = EventsSchema(many=True)
