.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules
SHELL         := bash

all:

# remove temporary files
clean:
	rm -f  *.tmp
	rm -rf __pycache__

# run docker
docker:
	cd front-end && docker build --pull --rm -f "Dockerfile" -t cs37310amgroup10:latest "." && docker run -it -p 3000:3000 cs37310amgroup10

# format code
format:
	cd front-end/ && npx prettier --write .
	cd front-end/ && black .
	cd back-end/ && black .

# run ESlint
lint:
	cd front-end/ && yarn && yarn run eslint .