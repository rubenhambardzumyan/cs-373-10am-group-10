**Group Members (GitLab ID, EID)**

- Victoria Hall: victoriahall, vjh378
- Sindhu Nemana: sinemana, sn25488
- Ruben Hambardzumyan: rubenhambardzumyan, rh36424
- Varad Thorat: varadthorat, vvt255
- Thomas Budiman: thomas1b, tib276

Git SHA: 
- Phase 1: 9b8ca0e8df14db12aceef9356520e25871c6bc91
- Phase 2: e5ccd0367e1c70b53df1583faa87bc772763f69b
- Phase 3: 7b7c133b6215ec9ceab8565fecea6a34f5f65cbc
- Phase 4: fc11f3b7efa940924020ddf0289465813acd17ea

Project Leader: 
- Phase 1: Sindhu Nemana
- Phase 2: Victoria Hall
- Phase 3: Thomas Budiman
- Phase 4: Varad Thorat

GitLab Pipelines: https://gitlab.com/rubenhambardzumyan/cs-373-10am-group-10/-/pipelines

Website: https://www.goingforgold.me

Estimated completion time for each member:

- Victoria: 
    - Phase 1: 10 hours
    - Phase 2: 24 hours
    - Phase 3: 20 hours
    - Phase 4: 15 hours
- Sindhu:
    - Phase 1: 10 hours
    - Phase 2: 24 hours
    - Phase 3: 19 hours
    - Phase 4: 15 hours
- Ruben:
    - Phase 1: 10 hours
    - Phase 2: 24 hours
    - Phase 3: 19 hours
    - Phase 4: 15 hours
- Varad:
    - Phase 1: 10 hours
    - Phase 2: 24 hours
    - Phase 3: 21 hours
    - Phase 4: 16 hours
- Thomas:
    - Phase 1: 10 hours
    - Phase 2: 24 hours
    - Phase 3: 23 hours
    - Phase 4: 16 hours

Actual completion time for each member:

- Victoria:
    - Phase 1: 21 hours
    - Phase 2: 24 hours
    - Phase 3: 18 hours
    - Phase 4: 13 hours
- Sindhu: 
    - Phase 1: 23 hours
    - Phase 2: 25 hours
    - Phase 3: 19 hours 
    - Phase 4: 13 hours
- Ruben: 
    - Phase 1: 20 hours
    - Phase 2: 22 hours
    - Phase 3: 16 hours
    - Phase 4: 14 hours
- Varad: 
    - Phase 1: 22 hours
    - Phase 2: 20 hours
    - Phase 3: 17 hours
    - Phase 4: 14 hours
- Thomas: 
    - Phase 1: 22 hours
    - Phase 2: 24 hours
    - Phase 3: 20 hours
    - Phase 4: 15 hours

Comments: It was challenging to get some of the tools set up, but we stayed on top of all of our work and are proud of what we accomplished!
For code references, we looked TA Caitlin and Jefferson's code repos from when they took the class for inspiration for our Docker image, Technical Report, Testing, and general code structure. We also looked at whereartthou.me's repo for help with our backend unit tests. There are also links to sources we used for implementing front end tests. We also took a look at Ta Jefferson's code to get the idea to create a seperate search page for the country and events view all page. This is because we are not able to modify the text output of the model table. TA Caitlin confirmed that this behavior met the requirements.
Additional sources used for Paginations:
- https://gitlab.com/ryanarifin134/travvy-explorers/
- https://javascript.plainenglish.io/building-a-pagination-component-in-react-with-typescript-2e7f7b62b35d
- https://www.youtube.com/watch?v=Law7wfdg_ls
Primary Source used for Phase 3:
- https://gitlab.com/forbesye/fitsbits/-/tree/master/
