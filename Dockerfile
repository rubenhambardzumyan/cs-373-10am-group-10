FROM python:latest

RUN apt-get update 
RUN apt-get install -y curl unzip xvfb libxi6 libgconf-2-4

RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN apt install -y ./google-chrome-stable_current_amd64.deb

#some envs
ENV APP_HOME /app 
ENV PORT 5000

#set workspace
WORKDIR ${APP_HOME}

#copy local files
COPY . . 

RUN pip3 install --upgrade pip
RUN pip3 install -r ./front-end/gui_tests/requirements.txt

CMD bash
